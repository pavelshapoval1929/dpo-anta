<?php  
include 'regions.php';
header("Content-type: text/plain");
$orig ="User-agent: Yandex
Disallow: /wp-admin
Disallow: /wp-includes
Disallow: /wp-login.php
Disallow: /wp-register.php
Disallow: /xmlrpc.php
Disallow: /?s=*
Disallow: /search*
Disallow: */trackback/
Disallow: */feed
Disallow: */comments/
Disallow: */comment
Disallow: */attachment/*
Disallow: */print/
Disallow: *?print=*
Disallow: */embed*
Allow: /wp-content/uploads/

User-agent: Googlebot
Disallow: /wp-admin
Disallow: /wp-login.php
Disallow: /wp-register.php
Disallow: /xmlrpc.php
Disallow: /?s=*
Disallow: /search*
Disallow: */trackback/
Disallow: */feed
Disallow: */comments/
Disallow: */comment
Disallow: */attachment/*
Disallow: */print/
Disallow: *?print=*
Disallow: */embed*
Allow: /wp-content/uploads/

User-agent: Mail.Ru
Disallow: /wp-admin
Disallow: /wp-includes
Disallow: /wp-login.php
Disallow: /wp-register.php
Disallow: /xmlrpc.php
Disallow: /?s=*
Disallow: /search*
Disallow: */trackback/
Disallow: */feed
Disallow: */comments/
Disallow: */comment
Disallow: */attachment/*
Disallow: */print/
Disallow: *?print=*
Disallow: */embed*
Allow: /wp-content/uploads/

User-agent: *
Disallow: /wp-admin
Disallow: /wp-includes
Disallow: /wp-login.php
Disallow: /wp-register.php
Disallow: /xmlrpc.php
Disallow: /?s=*
Disallow: /search*
Disallow: */trackback/
Disallow: */feed
Disallow: */comments/
Disallow: */comment
Disallow: */attachment/*
Disallow: */print/
Disallow: *?print=*
Disallow: */embed*
Allow: /wp-content/uploads/

User-agent: Googlebot-Image
Allow: /wp-content/uploads/

User-agent: YandexImages
Allow: /wp-content/uploads/";
if ($_SERVER['HTTP_HOST'] == 'dpo-anta.ru') {
	$host = "Host: https://".$_SERVER['SERVER_NAME']; 
	$sitemap="Sitemap: https://".$_SERVER['SERVER_NAME']."/sitemap.xml";
} else {
	$host = "Host: https://".$_SERVER['SERVER_NAME'];
	$sitemap="Sitemap: https://".$_SERVER['SERVER_NAME']."/sitemap.xml";
}

echo $orig."\n";
echo $host."\n";
echo $sitemap;

