<?
global $regions;
//return false;

	$regions['abakan.dpo-anta.ru']['title']=' в Абакане';
    $regions['abakan.dpo-anta.ru']['modal']=' Абакан';
    $regions['abakan.dpo-anta.ru']['address'] = '<span itemprop="addressLocality">г. Абакан</span>, <span itemprop="streetAddress">пр. Ямашева, д. 93, ТЦ «Савиново», 5 эт., м. Козья слобода</span>';
    $regions['abakan.dpo-anta.ru']['phone'] = '+7 (987) 291-86-08';
    $regions['abakan.dpo-anta.ru']['working_hours'] = 'с 10:00 до 20:00';
    $regions['abakan.dpo-anta.ru']['ya_coord'] = '55.826511, 49.148924';

	$regions['almetevsk.dpo-anta.ru']['title']=' в Альметьевске';
	$regions['almetevsk.dpo-anta.ru']['modal']=' Альметьевск';
	$regions['almetevsk.dpo-anta.ru']['address'] = '<span itemprop="addressLocality">г. Альметьевск</span>, <span itemprop="streetAddress">пр. Ямашева, д. 93, ТЦ «Савиново», 5 эт., м. Козья слобода</span>';
	$regions['almetevsk.dpo-anta.ru']['phone'] = '+7 (987) 291-86-08';
	$regions['almetevsk.dpo-anta.ru']['working_hours'] = 'с 10:00 до 20:00';
	$regions['almetevsk.dpo-anta.ru']['ya_coord'] = '55.826511, 49.148924';

	$regions['angarsk.dpo-anta.ru']['title']=' в Ангарске';
	$regions['angarsk.dpo-anta.ru']['modal']=' Ангарск';
	$regions['angarsk.dpo-anta.ru']['address'] = '<span itemprop="addressLocality">г. Ангарск</span>, <span itemprop="streetAddress">пр. Ямашева, д. 93, ТЦ «Савиново», 5 эт., м. Козья слобода</span>';
	$regions['angarsk.dpo-anta.ru']['phone'] = '+7 (987) 291-86-08';
	$regions['angarsk.dpo-anta.ru']['working_hours'] = 'с 10:00 до 20:00';
	$regions['angarsk.dpo-anta.ru']['ya_coord'] = '55.826511, 49.148924';

	$regions['arzamas.dpo-anta.ru']['title']=' в Арзамасе';
	$regions['arzamas.dpo-anta.ru']['modal']=' Арзамас';
	$regions['arzamas.dpo-anta.ru']['address'] = '<span itemprop="addressLocality">г. Арзамас</span>, <span itemprop="streetAddress">пр. Ямашева, д. 93, ТЦ «Савиново», 5 эт., м. Козья слобода</span>';
	$regions['arzamas.dpo-anta.ru']['phone'] = '+7 (987) 291-86-08';
	$regions['arzamas.dpo-anta.ru']['working_hours'] = 'с 10:00 до 20:00';
	$regions['arzamas.dpo-anta.ru']['ya_coord'] = '55.826511, 49.148924';

	$regions['armavir.dpo-anta.ru']['title']=' в Армавире';
	$regions['armavir.dpo-anta.ru']['modal']=' Армавир';
	$regions['armavir.dpo-anta.ru']['address'] = '<span itemprop="addressLocality">г. Армавир</span>, <span itemprop="streetAddress">пр. Ямашева, д. 93, ТЦ «Савиново», 5 эт., м. Козья слобода</span>';
	$regions['armavir.dpo-anta.ru']['phone'] = '+7 (987) 291-86-08';
	$regions['armavir.dpo-anta.ru']['working_hours'] = 'с 10:00 до 20:00';
	$regions['armavir.dpo-anta.ru']['ya_coord'] = '55.826511, 49.148924';

	$regions['artyem.dpo-anta.ru']['title']=' в Артеме';
	$regions['artyem.dpo-anta.ru']['modal']=' Артем';
	$regions['artyem.dpo-anta.ru']['address'] = '<span itemprop="addressLocality">г. Артем</span>, <span itemprop="streetAddress">пр. Ямашева, д. 93, ТЦ «Савиново», 5 эт., м. Козья слобода</span>';
	$regions['artyem.dpo-anta.ru']['phone'] = '+7 (987) 291-86-08';
	$regions['artyem.dpo-anta.ru']['working_hours'] = 'с 10:00 до 20:00';
	$regions['artyem.dpo-anta.ru']['ya_coord'] = '55.826511, 49.148924';

	$regions['arkhangelsk.dpo-anta.ru']['title']=' в Архангельске';
	$regions['arkhangelsk.dpo-anta.ru']['modal']=' Архангельск';
	$regions['arkhangelsk.dpo-anta.ru']['address'] = '<span itemprop="addressLocality">г. Архангельск</span>, <span itemprop="streetAddress">пр. Ямашева, д. 93, ТЦ «Савиново», 5 эт., м. Козья слобода</span>';
	$regions['arkhangelsk.dpo-anta.ru']['phone'] = '+7 (987) 291-86-08';
	$regions['arkhangelsk.dpo-anta.ru']['working_hours'] = 'с 10:00 до 20:00';
	$regions['arkhangelsk.dpo-anta.ru']['ya_coord'] = '55.826511, 49.148924';

	$regions['astrakhan.dpo-anta.ru']['title']=' в Астрахане';
	$regions['astrakhan.dpo-anta.ru']['modal']=' Астрахань';
	$regions['astrakhan.dpo-anta.ru']['address'] = '<span itemprop="addressLocality">г. Астрахань</span>, <span itemprop="streetAddress">пр. Ямашева, д. 93, ТЦ «Савиново», 5 эт., м. Козья слобода</span>';
	$regions['astrakhan.dpo-anta.ru']['phone'] = '+7 (987) 291-86-08';
	$regions['astrakhan.dpo-anta.ru']['working_hours'] = 'с 10:00 до 20:00';
	$regions['astrakhan.dpo-anta.ru']['ya_coord'] = '55.826511, 49.148924';

	$regions['achinsk.dpo-anta.ru']['title']=' в Ачинск';
	$regions['achinsk.dpo-anta.ru']['modal']=' Ачинск';
	$regions['achinsk.dpo-anta.ru']['address'] = '<span itemprop="addressLocality">г. Ачинск</span>, <span itemprop="streetAddress">пр. Ямашева, д. 93, ТЦ «Савиново», 5 эт., м. Козья слобода</span>';
	$regions['achinsk.dpo-anta.ru']['phone'] = '+7 (987) 291-86-08';
	$regions['achinsk.dpo-anta.ru']['working_hours'] = 'с 10:00 до 20:00';
	$regions['achinsk.dpo-anta.ru']['ya_coord'] = '55.826511, 49.148924';

	$regions['balakovo.dpo-anta.ru']['title']=' в Балакове';
	$regions['balakovo.dpo-anta.ru']['modal']=' Балаково';
	$regions['balakovo.dpo-anta.ru']['address'] = '<span itemprop="addressLocality">г. Балаково</span>, <span itemprop="streetAddress">пр. Ямашева, д. 93, ТЦ «Савиново», 5 эт., м. Козья слобода</span>';
	$regions['balakovo.dpo-anta.ru']['phone'] = '+7 (987) 291-86-08';
	$regions['balakovo.dpo-anta.ru']['working_hours'] = 'с 10:00 до 20:00';
	$regions['balakovo.dpo-anta.ru']['ya_coord'] = '55.826511, 49.148924';

	$regions['balashikha.dpo-anta.ru']['title']=' в Балашихе';
	$regions['balashikha.dpo-anta.ru']['modal']=' Балашиха';
	$regions['balashikha.dpo-anta.ru']['address'] = '<span itemprop="addressLocality">г. Балашиха</span>, <span itemprop="streetAddress">пр. Ямашева, д. 93, ТЦ «Савиново», 5 эт., м. Козья слобода</span>';
	$regions['balashikha.dpo-anta.ru']['phone'] = '+7 (987) 291-86-08';
	$regions['balashikha.dpo-anta.ru']['working_hours'] = 'с 10:00 до 20:00';
	$regions['balashikha.dpo-anta.ru']['ya_coord'] = '55.826511, 49.148924';

	$regions['barnaul.dpo-anta.ru']['title']=' в Барнауле';
	$regions['barnaul.dpo-anta.ru']['modal']=' Барнаул';
	$regions['barnaul.dpo-anta.ru']['address'] = '<span itemprop="addressLocality">г. Барнаул</span>, <span itemprop="streetAddress">пр. Ямашева, д. 93, ТЦ «Савиново», 5 эт., м. Козья слобода</span>';
	$regions['barnaul.dpo-anta.ru']['phone'] = '+7 (987) 291-86-08';
	$regions['barnaul.dpo-anta.ru']['working_hours'] = 'с 10:00 до 20:00';
	$regions['barnaul.dpo-anta.ru']['ya_coord'] = '55.826511, 49.148924';

	$regions['bataysk.dpo-anta.ru']['title']=' в Батайске';
	$regions['bataysk.dpo-anta.ru']['modal']=' Батайск';
	$regions['bataysk.dpo-anta.ru']['address'] = '<span itemprop="addressLocality">г. Батайск</span>, <span itemprop="streetAddress">пр. Ямашева, д. 93, ТЦ «Савиново», 5 эт., м. Козья слобода</span>';
	$regions['bataysk.dpo-anta.ru']['phone'] = '+7 (987) 291-86-08';
	$regions['bataysk.dpo-anta.ru']['working_hours'] = 'с 10:00 до 20:00';
	$regions['bataysk.dpo-anta.ru']['ya_coord'] = '55.826511, 49.148924';

	$regions['belgorod.dpo-anta.ru']['title']=' в Белгороде';
	$regions['belgorod.dpo-anta.ru']['modal']=' Белгород';
	$regions['belgorod.dpo-anta.ru']['address'] = '<span itemprop="addressLocality">г. Белгород</span>, <span itemprop="streetAddress">пр. Ямашева, д. 93, ТЦ «Савиново», 5 эт., м. Козья слобода</span>';
	$regions['belgorod.dpo-anta.ru']['phone'] = '+7 (987) 291-86-08';
	$regions['belgorod.dpo-anta.ru']['working_hours'] = 'с 10:00 до 20:00';
	$regions['belgorod.dpo-anta.ru']['ya_coord'] = '55.826511, 49.148924';

	$regions['berdsk.dpo-anta.ru']['title']=' в Бердске';
	$regions['berdsk.dpo-anta.ru']['modal']=' Бердск';
	$regions['berdsk.dpo-anta.ru']['address'] = '<span itemprop="addressLocality">г. Бердск</span>, <span itemprop="streetAddress">пр. Ямашева, д. 93, ТЦ «Савиново», 5 эт., м. Козья слобода</span>';
	$regions['berdsk.dpo-anta.ru']['phone'] = '+7 (987) 291-86-08';
	$regions['berdsk.dpo-anta.ru']['working_hours'] = 'с 10:00 до 20:00';
	$regions['berdsk.dpo-anta.ru']['ya_coord'] = '55.826511, 49.148924';

	$regions['berezniki.dpo-anta.ru']['title']=' в Березниках';
	$regions['berezniki.dpo-anta.ru']['modal']=' Березники';
	$regions['berezniki.dpo-anta.ru']['address'] = '<span itemprop="addressLocality">г. Березники</span>, <span itemprop="streetAddress">пр. Ямашева, д. 93, ТЦ «Савиново», 5 эт., м. Козья слобода</span>';
	$regions['berezniki.dpo-anta.ru']['phone'] = '+7 (987) 291-86-08';
	$regions['berezniki.dpo-anta.ru']['working_hours'] = 'с 10:00 до 20:00';
	$regions['berezniki.dpo-anta.ru']['ya_coord'] = '55.826511, 49.148924';

	$regions['biysk.dpo-anta.ru']['title']=' в Бийске';
	$regions['biysk.dpo-anta.ru']['modal']=' Бийск';
	$regions['biysk.dpo-anta.ru']['address'] = '<span itemprop="addressLocality">г. Бийск</span>, <span itemprop="streetAddress">пр. Ямашева, д. 93, ТЦ «Савиново», 5 эт., м. Козья слобода</span>';
	$regions['biysk.dpo-anta.ru']['phone'] = '+7 (987) 291-86-08';
	$regions['biysk.dpo-anta.ru']['working_hours'] = 'с 10:00 до 20:00';
	$regions['biysk.dpo-anta.ru']['ya_coord'] = '55.826511, 49.148924';

	$regions['blagoveshchensk.dpo-anta.ru']['title']=' в Благовещенске';
	$regions['blagoveshchensk.dpo-anta.ru']['modal']=' Благовещенск';
	$regions['blagoveshchensk.dpo-anta.ru']['address'] = '<span itemprop="addressLocality">г. Благовещенск</span>, <span itemprop="streetAddress">пр. Ямашева, д. 93, ТЦ «Савиново», 5 эт., м. Козья слобода</span>';
	$regions['blagoveshchensk.dpo-anta.ru']['phone'] = '+7 (987) 291-86-08';
	$regions['blagoveshchensk.dpo-anta.ru']['working_hours'] = 'с 10:00 до 20:00';
	$regions['blagoveshchensk.dpo-anta.ru']['ya_coord'] = '55.826511, 49.148924';

	$regions['bratsk.dpo-anta.ru']['title']=' в Братске';
	$regions['bratsk.dpo-anta.ru']['modal']=' Братск';
	$regions['bratsk.dpo-anta.ru']['address'] = '<span itemprop="addressLocality">г. Братск</span>, <span itemprop="streetAddress">пр. Ямашева, д. 93, ТЦ «Савиново», 5 эт., м. Козья слобода</span>';
	$regions['bratsk.dpo-anta.ru']['phone'] = '+7 (987) 291-86-08';
	$regions['bratsk.dpo-anta.ru']['working_hours'] = 'с 10:00 до 20:00';
	$regions['bratsk.dpo-anta.ru']['ya_coord'] = '55.826511, 49.148924';

	$regions['bryansk.dpo-anta.ru']['title']=' в Брянске';
	$regions['bryansk.dpo-anta.ru']['modal']=' Брянск';
	$regions['bryansk.dpo-anta.ru']['address'] = '<span itemprop="addressLocality">г. Брянск</span>, <span itemprop="streetAddress">пр. Ямашева, д. 93, ТЦ «Савиново», 5 эт., м. Козья слобода</span>';
	$regions['bryansk.dpo-anta.ru']['phone'] = '+7 (987) 291-86-08';
	$regions['bryansk.dpo-anta.ru']['working_hours'] = 'с 10:00 до 20:00';
	$regions['bryansk.dpo-anta.ru']['ya_coord'] = '55.826511, 49.148924';

	$regions['velikiy-novgorod.dpo-anta.ru']['title']=' в Великом Новгороде';
	$regions['velikiy-novgorod.dpo-anta.ru']['modal']=' Великий Новгород';
	$regions['velikiy-novgorod.dpo-anta.ru']['address'] = '<span itemprop="addressLocality">г. Великий Новгород</span>, <span itemprop="streetAddress">пр. Ямашева, д. 93, ТЦ «Савиново», 5 эт., м. Козья слобода</span>';
	$regions['velikiy-novgorod.dpo-anta.ru']['phone'] = '+7 (987) 291-86-08';
	$regions['velikiy-novgorod.dpo-anta.ru']['working_hours'] = 'с 10:00 до 20:00';
	$regions['velikiy-novgorod.dpo-anta.ru']['ya_coord'] = '55.826511, 49.148924';

	$regions['vladivostok.dpo-anta.ru']['title']=' во Владивостоке';
	$regions['vladivostok.dpo-anta.ru']['modal']=' Владивосток';
	$regions['vladivostok.dpo-anta.ru']['address'] = '<span itemprop="addressLocality">г. Владивосток</span>, <span itemprop="streetAddress">пр. Ямашева, д. 93, ТЦ «Савиново», 5 эт., м. Козья слобода</span>';
	$regions['vladivostok.dpo-anta.ru']['phone'] = '+7 (987) 291-86-08';
	$regions['vladivostok.dpo-anta.ru']['working_hours'] = 'с 10:00 до 20:00';
	$regions['vladivostok.dpo-anta.ru']['ya_coord'] = '55.826511, 49.148924';

	$regions['vladikavkaz.dpo-anta.ru']['title']=' во Владикавказе';
	$regions['vladikavkaz.dpo-anta.ru']['modal']=' Владикавказ';
	$regions['vladikavkaz.dpo-anta.ru']['address'] = '<span itemprop="addressLocality">г. Владикавказ</span>, <span itemprop="streetAddress">пр. Ямашева, д. 93, ТЦ «Савиново», 5 эт., м. Козья слобода</span>';
	$regions['vladikavkaz.dpo-anta.ru']['phone'] = '+7 (987) 291-86-08';
	$regions['vladikavkaz.dpo-anta.ru']['working_hours'] = 'с 10:00 до 20:00';
	$regions['vladikavkaz.dpo-anta.ru']['ya_coord'] = '55.826511, 49.148924';

	$regions['vladimir.dpo-anta.ru']['title']=' во Владимире';
	$regions['vladimir.dpo-anta.ru']['modal']=' Владимир';
	$regions['vladimir.dpo-anta.ru']['address'] = '<span itemprop="addressLocality">г. Владимир</span>, <span itemprop="streetAddress">пр. Ямашева, д. 93, ТЦ «Савиново», 5 эт., м. Козья слобода</span>';
	$regions['vladimir.dpo-anta.ru']['phone'] = '+7 (987) 291-86-08';
	$regions['vladimir.dpo-anta.ru']['working_hours'] = 'с 10:00 до 20:00';
	$regions['vladimir.dpo-anta.ru']['ya_coord'] = '55.826511, 49.148924';

	$regions['volgograd.dpo-anta.ru']['title']=' в Волгограде';
	$regions['volgograd.dpo-anta.ru']['modal']=' Волгоград';
	$regions['volgograd.dpo-anta.ru']['address'] = '<span itemprop="addressLocality">г. Волгоград</span>, <span itemprop="streetAddress">пр. Ямашева, д. 93, ТЦ «Савиново», 5 эт., м. Козья слобода</span>';
	$regions['volgograd.dpo-anta.ru']['phone'] = '+7 (987) 291-86-08';
	$regions['volgograd.dpo-anta.ru']['working_hours'] = 'с 10:00 до 20:00';
	$regions['volgograd.dpo-anta.ru']['ya_coord'] = '55.826511, 49.148924';

	$regions['volgodonsk.dpo-anta.ru']['title']=' в Волгодонске';
	$regions['volgodonsk.dpo-anta.ru']['modal']=' Волгодонск';
	$regions['volgodonsk.dpo-anta.ru']['address'] = '<span itemprop="addressLocality">г. Волгодонск</span>, <span itemprop="streetAddress">пр. Ямашева, д. 93, ТЦ «Савиново», 5 эт., м. Козья слобода</span>';
	$regions['volgodonsk.dpo-anta.ru']['phone'] = '+7 (987) 291-86-08';
	$regions['volgodonsk.dpo-anta.ru']['working_hours'] = 'с 10:00 до 20:00';
	$regions['volgodonsk.dpo-anta.ru']['ya_coord'] = '55.826511, 49.148924';

	$regions['volzhskiy.dpo-anta.ru']['title']=' в Волжском';
	$regions['volzhskiy.dpo-anta.ru']['modal']=' Волжский';
	$regions['volzhskiy.dpo-anta.ru']['address'] = '<span itemprop="addressLocality">г. Волжский</span>, <span itemprop="streetAddress">пр. Ямашева, д. 93, ТЦ «Савиново», 5 эт., м. Козья слобода</span>';
	$regions['volzhskiy.dpo-anta.ru']['phone'] = '+7 (987) 291-86-08';
	$regions['volzhskiy.dpo-anta.ru']['working_hours'] = 'с 10:00 до 20:00';
	$regions['volzhskiy.dpo-anta.ru']['ya_coord'] = '55.826511, 49.148924';

	$regions['vologda.dpo-anta.ru']['title']=' в Вологде';
	$regions['vologda.dpo-anta.ru']['modal']=' Вологда';
	$regions['vologda.dpo-anta.ru']['address'] = '<span itemprop="addressLocality">г. Вологда</span>, <span itemprop="streetAddress">пр. Ямашева, д. 93, ТЦ «Савиново», 5 эт., м. Козья слобода</span>';
	$regions['vologda.dpo-anta.ru']['phone'] = '+7 (987) 291-86-08';
	$regions['vologda.dpo-anta.ru']['working_hours'] = 'с 10:00 до 20:00';
	$regions['vologda.dpo-anta.ru']['ya_coord'] = '55.826511, 49.148924';

	$regions['voronezh.dpo-anta.ru']['title']=' в Воронеже';
	$regions['voronezh.dpo-anta.ru']['modal']=' Воронеж';
	$regions['voronezh.dpo-anta.ru']['address'] = '<span itemprop="addressLocality">г. Воронеж</span>, <span itemprop="streetAddress">пр. Ямашева, д. 93, ТЦ «Савиново», 5 эт., м. Козья слобода</span>';
	$regions['voronezh.dpo-anta.ru']['phone'] = '+7 (987) 291-86-08';
	$regions['voronezh.dpo-anta.ru']['working_hours'] = 'с 10:00 до 20:00';
	$regions['voronezh.dpo-anta.ru']['ya_coord'] = '55.826511, 49.148924';

	$regions['groznyy.dpo-anta.ru']['title']=' в Грозный';
	$regions['groznyy.dpo-anta.ru']['modal']=' Грозном';
	$regions['groznyy.dpo-anta.ru']['address'] = '<span itemprop="addressLocality">г. Грозный</span>, <span itemprop="streetAddress">пр. Ямашева, д. 93, ТЦ «Савиново», 5 эт., м. Козья слобода</span>';
	$regions['groznyy.dpo-anta.ru']['phone'] = '+7 (987) 291-86-08';
	$regions['groznyy.dpo-anta.ru']['working_hours'] = 'с 10:00 до 20:00';
	$regions['groznyy.dpo-anta.ru']['ya_coord'] = '55.826511, 49.148924';

	$regions['derbent.dpo-anta.ru']['title']=' в Дербенте';
	$regions['derbent.dpo-anta.ru']['modal']=' Дербент';
	$regions['derbent.dpo-anta.ru']['address'] = '<span itemprop="addressLocality">г. Дербент</span>, <span itemprop="streetAddress">пр. Ямашева, д. 93, ТЦ «Савиново», 5 эт., м. Козья слобода</span>';
	$regions['derbent.dpo-anta.ru']['phone'] = '+7 (987) 291-86-08';
	$regions['derbent.dpo-anta.ru']['working_hours'] = 'с 10:00 до 20:00';
	$regions['derbent.dpo-anta.ru']['ya_coord'] = '55.826511, 49.148924';

	$regions['dzerzhinsk.dpo-anta.ru']['title']=' в Дзержинске';
	$regions['dzerzhinsk.dpo-anta.ru']['modal']=' Дзержинск';
	$regions['dzerzhinsk.dpo-anta.ru']['address'] = '<span itemprop="addressLocality">г. Дзержинск</span>, <span itemprop="streetAddress">пр. Ямашева, д. 93, ТЦ «Савиново», 5 эт., м. Козья слобода</span>';
	$regions['dzerzhinsk.dpo-anta.ru']['phone'] = '+7 (987) 291-86-08';
	$regions['dzerzhinsk.dpo-anta.ru']['working_hours'] = 'с 10:00 до 20:00';
	$regions['dzerzhinsk.dpo-anta.ru']['ya_coord'] = '55.826511, 49.148924';

	$regions['dimitrovgrad.dpo-anta.ru']['title']=' в Димитровграде';
	$regions['dimitrovgrad.dpo-anta.ru']['modal']=' Димитровград';
	$regions['dimitrovgrad.dpo-anta.ru']['address'] = '<span itemprop="addressLocality">г. Димитровград</span>, <span itemprop="streetAddress">пр. Ямашева, д. 93, ТЦ «Савиново», 5 эт., м. Козья слобода</span>';
	$regions['dimitrovgrad.dpo-anta.ru']['phone'] = '+7 (987) 291-86-08';
	$regions['dimitrovgrad.dpo-anta.ru']['working_hours'] = 'с 10:00 до 20:00';
	$regions['dimitrovgrad.dpo-anta.ru']['ya_coord'] = '55.826511, 49.148924';

	$regions['dolgoprudnyy.dpo-anta.ru']['title']=' в Долгопрудном';
	$regions['dolgoprudnyy.dpo-anta.ru']['modal']=' Долгопрудный';
	$regions['dolgoprudnyy.dpo-anta.ru']['address'] = '<span itemprop="addressLocality">г. Долгопрудный</span>, <span itemprop="streetAddress">пр. Ямашева, д. 93, ТЦ «Савиново», 5 эт., м. Козья слобода</span>';
	$regions['dolgoprudnyy.dpo-anta.ru']['phone'] = '+7 (987) 291-86-08';
	$regions['dolgoprudnyy.dpo-anta.ru']['working_hours'] = 'с 10:00 до 20:00';
	$regions['dolgoprudnyy.dpo-anta.ru']['ya_coord'] = '55.826511, 49.148924';

	$regions['domodedovo.dpo-anta.ru']['title']=' в Домодедове';
	$regions['domodedovo.dpo-anta.ru']['modal']=' Домодедово';
	$regions['domodedovo.dpo-anta.ru']['address'] = '<span itemprop="addressLocality">г. Домодедово</span>, <span itemprop="streetAddress">пр. Ямашева, д. 93, ТЦ «Савиново», 5 эт., м. Козья слобода</span>';
	$regions['domodedovo.dpo-anta.ru']['phone'] = '+7 (987) 291-86-08';
	$regions['domodedovo.dpo-anta.ru']['working_hours'] = 'с 10:00 до 20:00';
	$regions['domodedovo.dpo-anta.ru']['ya_coord'] = '55.826511, 49.148924';

	$regions['evpatoriya.dpo-anta.ru']['title']=' в Евпатории';
	$regions['evpatoriya.dpo-anta.ru']['modal']=' Евпатория';
	$regions['evpatoriya.dpo-anta.ru']['address'] = '<span itemprop="addressLocality">г. Евпатория</span>, <span itemprop="streetAddress">пр. Ямашева, д. 93, ТЦ «Савиново», 5 эт., м. Козья слобода</span>';
	$regions['evpatoriya.dpo-anta.ru']['phone'] = '+7 (987) 291-86-08';
	$regions['evpatoriya.dpo-anta.ru']['working_hours'] = 'с 10:00 до 20:00';
	$regions['evpatoriya.dpo-anta.ru']['ya_coord'] = '55.826511, 49.148924';

	$regions['ekb.dpo-anta.ru']['title']=' в Екатеринбурге';
	$regions['ekb.dpo-anta.ru']['modal']=' Екатеринбург';
	$regions['ekb.dpo-anta.ru']['address'] = '<span itemprop="addressLocality">г. Екатеринбург</span>, <span itemprop="streetAddress">пр. Ямашева, д. 93, ТЦ «Савиново», 5 эт., м. Козья слобода</span>';
	$regions['ekb.dpo-anta.ru']['phone'] = '+7 (987) 291-86-08';
	$regions['ekb.dpo-anta.ru']['working_hours'] = 'с 10:00 до 20:00';
	$regions['ekb.dpo-anta.ru']['ya_coord'] = '55.826511, 49.148924';

	$regions['elets.dpo-anta.ru']['title']=' в Ельце';
	$regions['elets.dpo-anta.ru']['modal']=' Елец';
	$regions['elets.dpo-anta.ru']['address'] = '<span itemprop="addressLocality">г. Елец</span>, <span itemprop="streetAddress">пр. Ямашева, д. 93, ТЦ «Савиново», 5 эт., м. Козья слобода</span>';
	$regions['elets.dpo-anta.ru']['phone'] = '+7 (987) 291-86-08';
	$regions['elets.dpo-anta.ru']['working_hours'] = 'с 10:00 до 20:00';
	$regions['elets.dpo-anta.ru']['ya_coord'] = '55.826511, 49.148924';

	$regions['essentuki.dpo-anta.ru']['title']=' в Ессентуках';
	$regions['essentuki.dpo-anta.ru']['modal']=' Ессентуки';
	$regions['essentuki.dpo-anta.ru']['address'] = '<span itemprop="addressLocality">г. Ессентуки</span>, <span itemprop="streetAddress">пр. Ямашева, д. 93, ТЦ «Савиново», 5 эт., м. Козья слобода</span>';
	$regions['essentuki.dpo-anta.ru']['phone'] = '+7 (987) 291-86-08';
	$regions['essentuki.dpo-anta.ru']['working_hours'] = 'с 10:00 до 20:00';
	$regions['essentuki.dpo-anta.ru']['ya_coord'] = '55.826511, 49.148924';

	$regions['zheleznogorsk.dpo-anta.ru']['title']=' в Железногорске';
	$regions['zheleznogorsk.dpo-anta.ru']['modal']=' Железногорск';
	$regions['zheleznogorsk.dpo-anta.ru']['address'] = '<span itemprop="addressLocality">г. Железногорск</span>, <span itemprop="streetAddress">пр. Ямашева, д. 93, ТЦ «Савиново», 5 эт., м. Козья слобода</span>';
	$regions['zheleznogorsk.dpo-anta.ru']['phone'] = '+7 (987) 291-86-08';
	$regions['zheleznogorsk.dpo-anta.ru']['working_hours'] = 'с 10:00 до 20:00';
	$regions['zheleznogorsk.dpo-anta.ru']['ya_coord'] = '55.826511, 49.148924';

	$regions['zhukovskiy.dpo-anta.ru']['title']=' в Жуковском';
	$regions['zhukovskiy.dpo-anta.ru']['modal']=' Жуковский';
	$regions['zhukovskiy.dpo-anta.ru']['address'] = '<span itemprop="addressLocality">г. Жуковский</span>, <span itemprop="streetAddress">пр. Ямашева, д. 93, ТЦ «Савиново», 5 эт., м. Козья слобода</span>';
	$regions['zhukovskiy.dpo-anta.ru']['phone'] = '+7 (987) 291-86-08';
	$regions['zhukovskiy.dpo-anta.ru']['working_hours'] = 'с 10:00 до 20:00';
	$regions['zhukovskiy.dpo-anta.ru']['ya_coord'] = '55.826511, 49.148924';

	$regions['zlatoust.dpo-anta.ru']['title']=' в Златоусте';
	$regions['zlatoust.dpo-anta.ru']['modal']=' Златоуст';
	$regions['zlatoust.dpo-anta.ru']['address'] = '<span itemprop="addressLocality">г. Златоуст</span>, <span itemprop="streetAddress">пр. Ямашева, д. 93, ТЦ «Савиново», 5 эт., м. Козья слобода</span>';
	$regions['zlatoust.dpo-anta.ru']['phone'] = '+7 (987) 291-86-08';
	$regions['zlatoust.dpo-anta.ru']['working_hours'] = 'с 10:00 до 20:00';
	$regions['zlatoust.dpo-anta.ru']['ya_coord'] = '55.826511, 49.148924';

	$regions['ivanovo.dpo-anta.ru']['title']=' в Иванове';
	$regions['ivanovo.dpo-anta.ru']['modal']=' Иваново';
	$regions['ivanovo.dpo-anta.ru']['address'] = '<span itemprop="addressLocality">г. Иваново</span>, <span itemprop="streetAddress">пр. Ямашева, д. 93, ТЦ «Савиново», 5 эт., м. Козья слобода</span>';
	$regions['ivanovo.dpo-anta.ru']['phone'] = '+7 (987) 291-86-08';
	$regions['ivanovo.dpo-anta.ru']['working_hours'] = 'с 10:00 до 20:00';
	$regions['ivanovo.dpo-anta.ru']['ya_coord'] = '55.826511, 49.148924';

	$regions['izhevsk.dpo-anta.ru']['title']=' в Ижевске';
	$regions['izhevsk.dpo-anta.ru']['modal']=' Ижевск';
	$regions['izhevsk.dpo-anta.ru']['address'] = '<span itemprop="addressLocality">г. Ижевск</span>, <span itemprop="streetAddress">пр. Ямашева, д. 93, ТЦ «Савиново», 5 эт., м. Козья слобода</span>';
	$regions['izhevsk.dpo-anta.ru']['phone'] = '+7 (987) 291-86-08';
	$regions['izhevsk.dpo-anta.ru']['working_hours'] = 'с 10:00 до 20:00';
	$regions['izhevsk.dpo-anta.ru']['ya_coord'] = '55.826511, 49.148924';

	$regions['yoshkar-ola.dpo-anta.ru']['title']=' в Йошкар-Оле';
	$regions['yoshkar-ola.dpo-anta.ru']['modal']=' Йошкар-Ола';
	$regions['yoshkar-ola.dpo-anta.ru']['address'] = '<span itemprop="addressLocality">г. Йошкар-Ола</span>, <span itemprop="streetAddress">пр. Ямашева, д. 93, ТЦ «Савиново», 5 эт., м. Козья слобода</span>';
	$regions['yoshkar-ola.dpo-anta.ru']['phone'] = '+7 (987) 291-86-08';
	$regions['yoshkar-ola.dpo-anta.ru']['working_hours'] = 'с 10:00 до 20:00';
	$regions['yoshkar-ola.dpo-anta.ru']['ya_coord'] = '55.826511, 49.148924';

	$regions['irkutsk.dpo-anta.ru']['title']=' в Иркутске';
	$regions['irkutsk.dpo-anta.ru']['modal']=' Иркутск';
	$regions['irkutsk.dpo-anta.ru']['address'] = '<span itemprop="addressLocality">г. Иркутск</span>, <span itemprop="streetAddress">пр. Ямашева, д. 93, ТЦ «Савиново», 5 эт., м. Козья слобода</span>';
	$regions['irkutsk.dpo-anta.ru']['phone'] = '+7 (987) 291-86-08';
	$regions['irkutsk.dpo-anta.ru']['working_hours'] = 'с 10:00 до 20:00';
	$regions['irkutsk.dpo-anta.ru']['ya_coord'] = '55.826511, 49.148924';

	$regions['kazan.dpo-anta.ru']['title']=' в Казани';
	$regions['kazan.dpo-anta.ru']['modal']=' Казань';
	$regions['kazan.dpo-anta.ru']['address'] = '<span itemprop="addressLocality">г. Казань</span>, <span itemprop="streetAddress">пр. Ямашева, д. 93, ТЦ «Савиново», 5 эт., м. Козья слобода</span>';
	$regions['kazan.dpo-anta.ru']['phone'] = '+7 (987) 291-86-08';
	$regions['kazan.dpo-anta.ru']['working_hours'] = 'с 10:00 до 20:00';
	$regions['kazan.dpo-anta.ru']['ya_coord'] = '55.826511, 49.148924';

	$regions['kaliningrad.dpo-anta.ru']['title']=' в Калининграде';
	$regions['kaliningrad.dpo-anta.ru']['modal']=' Калининград';
	$regions['kaliningrad.dpo-anta.ru']['address'] = '<span itemprop="addressLocality">г. Калининград</span>, <span itemprop="streetAddress">пр. Ямашева, д. 93, ТЦ «Савиново», 5 эт., м. Козья слобода</span>';
	$regions['kaliningrad.dpo-anta.ru']['phone'] = '+7 (987) 291-86-08';
	$regions['kaliningrad.dpo-anta.ru']['working_hours'] = 'с 10:00 до 20:00';
	$regions['kaliningrad.dpo-anta.ru']['ya_coord'] = '55.826511, 49.148924';

	$regions['kaluga.dpo-anta.ru']['title']=' в Калуге';
	$regions['kaluga.dpo-anta.ru']['modal']=' Калуга';
	$regions['kaluga.dpo-anta.ru']['address'] = '<span itemprop="addressLocality">г. Калуга</span>, <span itemprop="streetAddress">пр. Ямашева, д. 93, ТЦ «Савиново», 5 эт., м. Козья слобода</span>';
	$regions['kaluga.dpo-anta.ru']['phone'] = '+7 (987) 291-86-08';
	$regions['kaluga.dpo-anta.ru']['working_hours'] = 'с 10:00 до 20:00';
	$regions['kaluga.dpo-anta.ru']['ya_coord'] = '55.826511, 49.148924';

	$regions['kamensk-uralskiy.dpo-anta.ru']['title']=' в Каменске-Уральском';
	$regions['kamensk-uralskiy.dpo-anta.ru']['modal']=' Каменск-Уральский';
	$regions['kamensk-uralskiy.dpo-anta.ru']['address'] = '<span itemprop="addressLocality">г. Каменск-Уральский</span>, <span itemprop="streetAddress">пр. Ямашева, д. 93, ТЦ «Савиново», 5 эт., м. Козья слобода</span>';
	$regions['kamensk-uralskiy.dpo-anta.ru']['phone'] = '+7 (987) 291-86-08';
	$regions['kamensk-uralskiy.dpo-anta.ru']['working_hours'] = 'с 10:00 до 20:00';
	$regions['kamensk-uralskiy.dpo-anta.ru']['ya_coord'] = '55.826511, 49.148924';

	$regions['kamyshin.dpo-anta.ru']['title']=' в Камышине';
	$regions['kamyshin.dpo-anta.ru']['modal']=' Камышин';
	$regions['kamyshin.dpo-anta.ru']['address'] = '<span itemprop="addressLocality">г. Камышин</span>, <span itemprop="streetAddress">пр. Ямашева, д. 93, ТЦ «Савиново», 5 эт., м. Козья слобода</span>';
	$regions['kamyshin.dpo-anta.ru']['phone'] = '+7 (987) 291-86-08';
	$regions['kamyshin.dpo-anta.ru']['working_hours'] = 'с 10:00 до 20:00';
	$regions['kamyshin.dpo-anta.ru']['ya_coord'] = '55.826511, 49.148924';

	$regions['kaspiysk.dpo-anta.ru']['title']=' в Каспийске';
	$regions['kaspiysk.dpo-anta.ru']['modal']=' Каспийск';
	$regions['kaspiysk.dpo-anta.ru']['address'] = '<span itemprop="addressLocality">г. Каспийск</span>, <span itemprop="streetAddress">пр. Ямашева, д. 93, ТЦ «Савиново», 5 эт., м. Козья слобода</span>';
	$regions['kaspiysk.dpo-anta.ru']['phone'] = '+7 (987) 291-86-08';
	$regions['kaspiysk.dpo-anta.ru']['working_hours'] = 'с 10:00 до 20:00';
	$regions['kaspiysk.dpo-anta.ru']['ya_coord'] = '55.826511, 49.148924';

	$regions['kemerovo.dpo-anta.ru']['title']=' в Кемерове';
	$regions['kemerovo.dpo-anta.ru']['modal']=' Кемерово';
	$regions['kemerovo.dpo-anta.ru']['address'] = '<span itemprop="addressLocality">г. Кемерово</span>, <span itemprop="streetAddress">пр. Ямашева, д. 93, ТЦ «Савиново», 5 эт., м. Козья слобода</span>';
	$regions['kemerovo.dpo-anta.ru']['phone'] = '+7 (987) 291-86-08';
	$regions['kemerovo.dpo-anta.ru']['working_hours'] = 'с 10:00 до 20:00';
	$regions['kemerovo.dpo-anta.ru']['ya_coord'] = '55.826511, 49.148924';

	$regions['kerch.dpo-anta.ru']['title']=' в Керчи';
	$regions['kerch.dpo-anta.ru']['modal']=' Керчь';
	$regions['kerch.dpo-anta.ru']['address'] = '<span itemprop="addressLocality">г. Керчь</span>, <span itemprop="streetAddress">пр. Ямашева, д. 93, ТЦ «Савиново», 5 эт., м. Козья слобода</span>';
	$regions['kerch.dpo-anta.ru']['phone'] = '+7 (987) 291-86-08';
	$regions['kerch.dpo-anta.ru']['working_hours'] = 'с 10:00 до 20:00';
	$regions['kerch.dpo-anta.ru']['ya_coord'] = '55.826511, 49.148924';

	$regions['kirov.dpo-anta.ru']['title']=' в Кирове';
	$regions['kirov.dpo-anta.ru']['modal']=' Киров';
	$regions['kirov.dpo-anta.ru']['address'] = '<span itemprop="addressLocality">г. Киров</span>, <span itemprop="streetAddress">пр. Ямашева, д. 93, ТЦ «Савиново», 5 эт., м. Козья слобода</span>';
	$regions['kirov.dpo-anta.ru']['phone'] = '+7 (987) 291-86-08';
	$regions['kirov.dpo-anta.ru']['working_hours'] = 'с 10:00 до 20:00';
	$regions['kirov.dpo-anta.ru']['ya_coord'] = '55.826511, 49.148924';

	$regions['kislovodsk.dpo-anta.ru']['title']=' в Кисловодске';
	$regions['kislovodsk.dpo-anta.ru']['modal']=' Кисловодск';
	$regions['kislovodsk.dpo-anta.ru']['address'] = '<span itemprop="addressLocality">г. Кисловодск</span>, <span itemprop="streetAddress">пр. Ямашева, д. 93, ТЦ «Савиново», 5 эт., м. Козья слобода</span>';
	$regions['kislovodsk.dpo-anta.ru']['phone'] = '+7 (987) 291-86-08';
	$regions['kislovodsk.dpo-anta.ru']['working_hours'] = 'с 10:00 до 20:00';
	$regions['kislovodsk.dpo-anta.ru']['ya_coord'] = '55.826511, 49.148924';

	$regions['kovrov.dpo-anta.ru']['title']=' в Коврове';
	$regions['kovrov.dpo-anta.ru']['modal']=' Ковров';
	$regions['kovrov.dpo-anta.ru']['address'] = '<span itemprop="addressLocality">г. Ковров</span>, <span itemprop="streetAddress">пр. Ямашева, д. 93, ТЦ «Савиново», 5 эт., м. Козья слобода</span>';
	$regions['kovrov.dpo-anta.ru']['phone'] = '+7 (987) 291-86-08';
	$regions['kovrov.dpo-anta.ru']['working_hours'] = 'с 10:00 до 20:00';
	$regions['kovrov.dpo-anta.ru']['ya_coord'] = '55.826511, 49.148924';

	$regions['kolomna.dpo-anta.ru']['title']=' в Коломне';
	$regions['kolomna.dpo-anta.ru']['modal']=' Коломна';
	$regions['kolomna.dpo-anta.ru']['address'] = '<span itemprop="addressLocality">г. Коломна</span>, <span itemprop="streetAddress">пр. Ямашева, д. 93, ТЦ «Савиново», 5 эт., м. Козья слобода</span>';
	$regions['kolomna.dpo-anta.ru']['phone'] = '+7 (987) 291-86-08';
	$regions['kolomna.dpo-anta.ru']['working_hours'] = 'с 10:00 до 20:00';
	$regions['kolomna.dpo-anta.ru']['ya_coord'] = '55.826511, 49.148924';

	$regions['komsomolsk-na-amure.dpo-anta.ru']['title']=' в Комсомольске-на-Амуре';
	$regions['komsomolsk-na-amure.dpo-anta.ru']['modal']=' Комсомольск-на-Амуре';
	$regions['komsomolsk-na-amure.dpo-anta.ru']['address'] = '<span itemprop="addressLocality">г. Комсомольск-на-Амуре</span>, <span itemprop="streetAddress">пр. Ямашева, д. 93, ТЦ «Савиново», 5 эт., м. Козья слобода</span>';
	$regions['komsomolsk-na-amure.dpo-anta.ru']['phone'] = '+7 (987) 291-86-08';
	$regions['komsomolsk-na-amure.dpo-anta.ru']['working_hours'] = 'с 10:00 до 20:00';
	$regions['komsomolsk-na-amure.dpo-anta.ru']['ya_coord'] = '55.826511, 49.148924';

	$regions['kopeysk.dpo-anta.ru']['title']=' в Копейске';
	$regions['kopeysk.dpo-anta.ru']['modal']=' Копейск';
	$regions['kopeysk.dpo-anta.ru']['address'] = '<span itemprop="addressLocality">г. Копейск</span>, <span itemprop="streetAddress">пр. Ямашева, д. 93, ТЦ «Савиново», 5 эт., м. Козья слобода</span>';
	$regions['kopeysk.dpo-anta.ru']['phone'] = '+7 (987) 291-86-08';
	$regions['kopeysk.dpo-anta.ru']['working_hours'] = 'с 10:00 до 20:00';
	$regions['kopeysk.dpo-anta.ru']['ya_coord'] = '55.826511, 49.148924';

	$regions['korolyev.dpo-anta.ru']['title']=' в Королеве';
	$regions['korolyev.dpo-anta.ru']['modal']=' Королев';
	$regions['korolyev.dpo-anta.ru']['address'] = '<span itemprop="addressLocality">г. Королев</span>, <span itemprop="streetAddress">пр. Ямашева, д. 93, ТЦ «Савиново», 5 эт., м. Козья слобода</span>';
	$regions['korolyev.dpo-anta.ru']['phone'] = '+7 (987) 291-86-08';
	$regions['korolyev.dpo-anta.ru']['working_hours'] = 'с 10:00 до 20:00';
	$regions['korolyev.dpo-anta.ru']['ya_coord'] = '55.826511, 49.148924';
	//61
	$regions['kostroma.dpo-anta.ru']['title']=' в Костроме';
	$regions['kostroma.dpo-anta.ru']['modal']=' Кострома';
	$regions['kostroma.dpo-anta.ru']['address'] = '<span itemprop="addressLocality">г. Кострома</span>, <span itemprop="streetAddress">пр. Ямашева, д. 93, ТЦ «Савиново», 5 эт., м. Козья слобода</span>';
	$regions['kostroma.dpo-anta.ru']['phone'] = '+7 (987) 291-86-08';
	$regions['kostroma.dpo-anta.ru']['working_hours'] = 'с 10:00 до 20:00';
	$regions['kostroma.dpo-anta.ru']['ya_coord'] = '55.826511, 49.148924';

	$regions['krasnogorsk.dpo-anta.ru']['title']=' в Красногорске';
	$regions['krasnogorsk.dpo-anta.ru']['modal']=' Красногорск';
	$regions['krasnogorsk.dpo-anta.ru']['address'] = '<span itemprop="addressLocality">г. Красногорск</span>, <span itemprop="streetAddress">пр. Ямашева, д. 93, ТЦ «Савиново», 5 эт., м. Козья слобода</span>';
	$regions['krasnogorsk.dpo-anta.ru']['phone'] = '+7 (987) 291-86-08';
	$regions['krasnogorsk.dpo-anta.ru']['working_hours'] = 'с 10:00 до 20:00';
	$regions['krasnogorsk.dpo-anta.ru']['ya_coord'] = '55.826511, 49.148924';

	$regions['krasnodar.dpo-anta.ru']['title']=' в Краснодаре';
	$regions['krasnodar.dpo-anta.ru']['modal']=' Краснодар';
	$regions['krasnodar.dpo-anta.ru']['address'] = '<span itemprop="addressLocality">г. Краснодар</span>, <span itemprop="streetAddress">пр. Ямашева, д. 93, ТЦ «Савиново», 5 эт., м. Козья слобода</span>';
	$regions['krasnodar.dpo-anta.ru']['phone'] = '+7 (987) 291-86-08';
	$regions['krasnodar.dpo-anta.ru']['working_hours'] = 'с 10:00 до 20:00';
	$regions['krasnodar.dpo-anta.ru']['ya_coord'] = '55.826511, 49.148924';

	$regions['krasnoyarsk.dpo-anta.ru']['title']=' в Красноярске';
	$regions['krasnoyarsk.dpo-anta.ru']['modal']=' Красноярск';
	$regions['krasnoyarsk.dpo-anta.ru']['address'] = '<span itemprop="addressLocality">г. Красноярск</span>, <span itemprop="streetAddress">пр. Ямашева, д. 93, ТЦ «Савиново», 5 эт., м. Козья слобода</span>';
	$regions['krasnoyarsk.dpo-anta.ru']['phone'] = '+7 (987) 291-86-08';
	$regions['krasnoyarsk.dpo-anta.ru']['working_hours'] = 'с 10:00 до 20:00';
	$regions['krasnoyarsk.dpo-anta.ru']['ya_coord'] = '55.826511, 49.148924';

	$regions['kurgan.dpo-anta.ru']['title']=' в Кургане';
	$regions['kurgan.dpo-anta.ru']['modal']=' Курган';
	$regions['kurgan.dpo-anta.ru']['address'] = '<span itemprop="addressLocality">г. Курган</span>, <span itemprop="streetAddress">пр. Ямашева, д. 93, ТЦ «Савиново», 5 эт., м. Козья слобода</span>';
	$regions['kurgan.dpo-anta.ru']['phone'] = '+7 (987) 291-86-08';
	$regions['kurgan.dpo-anta.ru']['working_hours'] = 'с 10:00 до 20:00';
	$regions['kurgan.dpo-anta.ru']['ya_coord'] = '55.826511, 49.148924';

	$regions['kursk.dpo-anta.ru']['title']=' в Курске';
	$regions['kursk.dpo-anta.ru']['modal']=' Курск';
	$regions['kursk.dpo-anta.ru']['address'] = '<span itemprop="addressLocality">г. Курск</span>, <span itemprop="streetAddress">пр. Ямашева, д. 93, ТЦ «Савиново», 5 эт., м. Козья слобода</span>';
	$regions['kursk.dpo-anta.ru']['phone'] = '+7 (987) 291-86-08';
	$regions['kursk.dpo-anta.ru']['working_hours'] = 'с 10:00 до 20:00';
	$regions['kursk.dpo-anta.ru']['ya_coord'] = '55.826511, 49.148924';

	$regions['kyzyl.dpo-anta.ru']['title']=' в Кызыле';
	$regions['kyzyl.dpo-anta.ru']['modal']=' Кызыл';
	$regions['kyzyl.dpo-anta.ru']['address'] = '<span itemprop="addressLocality">г. Кызыл</span>, <span itemprop="streetAddress">пр. Ямашева, д. 93, ТЦ «Савиново», 5 эт., м. Козья слобода</span>';
	$regions['kyzyl.dpo-anta.ru']['phone'] = '+7 (987) 291-86-08';
	$regions['kyzyl.dpo-anta.ru']['working_hours'] = 'с 10:00 до 20:00';
	$regions['kyzyl.dpo-anta.ru']['ya_coord'] = '55.826511, 49.148924';

	$regions['lipetsk.dpo-anta.ru']['title']=' в Липецке';
	$regions['lipetsk.dpo-anta.ru']['modal']=' Липецк';
	$regions['lipetsk.dpo-anta.ru']['address'] = '<span itemprop="addressLocality">г. Липецке</span>, <span itemprop="streetAddress">пр. Ямашева, д. 93, ТЦ «Савиново», 5 эт., м. Козья слобода</span>';
	$regions['lipetsk.dpo-anta.ru']['phone'] = '+7 (987) 291-86-08';
	$regions['lipetsk.dpo-anta.ru']['working_hours'] = 'с 10:00 до 20:00';
	$regions['lipetsk.dpo-anta.ru']['ya_coord'] = '55.826511, 49.148924';

	$regions['lyubertsy.dpo-anta.ru']['title']=' в Люберцах';
	$regions['lyubertsy.dpo-anta.ru']['modal']=' Люберцы';
	$regions['lyubertsy.dpo-anta.ru']['address'] = '<span itemprop="addressLocality">г. Люберцы</span>, <span itemprop="streetAddress">пр. Ямашева, д. 93, ТЦ «Савиново», 5 эт., м. Козья слобода</span>';
	$regions['lyubertsy.dpo-anta.ru']['phone'] = '+7 (987) 291-86-08';
	$regions['lyubertsy.dpo-anta.ru']['working_hours'] = 'с 10:00 до 20:00';
	$regions['lyubertsy.dpo-anta.ru']['ya_coord'] = '55.826511, 49.148924';

	$regions['magnitogorsk.dpo-anta.ru']['title']=' в Магнитогорске';
	$regions['magnitogorsk.dpo-anta.ru']['modal']=' Магнитогорск';
	$regions['magnitogorsk.dpo-anta.ru']['address'] = '<span itemprop="addressLocality">г. Магнитогорск</span>, <span itemprop="streetAddress">пр. Ямашева, д. 93, ТЦ «Савиново», 5 эт., м. Козья слобода</span>';
	$regions['magnitogorsk.dpo-anta.ru']['phone'] = '+7 (987) 291-86-08';
	$regions['magnitogorsk.dpo-anta.ru']['working_hours'] = 'с 10:00 до 20:00';
	$regions['magnitogorsk.dpo-anta.ru']['ya_coord'] = '55.826511, 49.148924';

	$regions['maykop.dpo-anta.ru']['title']=' в Майкопе';
	$regions['maykop.dpo-anta.ru']['modal']=' Майкоп';
	$regions['maykop.dpo-anta.ru']['address'] = '<span itemprop="addressLocality">г. Майкоп</span>, <span itemprop="streetAddress">пр. Ямашева, д. 93, ТЦ «Савиново», 5 эт., м. Козья слобода</span>';
	$regions['maykop.dpo-anta.ru']['phone'] = '+7 (987) 291-86-08';
	$regions['maykop.dpo-anta.ru']['working_hours'] = 'с 10:00 до 20:00';
	$regions['maykop.dpo-anta.ru']['ya_coord'] = '55.826511, 49.148924';

	$regions['makhachkala.dpo-anta.ru']['title']=' в Махачкале';
	$regions['makhachkala.dpo-anta.ru']['modal']=' Махачкала';
	$regions['makhachkala.dpo-anta.ru']['address'] = '<span itemprop="addressLocality">г. Махачкала</span>, <span itemprop="streetAddress">пр. Ямашева, д. 93, ТЦ «Савиново», 5 эт., м. Козья слобода</span>';
	$regions['makhachkala.dpo-anta.ru']['phone'] = '+7 (987) 291-86-08';
	$regions['makhachkala.dpo-anta.ru']['working_hours'] = 'с 10:00 до 20:00';
	$regions['makhachkala.dpo-anta.ru']['ya_coord'] = '55.826511, 49.148924';

	$regions['miass.dpo-anta.ru']['title']=' в Миассе';
	$regions['miass.dpo-anta.ru']['modal']=' Миасс';
	$regions['miass.dpo-anta.ru']['address'] = '<span itemprop="addressLocality">г. Миасс</span>, <span itemprop="streetAddress">пр. Ямашева, д. 93, ТЦ «Савиново», 5 эт., м. Козья слобода</span>';
	$regions['miass.dpo-anta.ru']['phone'] = '+7 (987) 291-86-08';
	$regions['miass.dpo-anta.ru']['working_hours'] = 'с 10:00 до 20:00';
	$regions['miass.dpo-anta.ru']['ya_coord'] = '55.826511, 49.148924';

	$regions['murmansk.dpo-anta.ru']['title']=' в Мурманске';
	$regions['murmansk.dpo-anta.ru']['modal']=' Мурманск';
	$regions['murmansk.dpo-anta.ru']['address'] = '<span itemprop="addressLocality">г. Мурманск</span>, <span itemprop="streetAddress">пр. Ямашева, д. 93, ТЦ «Савиново», 5 эт., м. Козья слобода</span>';
	$regions['murmansk.dpo-anta.ru']['phone'] = '+7 (987) 291-86-08';
	$regions['murmansk.dpo-anta.ru']['working_hours'] = 'с 10:00 до 20:00';
	$regions['murmansk.dpo-anta.ru']['ya_coord'] = '55.826511, 49.148924';

	$regions['murom.dpo-anta.ru']['title']=' в Муроме';
	$regions['murom.dpo-anta.ru']['modal']=' Муром';
	$regions['murom.dpo-anta.ru']['address'] = '<span itemprop="addressLocality">г. Муром</span>, <span itemprop="streetAddress">пр. Ямашева, д. 93, ТЦ «Савиново», 5 эт., м. Козья слобода</span>';
	$regions['murom.dpo-anta.ru']['phone'] = '+7 (987) 291-86-08';
	$regions['murom.dpo-anta.ru']['working_hours'] = 'с 10:00 до 20:00';
	$regions['murom.dpo-anta.ru']['ya_coord'] = '55.826511, 49.148924';

	$regions['mytishchi.dpo-anta.ru']['title']=' в Мытищах';
	$regions['mytishchi.dpo-anta.ru']['modal']=' Мытищи';
	$regions['mytishchi.dpo-anta.ru']['address'] = '<span itemprop="addressLocality">г. Мытищи</span>, <span itemprop="streetAddress">пр. Ямашева, д. 93, ТЦ «Савиново», 5 эт., м. Козья слобода</span>';
	$regions['mytishchi.dpo-anta.ru']['phone'] = '+7 (987) 291-86-08';
	$regions['mytishchi.dpo-anta.ru']['working_hours'] = 'с 10:00 до 20:00';
	$regions['mytishchi.dpo-anta.ru']['ya_coord'] = '55.826511, 49.148924';

	$regions['naberezhnye-chelny.dpo-anta.ru']['title']=' в Набережных Челнах';
	$regions['naberezhnye-chelny.dpo-anta.ru']['modal']=' Набережные Челны';
	$regions['naberezhnye-chelny.dpo-anta.ru']['address'] = '<span itemprop="addressLocality">г. Набережные Челны</span>, <span itemprop="streetAddress">пр. Ямашева, д. 93, ТЦ «Савиново», 5 эт., м. Козья слобода</span>';
	$regions['naberezhnye-chelny.dpo-anta.ru']['phone'] = '+7 (987) 291-86-08';
	$regions['naberezhnye-chelny.dpo-anta.ru']['working_hours'] = 'с 10:00 до 20:00';
	$regions['naberezhnye-chelny.dpo-anta.ru']['ya_coord'] = '55.826511, 49.148924';

	$regions['nazran.dpo-anta.ru']['title']=' в Назрани';
	$regions['nazran.dpo-anta.ru']['modal']=' Назрань';
	$regions['nazran.dpo-anta.ru']['address'] = '<span itemprop="addressLocality">г. Назрань</span>, <span itemprop="streetAddress">пр. Ямашева, д. 93, ТЦ «Савиново», 5 эт., м. Козья слобода</span>';
	$regions['nazran.dpo-anta.ru']['phone'] = '+7 (987) 291-86-08';
	$regions['nazran.dpo-anta.ru']['working_hours'] = 'с 10:00 до 20:00';
	$regions['nazran.dpo-anta.ru']['ya_coord'] = '55.826511, 49.148924';

	$regions['nalchik.dpo-anta.ru']['title']=' в Нальчике';
	$regions['nalchik.dpo-anta.ru']['modal']=' Нальчик';
	$regions['nalchik.dpo-anta.ru']['address'] = '<span itemprop="addressLocality">г. Нальчик</span>, <span itemprop="streetAddress">пр. Ямашева, д. 93, ТЦ «Савиново», 5 эт., м. Козья слобода</span>';
	$regions['nalchik.dpo-anta.ru']['phone'] = '+7 (987) 291-86-08';
	$regions['nalchik.dpo-anta.ru']['working_hours'] = 'с 10:00 до 20:00';
	$regions['nalchik.dpo-anta.ru']['ya_coord'] = '55.826511, 49.148924';

	$regions['nakhodka.dpo-anta.ru']['title']=' в Находке';
	$regions['nakhodka.dpo-anta.ru']['modal']=' Находка';
	$regions['nakhodka.dpo-anta.ru']['address'] = '<span itemprop="addressLocality">г. Находка</span>, <span itemprop="streetAddress">пр. Ямашева, д. 93, ТЦ «Савиново», 5 эт., м. Козья слобода</span>';
	$regions['nakhodka.dpo-anta.ru']['phone'] = '+7 (987) 291-86-08';
	$regions['nakhodka.dpo-anta.ru']['working_hours'] = 'с 10:00 до 20:00';
	$regions['nakhodka.dpo-anta.ru']['ya_coord'] = '55.826511, 49.148924';
	//81
	$regions['nevinnomyssk.dpo-anta.ru']['title']=' в Невинномысске';
	$regions['nevinnomyssk.dpo-anta.ru']['modal']=' Невинномысск';
	$regions['nevinnomyssk.dpo-anta.ru']['address'] = '<span itemprop="addressLocality">г. Невинномысск</span>, <span itemprop="streetAddress">пр. Ямашева, д. 93, ТЦ «Савиново», 5 эт., м. Козья слобода</span>';
	$regions['nevinnomyssk.dpo-anta.ru']['phone'] = '+7 (987) 291-86-08';
	$regions['nevinnomyssk.dpo-anta.ru']['working_hours'] = 'с 10:00 до 20:00';
	$regions['nevinnomyssk.dpo-anta.ru']['ya_coord'] = '55.826511, 49.148924';

	$regions['neftekamsk.dpo-anta.ru']['title']=' в Нефтекамске';
	$regions['neftekamsk.dpo-anta.ru']['modal']=' Нефтекамск';
	$regions['neftekamsk.dpo-anta.ru']['address'] = '<span itemprop="addressLocality">г. Нефтекамск</span>, <span itemprop="streetAddress">пр. Ямашева, д. 93, ТЦ «Савиново», 5 эт., м. Козья слобода</span>';
	$regions['neftekamsk.dpo-anta.ru']['phone'] = '+7 (987) 291-86-08';
	$regions['neftekamsk.dpo-anta.ru']['working_hours'] = 'с 10:00 до 20:00';
	$regions['neftekamsk.dpo-anta.ru']['ya_coord'] = '55.826511, 49.148924';

	$regions['nefteyugansk.dpo-anta.ru']['title']=' в Нефтеюганске';
	$regions['nefteyugansk.dpo-anta.ru']['modal']=' Нефтеюганск';
	$regions['nefteyugansk.dpo-anta.ru']['address'] = '<span itemprop="addressLocality">г. Нефтеюганск</span>, <span itemprop="streetAddress">пр. Ямашева, д. 93, ТЦ «Савиново», 5 эт., м. Козья слобода</span>';
	$regions['nefteyugansk.dpo-anta.ru']['phone'] = '+7 (987) 291-86-08';
	$regions['nefteyugansk.dpo-anta.ru']['working_hours'] = 'с 10:00 до 20:00';
	$regions['nefteyugansk.dpo-anta.ru']['ya_coord'] = '55.826511, 49.148924';

	$regions['nizhnevartovsk.dpo-anta.ru']['title']=' в Нижневартовске';
	$regions['nizhnevartovsk.dpo-anta.ru']['modal']=' Нижневартовск';
	$regions['nizhnevartovsk.dpo-anta.ru']['address'] = '<span itemprop="addressLocality">г. Нижневартовск</span>, <span itemprop="streetAddress">пр. Ямашева, д. 93, ТЦ «Савиново», 5 эт., м. Козья слобода</span>';
	$regions['nizhnevartovsk.dpo-anta.ru']['phone'] = '+7 (987) 291-86-08';
	$regions['nizhnevartovsk.dpo-anta.ru']['working_hours'] = 'с 10:00 до 20:00';
	$regions['nizhnevartovsk.dpo-anta.ru']['ya_coord'] = '55.826511, 49.148924';

	$regions['nizhnekamsk.dpo-anta.ru']['title']=' в Нижнекамске';
	$regions['nizhnekamsk.dpo-anta.ru']['modal']=' Нижнекамск';
	$regions['nizhnekamsk.dpo-anta.ru']['address'] = '<span itemprop="addressLocality">г. Нижнекамск</span>, <span itemprop="streetAddress">пр. Ямашева, д. 93, ТЦ «Савиново», 5 эт., м. Козья слобода</span>';
	$regions['nizhnekamsk.dpo-anta.ru']['phone'] = '+7 (987) 291-86-08';
	$regions['nizhnekamsk.dpo-anta.ru']['working_hours'] = 'с 10:00 до 20:00';
	$regions['nizhnekamsk.dpo-anta.ru']['ya_coord'] = '55.826511, 49.148924';

	$regions['nn.dpo-anta.ru']['title']=' в Нижнем Новгороде';
	$regions['nn.dpo-anta.ru']['modal']=' Нижний Новгород';
	$regions['nn.dpo-anta.ru']['address'] = '<span itemprop="addressLocality">г. Нижний Новгород</span>, <span itemprop="streetAddress">пр. Ямашева, д. 93, ТЦ «Савиново», 5 эт., м. Козья слобода</span>';
	$regions['nn.dpo-anta.ru']['phone'] = '+7 (987) 291-86-08';
	$regions['nn.dpo-anta.ru']['working_hours'] = 'с 10:00 до 20:00';
	$regions['nn.dpo-anta.ru']['ya_coord'] = '55.826511, 49.148924';

	$regions['nizhniy-tagil.dpo-anta.ru']['title']=' в Нижнем Тагиле';
	$regions['nizhniy-tagil.dpo-anta.ru']['modal']=' Нижний Тагил';
	$regions['nizhniy-tagil.dpo-anta.ru']['address'] = '<span itemprop="addressLocality">г. Нижний Тагил</span>, <span itemprop="streetAddress">пр. Ямашева, д. 93, ТЦ «Савиново», 5 эт., м. Козья слобода</span>';
	$regions['nizhniy-tagil.dpo-anta.ru']['phone'] = '+7 (987) 291-86-08';
	$regions['nizhniy-tagil.dpo-anta.ru']['working_hours'] = 'с 10:00 до 20:00';
	$regions['nizhniy-tagil.dpo-anta.ru']['ya_coord'] = '55.826511, 49.148924';

	$regions['novokuznetsk.dpo-anta.ru']['title']=' в Новокузнецке';
	$regions['novokuznetsk.dpo-anta.ru']['modal']=' Новокузнецк';
	$regions['novokuznetsk.dpo-anta.ru']['address'] = '<span itemprop="addressLocality">г. Новокузнецк</span>, <span itemprop="streetAddress">пр. Ямашева, д. 93, ТЦ «Савиново», 5 эт., м. Козья слобода</span>';
	$regions['novokuznetsk.dpo-anta.ru']['phone'] = '+7 (987) 291-86-08';
	$regions['novokuznetsk.dpo-anta.ru']['working_hours'] = 'с 10:00 до 20:00';
	$regions['novokuznetsk.dpo-anta.ru']['ya_coord'] = '55.826511, 49.148924';

	$regions['novokuybyshevsk.dpo-anta.ru']['title']=' в Новокуйбышевске';
	$regions['novokuybyshevsk.dpo-anta.ru']['modal']=' Новокуйбышевск';
	$regions['novokuybyshevsk.dpo-anta.ru']['address'] = '<span itemprop="addressLocality">г. Новокуйбышевск</span>, <span itemprop="streetAddress">пр. Ямашева, д. 93, ТЦ «Савиново», 5 эт., м. Козья слобода</span>';
	$regions['novokuybyshevsk.dpo-anta.ru']['phone'] = '+7 (987) 291-86-08';
	$regions['novokuybyshevsk.dpo-anta.ru']['working_hours'] = 'с 10:00 до 20:00';
	$regions['novokuybyshevsk.dpo-anta.ru']['ya_coord'] = '55.826511, 49.148924';

	$regions['novomoskovsk.dpo-anta.ru']['title']=' в Новомосковске';
	$regions['novomoskovsk.dpo-anta.ru']['modal']=' Новомосковск';
	$regions['novomoskovsk.dpo-anta.ru']['address'] = '<span itemprop="addressLocality">г. Новомосковск</span>, <span itemprop="streetAddress">пр. Ямашева, д. 93, ТЦ «Савиново», 5 эт., м. Козья слобода</span>';
	$regions['novomoskovsk.dpo-anta.ru']['phone'] = '+7 (987) 291-86-08';
	$regions['novomoskovsk.dpo-anta.ru']['working_hours'] = 'с 10:00 до 20:00';
	$regions['novomoskovsk.dpo-anta.ru']['ya_coord'] = '55.826511, 49.148924';

	$regions['novorossiysk.dpo-anta.ru']['title']=' в Новороссийске';
	$regions['novorossiysk.dpo-anta.ru']['modal']=' Новороссийск';
	$regions['novorossiysk.dpo-anta.ru']['address'] = '<span itemprop="addressLocality">г. Новороссийск</span>, <span itemprop="streetAddress">пр. Ямашева, д. 93, ТЦ «Савиново», 5 эт., м. Козья слобода</span>';
	$regions['novorossiysk.dpo-anta.ru']['phone'] = '+7 (987) 291-86-08';
	$regions['novorossiysk.dpo-anta.ru']['working_hours'] = 'с 10:00 до 20:00';
	$regions['novorossiysk.dpo-anta.ru']['ya_coord'] = '55.826511, 49.148924';

	$regions['novosibirsk.dpo-anta.ru']['title']=' в Новосибирске';
	$regions['novosibirsk.dpo-anta.ru']['modal']=' Новосибирск';
	$regions['novosibirsk.dpo-anta.ru']['address'] = '<span itemprop="addressLocality">г. Новосибирск</span>, <span itemprop="streetAddress">пр. Ямашева, д. 93, ТЦ «Савиново», 5 эт., м. Козья слобода</span>';
	$regions['novosibirsk.dpo-anta.ru']['phone'] = '+7 (987) 291-86-08';
	$regions['novosibirsk.dpo-anta.ru']['working_hours'] = 'с 10:00 до 20:00';
	$regions['novosibirsk.dpo-anta.ru']['ya_coord'] = '55.826511, 49.148924';

	$regions['novocheboksarsk.dpo-anta.ru']['title']=' в Новочебоксарске';
	$regions['novocheboksarsk.dpo-anta.ru']['modal']=' Новочебоксарск';
	$regions['novocheboksarsk.dpo-anta.ru']['address'] = '<span itemprop="addressLocality">г. Новочебоксарск</span>, <span itemprop="streetAddress">пр. Ямашева, д. 93, ТЦ «Савиново», 5 эт., м. Козья слобода</span>';
	$regions['novocheboksarsk.dpo-anta.ru']['phone'] = '+7 (987) 291-86-08';
	$regions['novocheboksarsk.dpo-anta.ru']['working_hours'] = 'с 10:00 до 20:00';
	$regions['novocheboksarsk.dpo-anta.ru']['ya_coord'] = '55.826511, 49.148924';

	$regions['novocherkassk.dpo-anta.ru']['title']=' в Новочеркасске';
	$regions['novocherkassk.dpo-anta.ru']['modal']=' Новочеркасск';
	$regions['novocherkassk.dpo-anta.ru']['address'] = '<span itemprop="addressLocality">г. Новочеркасск</span>, <span itemprop="streetAddress">пр. Ямашева, д. 93, ТЦ «Савиново», 5 эт., м. Козья слобода</span>';
	$regions['novocherkassk.dpo-anta.ru']['phone'] = '+7 (987) 291-86-08';
	$regions['novocherkassk.dpo-anta.ru']['working_hours'] = 'с 10:00 до 20:00';
	$regions['novocherkassk.dpo-anta.ru']['ya_coord'] = '55.826511, 49.148924';

	$regions['novoshakhtinsk.dpo-anta.ru']['title']=' в Новошахтинске';
	$regions['novoshakhtinsk.dpo-anta.ru']['modal']=' Новошахтинск';
	$regions['novoshakhtinsk.dpo-anta.ru']['address'] = '<span itemprop="addressLocality">г. Новошахтинск</span>, <span itemprop="streetAddress">пр. Ямашева, д. 93, ТЦ «Савиново», 5 эт., м. Козья слобода</span>';
	$regions['novoshakhtinsk.dpo-anta.ru']['phone'] = '+7 (987) 291-86-08';
	$regions['novoshakhtinsk.dpo-anta.ru']['working_hours'] = 'с 10:00 до 20:00';
	$regions['novoshakhtinsk.dpo-anta.ru']['ya_coord'] = '55.826511, 49.148924';

	$regions['novyy-urengoy.dpo-anta.ru']['title']=' в Новом Уренгое';
	$regions['novyy-urengoy.dpo-anta.ru']['modal']=' Новый Уренгой';
	$regions['novyy-urengoy.dpo-anta.ru']['address'] = '<span itemprop="addressLocality">г. Новый Уренгой</span>, <span itemprop="streetAddress">пр. Ямашева, д. 93, ТЦ «Савиново», 5 эт., м. Козья слобода</span>';
	$regions['novyy-urengoy.dpo-anta.ru']['phone'] = '+7 (987) 291-86-08';
	$regions['novyy-urengoy.dpo-anta.ru']['working_hours'] = 'с 10:00 до 20:00';
	$regions['novyy-urengoy.dpo-anta.ru']['ya_coord'] = '55.826511, 49.148924';

	$regions['noginsk.dpo-anta.ru']['title']=' в Ногинске';
	$regions['noginsk.dpo-anta.ru']['modal']=' Ногинск';
	$regions['noginsk.dpo-anta.ru']['address'] = '<span itemprop="addressLocality">г. Ногинск</span>, <span itemprop="streetAddress">пр. Ямашева, д. 93, ТЦ «Савиново», 5 эт., м. Козья слобода</span>';
	$regions['noginsk.dpo-anta.ru']['phone'] = '+7 (987) 291-86-08';
	$regions['noginsk.dpo-anta.ru']['working_hours'] = 'с 10:00 до 20:00';
	$regions['noginsk.dpo-anta.ru']['ya_coord'] = '55.826511, 49.148924';

	$regions['norilsk.dpo-anta.ru']['title']=' в Норильске';
	$regions['norilsk.dpo-anta.ru']['modal']=' Норильск';
	$regions['norilsk.dpo-anta.ru']['address'] = '<span itemprop="addressLocality">г. Норильск</span>, <span itemprop="streetAddress">пр. Ямашева, д. 93, ТЦ «Савиново», 5 эт., м. Козья слобода</span>';
	$regions['norilsk.dpo-anta.ru']['phone'] = '+7 (987) 291-86-08';
	$regions['norilsk.dpo-anta.ru']['working_hours'] = 'с 10:00 до 20:00';
	$regions['norilsk.dpo-anta.ru']['ya_coord'] = '55.826511, 49.148924';

	$regions['noyabrsk.dpo-anta.ru']['title']=' в Ноябрьске';
	$regions['noyabrsk.dpo-anta.ru']['modal']=' Ноябрьск';
	$regions['noyabrsk.dpo-anta.ru']['address'] = '<span itemprop="addressLocality">г. Ноябрьск</span>, <span itemprop="streetAddress">пр. Ямашева, д. 93, ТЦ «Савиново», 5 эт., м. Козья слобода</span>';
	$regions['noyabrsk.dpo-anta.ru']['phone'] = '+7 (987) 291-86-08';
	$regions['noyabrsk.dpo-anta.ru']['working_hours'] = 'с 10:00 до 20:00';
	$regions['noyabrsk.dpo-anta.ru']['ya_coord'] = '55.826511, 49.148924';

	$regions['obninsk.dpo-anta.ru']['title']=' в Обнинске';
	$regions['obninsk.dpo-anta.ru']['modal']=' Обнинск';
	$regions['obninsk.dpo-anta.ru']['address'] = '<span itemprop="addressLocality">г. Обнинск</span>, <span itemprop="streetAddress">пр. Ямашева, д. 93, ТЦ «Савиново», 5 эт., м. Козья слобода</span>';
	$regions['obninsk.dpo-anta.ru']['phone'] = '+7 (987) 291-86-08';
	$regions['obninsk.dpo-anta.ru']['working_hours'] = 'с 10:00 до 20:00';
	$regions['obninsk.dpo-anta.ru']['ya_coord'] = '55.826511, 49.148924';
	//101
	$regions['odintsovo.dpo-anta.ru']['title']=' в Одинцове';
	$regions['odintsovo.dpo-anta.ru']['modal']=' Одинцово';
	$regions['odintsovo.dpo-anta.ru']['address'] = '<span itemprop="addressLocality">г. Одинцово</span>, <span itemprop="streetAddress">пр. Ямашева, д. 93, ТЦ «Савиново», 5 эт., м. Козья слобода</span>';
	$regions['odintsovo.dpo-anta.ru']['phone'] = '+7 (987) 291-86-08';
	$regions['odintsovo.dpo-anta.ru']['working_hours'] = 'с 10:00 до 20:00';
	$regions['odintsovo.dpo-anta.ru']['ya_coord'] = '55.826511, 49.148924';

	$regions['oktyabrskiy.dpo-anta.ru']['title']=' в Октябрьском';
	$regions['oktyabrskiy.dpo-anta.ru']['modal']=' Октябрьский';
	$regions['oktyabrskiy.dpo-anta.ru']['address'] = '<span itemprop="addressLocality">г. Октябрьский</span>, <span itemprop="streetAddress">пр. Ямашева, д. 93, ТЦ «Савиново», 5 эт., м. Козья слобода</span>';
	$regions['oktyabrskiy.dpo-anta.ru']['phone'] = '+7 (987) 291-86-08';
	$regions['oktyabrskiy.dpo-anta.ru']['working_hours'] = 'с 10:00 до 20:00';
	$regions['oktyabrskiy.dpo-anta.ru']['ya_coord'] = '55.826511, 49.148924';

	$regions['omsk.dpo-anta.ru']['title']=' в Омске';
	$regions['omsk.dpo-anta.ru']['modal']=' Омск';
	$regions['omsk.dpo-anta.ru']['address'] = '<span itemprop="addressLocality">г. Омск</span>, <span itemprop="streetAddress">пр. Ямашева, д. 93, ТЦ «Савиново», 5 эт., м. Козья слобода</span>';
	$regions['omsk.dpo-anta.ru']['phone'] = '+7 (987) 291-86-08';
	$regions['omsk.dpo-anta.ru']['working_hours'] = 'с 10:00 до 20:00';
	$regions['omsk.dpo-anta.ru']['ya_coord'] = '55.826511, 49.148924';

	$regions['oryel.dpo-anta.ru']['title']=' в Орле';
	$regions['oryel.dpo-anta.ru']['modal']=' Орел';
	$regions['oryel.dpo-anta.ru']['address'] = '<span itemprop="addressLocality">г. Орел</span>, <span itemprop="streetAddress">пр. Ямашева, д. 93, ТЦ «Савиново», 5 эт., м. Козья слобода</span>';
	$regions['oryel.dpo-anta.ru']['phone'] = '+7 (987) 291-86-08';
	$regions['oryel.dpo-anta.ru']['working_hours'] = 'с 10:00 до 20:00';
	$regions['oryel.dpo-anta.ru']['ya_coord'] = '55.826511, 49.148924';

	$regions['orenburg.dpo-anta.ru']['title']=' в Оренбурге';
	$regions['orenburg.dpo-anta.ru']['modal']=' Оренбург';
	$regions['orenburg.dpo-anta.ru']['address'] = '<span itemprop="addressLocality">г. Оренбург</span>, <span itemprop="streetAddress">пр. Ямашева, д. 93, ТЦ «Савиново», 5 эт., м. Козья слобода</span>';
	$regions['orenburg.dpo-anta.ru']['phone'] = '+7 (987) 291-86-08';
	$regions['orenburg.dpo-anta.ru']['working_hours'] = 'с 10:00 до 20:00';
	$regions['orenburg.dpo-anta.ru']['ya_coord'] = '55.826511, 49.148924';

	$regions['orekhovo-zuevo.dpo-anta.ru']['title']=' в Орехово-Зуеве';
	$regions['orekhovo-zuevo.dpo-anta.ru']['modal']=' Орехово-Зуево';
	$regions['orekhovo-zuevo.dpo-anta.ru']['address'] = '<span itemprop="addressLocality">г. Орехово-Зуево</span>, <span itemprop="streetAddress">пр. Ямашева, д. 93, ТЦ «Савиново», 5 эт., м. Козья слобода</span>';
	$regions['orekhovo-zuevo.dpo-anta.ru']['phone'] = '+7 (987) 291-86-08';
	$regions['orekhovo-zuevo.dpo-anta.ru']['working_hours'] = 'с 10:00 до 20:00';
	$regions['orekhovo-zuevo.dpo-anta.ru']['ya_coord'] = '55.826511, 49.148924';

	$regions['orsk.dpo-anta.ru']['title']=' в Орске';
	$regions['orsk.dpo-anta.ru']['modal']=' Орск';
	$regions['orsk.dpo-anta.ru']['address'] = '<span itemprop="addressLocality">г. Орск</span>, <span itemprop="streetAddress">пр. Ямашева, д. 93, ТЦ «Савиново», 5 эт., м. Козья слобода</span>';
	$regions['orsk.dpo-anta.ru']['phone'] = '+7 (987) 291-86-08';
	$regions['orsk.dpo-anta.ru']['working_hours'] = 'с 10:00 до 20:00';
	$regions['orsk.dpo-anta.ru']['ya_coord'] = '55.826511, 49.148924';

	$regions['penza.dpo-anta.ru']['title']=' в Пензе';
	$regions['penza.dpo-anta.ru']['modal']=' Пенза';
	$regions['penza.dpo-anta.ru']['address'] = '<span itemprop="addressLocality">г. Пенза</span>, <span itemprop="streetAddress">пр. Ямашева, д. 93, ТЦ «Савиново», 5 эт., м. Козья слобода</span>';
	$regions['penza.dpo-anta.ru']['phone'] = '+7 (987) 291-86-08';
	$regions['penza.dpo-anta.ru']['working_hours'] = 'с 10:00 до 20:00';
	$regions['penza.dpo-anta.ru']['ya_coord'] = '55.826511, 49.148924';

	$regions['pervouralsk.dpo-anta.ru']['title']=' в Первоуральске';
	$regions['pervouralsk.dpo-anta.ru']['modal']=' Первоуральск';
	$regions['pervouralsk.dpo-anta.ru']['address'] = '<span itemprop="addressLocality">г. Первоуральск</span>, <span itemprop="streetAddress">пр. Ямашева, д. 93, ТЦ «Савиново», 5 эт., м. Козья слобода</span>';
	$regions['pervouralsk.dpo-anta.ru']['phone'] = '+7 (987) 291-86-08';
	$regions['pervouralsk.dpo-anta.ru']['working_hours'] = 'с 10:00 до 20:00';
	$regions['pervouralsk.dpo-anta.ru']['ya_coord'] = '55.826511, 49.148924';

	$regions['perm.dpo-anta.ru']['title']=' в Перми';
	$regions['perm.dpo-anta.ru']['modal']=' Пермь';
	$regions['perm.dpo-anta.ru']['address'] = '<span itemprop="addressLocality">г. Пермь</span>, <span itemprop="streetAddress">пр. Ямашева, д. 93, ТЦ «Савиново», 5 эт., м. Козья слобода</span>';
	$regions['perm.dpo-anta.ru']['phone'] = '+7 (987) 291-86-08';
	$regions['perm.dpo-anta.ru']['working_hours'] = 'с 10:00 до 20:00';
	$regions['perm.dpo-anta.ru']['ya_coord'] = '55.826511, 49.148924';

	$regions['petrozavodsk.dpo-anta.ru']['title']=' в Петрозаводске';
	$regions['petrozavodsk.dpo-anta.ru']['modal']=' Петрозаводск';
	$regions['petrozavodsk.dpo-anta.ru']['address'] = '<span itemprop="addressLocality">г. Петрозаводск</span>, <span itemprop="streetAddress">пр. Ямашева, д. 93, ТЦ «Савиново», 5 эт., м. Козья слобода</span>';
	$regions['petrozavodsk.dpo-anta.ru']['phone'] = '+7 (987) 291-86-08';
	$regions['petrozavodsk.dpo-anta.ru']['working_hours'] = 'с 10:00 до 20:00';
	$regions['petrozavodsk.dpo-anta.ru']['ya_coord'] = '55.826511, 49.148924';

	$regions['petropavlovsk-kamchatskiy.dpo-anta.ru']['title']=' в Петропавловске-Камчатском';
	$regions['petropavlovsk-kamchatskiy.dpo-anta.ru']['modal']=' Петропавловск-Камчатский';
	$regions['petropavlovsk-kamchatskiy.dpo-anta.ru']['address'] = '<span itemprop="addressLocality">г. Петропавловск-Камчатский</span>, <span itemprop="streetAddress">пр. Ямашева, д. 93, ТЦ «Савиново», 5 эт., м. Козья слобода</span>';
	$regions['petropavlovsk-kamchatskiy.dpo-anta.ru']['phone'] = '+7 (987) 291-86-08';
	$regions['petropavlovsk-kamchatskiy.dpo-anta.ru']['working_hours'] = 'с 10:00 до 20:00';
	$regions['petropavlovsk-kamchatskiy.dpo-anta.ru']['ya_coord'] = '55.826511, 49.148924';

	$regions['podolsk.dpo-anta.ru']['title']=' в Подольске';
	$regions['podolsk.dpo-anta.ru']['modal']=' Подольск';
	$regions['podolsk.dpo-anta.ru']['address'] = '<span itemprop="addressLocality">г. Подольск</span>, <span itemprop="streetAddress">пр. Ямашева, д. 93, ТЦ «Савиново», 5 эт., м. Козья слобода</span>';
	$regions['podolsk.dpo-anta.ru']['phone'] = '+7 (987) 291-86-08';
	$regions['podolsk.dpo-anta.ru']['working_hours'] = 'с 10:00 до 20:00';
	$regions['podolsk.dpo-anta.ru']['ya_coord'] = '55.826511, 49.148924';

	$regions['prokopevsk.dpo-anta.ru']['title']=' в Прокопьевске';
	$regions['prokopevsk.dpo-anta.ru']['modal']=' Прокопьевск';
	$regions['prokopevsk.dpo-anta.ru']['address'] = '<span itemprop="addressLocality">г. Прокопьевск</span>, <span itemprop="streetAddress">пр. Ямашева, д. 93, ТЦ «Савиново», 5 эт., м. Козья слобода</span>';
	$regions['prokopevsk.dpo-anta.ru']['phone'] = '+7 (987) 291-86-08';
	$regions['prokopevsk.dpo-anta.ru']['working_hours'] = 'с 10:00 до 20:00';
	$regions['prokopevsk.dpo-anta.ru']['ya_coord'] = '55.826511, 49.148924';

	$regions['pskov.dpo-anta.ru']['title']=' в Пскове';
	$regions['pskov.dpo-anta.ru']['modal']=' Псков';
	$regions['pskov.dpo-anta.ru']['address'] = '<span itemprop="addressLocality">г. Псков</span>, <span itemprop="streetAddress">пр. Ямашева, д. 93, ТЦ «Савиново», 5 эт., м. Козья слобода</span>';
	$regions['pskov.dpo-anta.ru']['phone'] = '+7 (987) 291-86-08';
	$regions['pskov.dpo-anta.ru']['working_hours'] = 'с 10:00 до 20:00';
	$regions['pskov.dpo-anta.ru']['ya_coord'] = '55.826511, 49.148924';

	$regions['pushkino.dpo-anta.ru']['title']=' в Пушкине';
	$regions['pushkino.dpo-anta.ru']['modal']=' Пушкино';
	$regions['pushkino.dpo-anta.ru']['address'] = '<span itemprop="addressLocality">г. Пушкино</span>, <span itemprop="streetAddress">пр. Ямашева, д. 93, ТЦ «Савиново», 5 эт., м. Козья слобода</span>';
	$regions['pushkino.dpo-anta.ru']['phone'] = '+7 (987) 291-86-08';
	$regions['pushkino.dpo-anta.ru']['working_hours'] = 'с 10:00 до 20:00';
	$regions['pushkino.dpo-anta.ru']['ya_coord'] = '55.826511, 49.148924';

	$regions['pyatigorsk.dpo-anta.ru']['title']=' в Пятигорске';
	$regions['pyatigorsk.dpo-anta.ru']['modal']=' Пятигорск';
	$regions['pyatigorsk.dpo-anta.ru']['address'] = '<span itemprop="addressLocality">г. Пятигорск</span>, <span itemprop="streetAddress">пр. Ямашева, д. 93, ТЦ «Савиново», 5 эт., м. Козья слобода</span>';
	$regions['pyatigorsk.dpo-anta.ru']['phone'] = '+7 (987) 291-86-08';
	$regions['pyatigorsk.dpo-anta.ru']['working_hours'] = 'с 10:00 до 20:00';
	$regions['pyatigorsk.dpo-anta.ru']['ya_coord'] = '55.826511, 49.148924';

	$regions['ramenskoe.dpo-anta.ru']['title']=' в Раменском';
	$regions['ramenskoe.dpo-anta.ru']['modal']=' Раменское';
	$regions['ramenskoe.dpo-anta.ru']['address'] = '<span itemprop="addressLocality">г. Раменское</span>, <span itemprop="streetAddress">пр. Ямашева, д. 93, ТЦ «Савиново», 5 эт., м. Козья слобода</span>';
	$regions['ramenskoe.dpo-anta.ru']['phone'] = '+7 (987) 291-86-08';
	$regions['ramenskoe.dpo-anta.ru']['working_hours'] = 'с 10:00 до 20:00';
	$regions['ramenskoe.dpo-anta.ru']['ya_coord'] = '55.826511, 49.148924';

	$regions['reutov.dpo-anta.ru']['title']=' в Реутове';
	$regions['reutov.dpo-anta.ru']['modal']=' Реутов';
	$regions['reutov.dpo-anta.ru']['address'] = '<span itemprop="addressLocality">г. Реутов</span>, <span itemprop="streetAddress">пр. Ямашева, д. 93, ТЦ «Савиново», 5 эт., м. Козья слобода</span>';
	$regions['reutov.dpo-anta.ru']['phone'] = '+7 (987) 291-86-08';
	$regions['reutov.dpo-anta.ru']['working_hours'] = 'с 10:00 до 20:00';
	$regions['reutov.dpo-anta.ru']['ya_coord'] = '55.826511, 49.148924';

	$regions['rostov-na-donu.dpo-anta.ru']['title']=' в Ростове-на-Дону';
	$regions['rostov-na-donu.dpo-anta.ru']['modal']=' Ростов-на-Дону';
	$regions['rostov-na-donu.dpo-anta.ru']['address'] = '<span itemprop="addressLocality">г. Ростов-на-Дону</span>, <span itemprop="streetAddress">пр. Ямашева, д. 93, ТЦ «Савиново», 5 эт., м. Козья слобода</span>';
	$regions['rostov-na-donu.dpo-anta.ru']['phone'] = '+7 (987) 291-86-08';
	$regions['rostov-na-donu.dpo-anta.ru']['working_hours'] = 'с 10:00 до 20:00';
	$regions['rostov-na-donu.dpo-anta.ru']['ya_coord'] = '55.826511, 49.148924';

	$regions['rubtsovsk.dpo-anta.ru']['title']=' в Рубцовске';
	$regions['rubtsovsk.dpo-anta.ru']['modal']=' Рубцовск';
	$regions['rubtsovsk.dpo-anta.ru']['address'] = '<span itemprop="addressLocality">г. Рубцовск</span>, <span itemprop="streetAddress">пр. Ямашева, д. 93, ТЦ «Савиново», 5 эт., м. Козья слобода</span>';
	$regions['rubtsovsk.dpo-anta.ru']['phone'] = '+7 (987) 291-86-08';
	$regions['rubtsovsk.dpo-anta.ru']['working_hours'] = 'с 10:00 до 20:00';
	$regions['rubtsovsk.dpo-anta.ru']['ya_coord'] = '55.826511, 49.148924';

	$regions['rybinsk.dpo-anta.ru']['title']=' в Рыбинске';
	$regions['rybinsk.dpo-anta.ru']['modal']=' Рыбинск';
	$regions['rybinsk.dpo-anta.ru']['address'] = '<span itemprop="addressLocality">г. Рыбинск</span>, <span itemprop="streetAddress">пр. Ямашева, д. 93, ТЦ «Савиново», 5 эт., м. Козья слобода</span>';
	$regions['rybinsk.dpo-anta.ru']['phone'] = '+7 (987) 291-86-08';
	$regions['rybinsk.dpo-anta.ru']['working_hours'] = 'с 10:00 до 20:00';
	$regions['rybinsk.dpo-anta.ru']['ya_coord'] = '55.826511, 49.148924';

	$regions['ryazan.dpo-anta.ru']['title']=' в Рязани';
	$regions['ryazan.dpo-anta.ru']['modal']=' Рязань';
	$regions['ryazan.dpo-anta.ru']['address'] = '<span itemprop="addressLocality">г. Рязань</span>, <span itemprop="streetAddress">пр. Ямашева, д. 93, ТЦ «Савиново», 5 эт., м. Козья слобода</span>';
	$regions['ryazan.dpo-anta.ru']['phone'] = '+7 (987) 291-86-08';
	$regions['ryazan.dpo-anta.ru']['working_hours'] = 'с 10:00 до 20:00';
	$regions['ryazan.dpo-anta.ru']['ya_coord'] = '55.826511, 49.148924';

	$regions['salavat.dpo-anta.ru']['title']=' в Салавате';
	$regions['salavat.dpo-anta.ru']['modal']=' Салават';
	$regions['salavat.dpo-anta.ru']['address'] = '<span itemprop="addressLocality">г. Салават</span>, <span itemprop="streetAddress">пр. Ямашева, д. 93, ТЦ «Савиново», 5 эт., м. Козья слобода</span>';
	$regions['salavat.dpo-anta.ru']['phone'] = '+7 (987) 291-86-08';
	$regions['salavat.dpo-anta.ru']['working_hours'] = 'с 10:00 до 20:00';
	$regions['salavat.dpo-anta.ru']['ya_coord'] = '55.826511, 49.148924';

	$regions['samara.dpo-anta.ru']['title']=' в Самаре';
	$regions['samara.dpo-anta.ru']['modal']=' Самара';
	$regions['samara.dpo-anta.ru']['address'] = '<span itemprop="addressLocality">г. Самара</span>, <span itemprop="streetAddress">пр. Ямашева, д. 93, ТЦ «Савиново», 5 эт., м. Козья слобода</span>';
	$regions['samara.dpo-anta.ru']['phone'] = '+7 (987) 291-86-08';
	$regions['samara.dpo-anta.ru']['working_hours'] = 'с 10:00 до 20:00';
	$regions['samara.dpo-anta.ru']['ya_coord'] = '55.826511, 49.148924';

	$regions['spb.dpo-anta.ru']['title']=' в Санкт-Петербурге';
	$regions['spb.dpo-anta.ru']['modal']=' Санкт-Петербург';
	$regions['spb.dpo-anta.ru']['address'] = '<span itemprop="addressLocality">г. Санкт-Петербург</span>, <span itemprop="streetAddress">пр. Ямашева, д. 93, ТЦ «Савиново», 5 эт., м. Козья слобода</span>';
	$regions['spb.dpo-anta.ru']['phone'] = '+7 (987) 291-86-08';
	$regions['spb.dpo-anta.ru']['working_hours'] = 'с 10:00 до 20:00';
	$regions['spb.dpo-anta.ru']['ya_coord'] = '55.826511, 49.148924';

	$regions['saransk.dpo-anta.ru']['title']=' в Саранске';
	$regions['saransk.dpo-anta.ru']['modal']=' Саранск';
	$regions['saransk.dpo-anta.ru']['address'] = '<span itemprop="addressLocality">г. Саранск</span>, <span itemprop="streetAddress">пр. Ямашева, д. 93, ТЦ «Савиново», 5 эт., м. Козья слобода</span>';
	$regions['saransk.dpo-anta.ru']['phone'] = '+7 (987) 291-86-08';
	$regions['saransk.dpo-anta.ru']['working_hours'] = 'с 10:00 до 20:00';
	$regions['saransk.dpo-anta.ru']['ya_coord'] = '55.826511, 49.148924';

	$regions['saratov.dpo-anta.ru']['title']=' в Саратове';
	$regions['saratov.dpo-anta.ru']['modal']=' Саратов';
	$regions['saratov.dpo-anta.ru']['address'] = '<span itemprop="addressLocality">г. Саратов</span>, <span itemprop="streetAddress">пр. Ямашева, д. 93, ТЦ «Савиново», 5 эт., м. Козья слобода</span>';
	$regions['saratov.dpo-anta.ru']['phone'] = '+7 (987) 291-86-08';
	$regions['saratov.dpo-anta.ru']['working_hours'] = 'с 10:00 до 20:00';
	$regions['saratov.dpo-anta.ru']['ya_coord'] = '55.826511, 49.148924';

	$regions['sevastopol.dpo-anta.ru']['title']=' в Севастополе';
	$regions['sevastopol.dpo-anta.ru']['modal']=' Севастополь';
	$regions['sevastopol.dpo-anta.ru']['address'] = '<span itemprop="addressLocality">г. Севастополь</span>, <span itemprop="streetAddress">пр. Ямашева, д. 93, ТЦ «Савиново», 5 эт., м. Козья слобода</span>';
	$regions['sevastopol.dpo-anta.ru']['phone'] = '+7 (987) 291-86-08';
	$regions['sevastopol.dpo-anta.ru']['working_hours'] = 'с 10:00 до 20:00';
	$regions['sevastopol.dpo-anta.ru']['ya_coord'] = '55.826511, 49.148924';

	$regions['severodvinsk.dpo-anta.ru']['title']=' в Северодвинске';
	$regions['severodvinsk.dpo-anta.ru']['modal']=' Северодвинск';
	$regions['severodvinsk.dpo-anta.ru']['address'] = '<span itemprop="addressLocality">г. Северодвинск</span>, <span itemprop="streetAddress">пр. Ямашева, д. 93, ТЦ «Савиново», 5 эт., м. Козья слобода</span>';
	$regions['severodvinsk.dpo-anta.ru']['phone'] = '+7 (987) 291-86-08';
	$regions['severodvinsk.dpo-anta.ru']['working_hours'] = 'с 10:00 до 20:00';
	$regions['severodvinsk.dpo-anta.ru']['ya_coord'] = '55.826511, 49.148924';

	$regions['seversk.dpo-anta.ru']['title']=' в Северске';
	$regions['seversk.dpo-anta.ru']['modal']=' Северск';
	$regions['seversk.dpo-anta.ru']['address'] = '<span itemprop="addressLocality">г. Северск</span>, <span itemprop="streetAddress">пр. Ямашева, д. 93, ТЦ «Савиново», 5 эт., м. Козья слобода</span>';
	$regions['seversk.dpo-anta.ru']['phone'] = '+7 (987) 291-86-08';
	$regions['seversk.dpo-anta.ru']['working_hours'] = 'с 10:00 до 20:00';
	$regions['seversk.dpo-anta.ru']['ya_coord'] = '55.826511, 49.148924';

	$regions['sergiev-posad.dpo-anta.ru']['title']=' в в Сергиевом Посаде';
	$regions['sergiev-posad.dpo-anta.ru']['modal']=' Сергиев Посад';
	$regions['sergiev-posad.dpo-anta.ru']['address'] = '<span itemprop="addressLocality">г. Сергиев Посад</span>, <span itemprop="streetAddress">пр. Ямашева, д. 93, ТЦ «Савиново», 5 эт., м. Козья слобода</span>';
	$regions['sergiev-posad.dpo-anta.ru']['phone'] = '+7 (987) 291-86-08';
	$regions['sergiev-posad.dpo-anta.ru']['working_hours'] = 'с 10:00 до 20:00';
	$regions['sergiev-posad.dpo-anta.ru']['ya_coord'] = '55.826511, 49.148924';

	$regions['serpukhov.dpo-anta.ru']['title']=' в Серпухове';
	$regions['serpukhov.dpo-anta.ru']['modal']=' Серпухов';
	$regions['serpukhov.dpo-anta.ru']['address'] = '<span itemprop="addressLocality">г. Серпухов</span>, <span itemprop="streetAddress">пр. Ямашева, д. 93, ТЦ «Савиново», 5 эт., м. Козья слобода</span>';
	$regions['serpukhov.dpo-anta.ru']['phone'] = '+7 (987) 291-86-08';
	$regions['serpukhov.dpo-anta.ru']['working_hours'] = 'с 10:00 до 20:00';
	$regions['serpukhov.dpo-anta.ru']['ya_coord'] = '55.826511, 49.148924';

	$regions['simferopol.dpo-anta.ru']['title']=' в Симферополе';
	$regions['simferopol.dpo-anta.ru']['modal']=' Симферополь';
	$regions['simferopol.dpo-anta.ru']['address'] = '<span itemprop="addressLocality">г. Симферополь</span>, <span itemprop="streetAddress">пр. Ямашева, д. 93, ТЦ «Савиново», 5 эт., м. Козья слобода</span>';
	$regions['simferopol.dpo-anta.ru']['phone'] = '+7 (987) 291-86-08';
	$regions['simferopol.dpo-anta.ru']['working_hours'] = 'с 10:00 до 20:00';
	$regions['simferopol.dpo-anta.ru']['ya_coord'] = '55.826511, 49.148924';

	$regions['smolensk.dpo-anta.ru']['title']=' в Смоленске';
	$regions['smolensk.dpo-anta.ru']['modal']=' Смоленск';
	$regions['smolensk.dpo-anta.ru']['address'] = '<span itemprop="addressLocality">г. Смоленск</span>, <span itemprop="streetAddress">пр. Ямашева, д. 93, ТЦ «Савиново», 5 эт., м. Козья слобода</span>';
	$regions['smolensk.dpo-anta.ru']['phone'] = '+7 (987) 291-86-08';
	$regions['smolensk.dpo-anta.ru']['working_hours'] = 'с 10:00 до 20:00';
	$regions['smolensk.dpo-anta.ru']['ya_coord'] = '55.826511, 49.148924';

	$regions['sochi.dpo-anta.ru']['title']=' в Сочи';
	$regions['sochi.dpo-anta.ru']['modal']=' Сочи';
	$regions['sochi.dpo-anta.ru']['address'] = '<span itemprop="addressLocality">г. Сочи</span>, <span itemprop="streetAddress">пр. Ямашева, д. 93, ТЦ «Савиново», 5 эт., м. Козья слобода</span>';
	$regions['sochi.dpo-anta.ru']['phone'] = '+7 (987) 291-86-08';
	$regions['sochi.dpo-anta.ru']['working_hours'] = 'с 10:00 до 20:00';
	$regions['sochi.dpo-anta.ru']['ya_coord'] = '55.826511, 49.148924';

	$regions['stavropol.dpo-anta.ru']['title']=' в Ставраполе';
	$regions['stavropol.dpo-anta.ru']['modal']=' Ставраполь';
	$regions['stavropol.dpo-anta.ru']['address'] = '<span itemprop="addressLocality">г. Ставраполь</span>, <span itemprop="streetAddress">пр. Ямашева, д. 93, ТЦ «Савиново», 5 эт., м. Козья слобода</span>';
	$regions['stavropol.dpo-anta.ru']['phone'] = '+7 (987) 291-86-08';
	$regions['stavropol.dpo-anta.ru']['working_hours'] = 'с 10:00 до 20:00';
	$regions['stavropol.dpo-anta.ru']['ya_coord'] = '55.826511, 49.148924';

	$regions['staryy-oskol.dpo-anta.ru']['title']=' в Старом Осколе';
	$regions['staryy-oskol.dpo-anta.ru']['modal']=' Старый Оскол';
	$regions['staryy-oskol.dpo-anta.ru']['address'] = '<span itemprop="addressLocality">г. Старый Оскол</span>, <span itemprop="streetAddress">пр. Ямашева, д. 93, ТЦ «Савиново», 5 эт., м. Козья слобода</span>';
	$regions['staryy-oskol.dpo-anta.ru']['phone'] = '+7 (987) 291-86-08';
	$regions['staryy-oskol.dpo-anta.ru']['working_hours'] = 'с 10:00 до 20:00';
	$regions['staryy-oskol.dpo-anta.ru']['ya_coord'] = '55.826511, 49.148924';

	$regions['sterlitamak.dpo-anta.ru']['title']=' в Стерлитамаке';
	$regions['sterlitamak.dpo-anta.ru']['modal']=' Стерлитамак';
	$regions['sterlitamak.dpo-anta.ru']['address'] = '<span itemprop="addressLocality">г. Стерлитамак</span>, <span itemprop="streetAddress">пр. Ямашева, д. 93, ТЦ «Савиново», 5 эт., м. Козья слобода</span>';
	$regions['sterlitamak.dpo-anta.ru']['phone'] = '+7 (987) 291-86-08';
	$regions['sterlitamak.dpo-anta.ru']['working_hours'] = 'с 10:00 до 20:00';
	$regions['sterlitamak.dpo-anta.ru']['ya_coord'] = '55.826511, 49.148924';

	$regions['surgut.dpo-anta.ru']['title']=' в Сургуте';
	$regions['surgut.dpo-anta.ru']['modal']=' Сургут';
	$regions['surgut.dpo-anta.ru']['address'] = '<span itemprop="addressLocality">г. Сургут</span>, <span itemprop="streetAddress">пр. Ямашева, д. 93, ТЦ «Савиново», 5 эт., м. Козья слобода</span>';
	$regions['surgut.dpo-anta.ru']['phone'] = '+7 (987) 291-86-08';
	$regions['surgut.dpo-anta.ru']['working_hours'] = 'с 10:00 до 20:00';
	$regions['surgut.dpo-anta.ru']['ya_coord'] = '55.826511, 49.148924';

	$regions['syzran.dpo-anta.ru']['title']=' в Сызрани';
	$regions['syzran.dpo-anta.ru']['modal']=' Сызрань';
	$regions['syzran.dpo-anta.ru']['address'] = '<span itemprop="addressLocality">г. Сызрань</span>, <span itemprop="streetAddress">пр. Ямашева, д. 93, ТЦ «Савиново», 5 эт., м. Козья слобода</span>';
	$regions['syzran.dpo-anta.ru']['phone'] = '+7 (987) 291-86-08';
	$regions['syzran.dpo-anta.ru']['working_hours'] = 'с 10:00 до 20:00';
	$regions['syzran.dpo-anta.ru']['ya_coord'] = '55.826511, 49.148924';

	$regions['syktyvkar.dpo-anta.ru']['title']=' в Сыктывкаре';
	$regions['syktyvkar.dpo-anta.ru']['modal']=' Сыктывкар';
	$regions['syktyvkar.dpo-anta.ru']['address'] = '<span itemprop="addressLocality">г. Сыктывкар</span>, <span itemprop="streetAddress">пр. Ямашева, д. 93, ТЦ «Савиново», 5 эт., м. Козья слобода</span>';
	$regions['syktyvkar.dpo-anta.ru']['phone'] = '+7 (987) 291-86-08';
	$regions['syktyvkar.dpo-anta.ru']['working_hours'] = 'с 10:00 до 20:00';
	$regions['syktyvkar.dpo-anta.ru']['ya_coord'] = '55.826511, 49.148924';

	$regions['taganrog.dpo-anta.ru']['title']=' в Таганроге';
	$regions['taganrog.dpo-anta.ru']['modal']=' Таганрог';
	$regions['taganrog.dpo-anta.ru']['address'] = '<span itemprop="addressLocality">г. Таганрог</span>, <span itemprop="streetAddress">пр. Ямашева, д. 93, ТЦ «Савиново», 5 эт., м. Козья слобода</span>';
	$regions['taganrog.dpo-anta.ru']['phone'] = '+7 (987) 291-86-08';
	$regions['taganrog.dpo-anta.ru']['working_hours'] = 'с 10:00 до 20:00';
	$regions['taganrog.dpo-anta.ru']['ya_coord'] = '55.826511, 49.148924';

	$regions['tambov.dpo-anta.ru']['title']=' в Тамбове';
	$regions['tambov.dpo-anta.ru']['modal']=' Тамбов';
	$regions['tambov.dpo-anta.ru']['address'] = '<span itemprop="addressLocality">г. Тамбов</span>, <span itemprop="streetAddress">пр. Ямашева, д. 93, ТЦ «Савиново», 5 эт., м. Козья слобода</span>';
	$regions['tambov.dpo-anta.ru']['phone'] = '+7 (987) 291-86-08';
	$regions['tambov.dpo-anta.ru']['working_hours'] = 'с 10:00 до 20:00';
	$regions['tambov.dpo-anta.ru']['ya_coord'] = '55.826511, 49.148924';

	$regions['tver.dpo-anta.ru']['title']=' в Твери';
	$regions['tver.dpo-anta.ru']['modal']=' Тверь';
	$regions['tver.dpo-anta.ru']['address'] = '<span itemprop="addressLocality">г. Тверь</span>, <span itemprop="streetAddress">пр. Ямашева, д. 93, ТЦ «Савиново», 5 эт., м. Козья слобода</span>';
	$regions['tver.dpo-anta.ru']['phone'] = '+7 (987) 291-86-08';
	$regions['tver.dpo-anta.ru']['working_hours'] = 'с 10:00 до 20:00';
	$regions['tver.dpo-anta.ru']['ya_coord'] = '55.826511, 49.148924';

	$regions['tolyatti.dpo-anta.ru']['title']=' в Тольятти';
	$regions['tolyatti.dpo-anta.ru']['modal']=' Тольятти';
	$regions['tolyatti.dpo-anta.ru']['address'] = '<span itemprop="addressLocality">г. Тольятти</span>, <span itemprop="streetAddress">пр. Ямашева, д. 93, ТЦ «Савиново», 5 эт., м. Козья слобода</span>';
	$regions['tolyatti.dpo-anta.ru']['phone'] = '+7 (987) 291-86-08';
	$regions['tolyatti.dpo-anta.ru']['working_hours'] = 'с 10:00 до 20:00';
	$regions['tolyatti.dpo-anta.ru']['ya_coord'] = '55.826511, 49.148924';

	$regions['tomsk.dpo-anta.ru']['title']=' в Томске';
	$regions['tomsk.dpo-anta.ru']['modal']=' Томск';
	$regions['tomsk.dpo-anta.ru']['address'] = '<span itemprop="addressLocality">г. Томск</span>, <span itemprop="streetAddress">пр. Ямашева, д. 93, ТЦ «Савиново», 5 эт., м. Козья слобода</span>';
	$regions['tomsk.dpo-anta.ru']['phone'] = '+7 (987) 291-86-08';
	$regions['tomsk.dpo-anta.ru']['working_hours'] = 'с 10:00 до 20:00';
	$regions['tomsk.dpo-anta.ru']['ya_coord'] = '55.826511, 49.148924';

	$regions['tula.dpo-anta.ru']['title']=' в Туле';
	$regions['tula.dpo-anta.ru']['modal']=' Тула';
	$regions['tula.dpo-anta.ru']['address'] = '<span itemprop="addressLocality">г. Тула</span>, <span itemprop="streetAddress">пр. Ямашева, д. 93, ТЦ «Савиново», 5 эт., м. Козья слобода</span>';
	$regions['tula.dpo-anta.ru']['phone'] = '+7 (987) 291-86-08';
	$regions['tula.dpo-anta.ru']['working_hours'] = 'с 10:00 до 20:00';
	$regions['tula.dpo-anta.ru']['ya_coord'] = '55.826511, 49.148924';

	$regions['tyumen.dpo-anta.ru']['title']=' в Тюмени';
	$regions['tyumen.dpo-anta.ru']['modal']=' Тюмень';
	$regions['tyumen.dpo-anta.ru']['address'] = '<span itemprop="addressLocality">г. Тюмень</span>, <span itemprop="streetAddress">пр. Ямашева, д. 93, ТЦ «Савиново», 5 эт., м. Козья слобода</span>';
	$regions['tyumen.dpo-anta.ru']['phone'] = '+7 (987) 291-86-08';
	$regions['tyumen.dpo-anta.ru']['working_hours'] = 'с 10:00 до 20:00';
	$regions['tyumen.dpo-anta.ru']['ya_coord'] = '55.826511, 49.148924';

	$regions['ulan-ude.dpo-anta.ru']['title']=' в Улан-Удэ';
	$regions['ulan-ude.dpo-anta.ru']['modal']=' Улан-Удэ';
	$regions['ulan-ude.dpo-anta.ru']['address'] = '<span itemprop="addressLocality">г. Улан-Удэ</span>, <span itemprop="streetAddress">пр. Ямашева, д. 93, ТЦ «Савиново», 5 эт., м. Козья слобода</span>';
	$regions['ulan-ude.dpo-anta.ru']['phone'] = '+7 (987) 291-86-08';
	$regions['ulan-ude.dpo-anta.ru']['working_hours'] = 'с 10:00 до 20:00';
	$regions['ulan-ude.dpo-anta.ru']['ya_coord'] = '55.826511, 49.148924';

	$regions['ulyanovsk.dpo-anta.ru']['title']=' в Ульяновске';
	$regions['ulyanovsk.dpo-anta.ru']['modal']=' Ульяновск';
	$regions['ulyanovsk.dpo-anta.ru']['address'] = '<span itemprop="addressLocality">г. Ульяновск</span>, <span itemprop="streetAddress">пр. Ямашева, д. 93, ТЦ «Савиново», 5 эт., м. Козья слобода</span>';
	$regions['ulyanovsk.dpo-anta.ru']['phone'] = '+7 (987) 291-86-08';
	$regions['ulyanovsk.dpo-anta.ru']['working_hours'] = 'с 10:00 до 20:00';
	$regions['ulyanovsk.dpo-anta.ru']['ya_coord'] = '55.826511, 49.148924';

	$regions['ussuriysk.dpo-anta.ru']['title']=' в Уссурийске';
	$regions['ussuriysk.dpo-anta.ru']['modal']=' Уссурийск';
	$regions['ussuriysk.dpo-anta.ru']['address'] = '<span itemprop="addressLocality">г. Уссурийск</span>, <span itemprop="streetAddress">пр. Ямашева, д. 93, ТЦ «Савиново», 5 эт., м. Козья слобода</span>';
	$regions['ussuriysk.dpo-anta.ru']['phone'] = '+7 (987) 291-86-08';
	$regions['ussuriysk.dpo-anta.ru']['working_hours'] = 'с 10:00 до 20:00';
	$regions['ussuriysk.dpo-anta.ru']['ya_coord'] = '55.826511, 49.148924';

	$regions['ufa.dpo-anta.ru']['title']=' в Уфе';
	$regions['ufa.dpo-anta.ru']['modal']=' Уфа';
	$regions['ufa.dpo-anta.ru']['address'] = '<span itemprop="addressLocality">г. Уфа</span>, <span itemprop="streetAddress">пр. Ямашева, д. 93, ТЦ «Савиново», 5 эт., м. Козья слобода</span>';
	$regions['ufa.dpo-anta.ru']['phone'] = '+7 (987) 291-86-08';
	$regions['ufa.dpo-anta.ru']['working_hours'] = 'с 10:00 до 20:00';
	$regions['ufa.dpo-anta.ru']['ya_coord'] = '55.826511, 49.148924';

	$regions['khabarovsk.dpo-anta.ru']['title']=' в Хабаровске';
	$regions['khabarovsk.dpo-anta.ru']['modal']=' Хабаровск';
	$regions['khabarovsk.dpo-anta.ru']['address'] = '<span itemprop="addressLocality">г. Хабаровск</span>, <span itemprop="streetAddress">пр. Ямашева, д. 93, ТЦ «Савиново», 5 эт., м. Козья слобода</span>';
	$regions['khabarovsk.dpo-anta.ru']['phone'] = '+7 (987) 291-86-08';
	$regions['khabarovsk.dpo-anta.ru']['working_hours'] = 'с 10:00 до 20:00';
	$regions['khabarovsk.dpo-anta.ru']['ya_coord'] = '55.826511, 49.148924';

	$regions['khasavyurt.dpo-anta.ru']['title']=' в Хасавюрте';
	$regions['khasavyurt.dpo-anta.ru']['modal']=' Хасавюрт';
	$regions['khasavyurt.dpo-anta.ru']['address'] = '<span itemprop="addressLocality">г. Хасавюрт</span>, <span itemprop="streetAddress">пр. Ямашева, д. 93, ТЦ «Савиново», 5 эт., м. Козья слобода</span>';
	$regions['khasavyurt.dpo-anta.ru']['phone'] = '+7 (987) 291-86-08';
	$regions['khasavyurt.dpo-anta.ru']['working_hours'] = 'с 10:00 до 20:00';
	$regions['khasavyurt.dpo-anta.ru']['ya_coord'] = '55.826511, 49.148924';

	$regions['khimki.dpo-anta.ru']['title']=' в Химках';
	$regions['khimki.dpo-anta.ru']['modal']=' Химки';
	$regions['khimki.dpo-anta.ru']['address'] = '<span itemprop="addressLocality">г. Химки</span>, <span itemprop="streetAddress">пр. Ямашева, д. 93, ТЦ «Савиново», 5 эт., м. Козья слобода</span>';
	$regions['khimki.dpo-anta.ru']['phone'] = '+7 (987) 291-86-08';
	$regions['khimki.dpo-anta.ru']['working_hours'] = 'с 10:00 до 20:00';
	$regions['khimki.dpo-anta.ru']['ya_coord'] = '55.826511, 49.148924';

	$regions['cheboksary.dpo-anta.ru']['title']=' в Чебоксарах';
	$regions['cheboksary.dpo-anta.ru']['modal']=' Чебоксары';
	$regions['cheboksary.dpo-anta.ru']['address'] = '<span itemprop="addressLocality">г. Чебоксары</span>, <span itemprop="streetAddress">пр. Ямашева, д. 93, ТЦ «Савиново», 5 эт., м. Козья слобода</span>';
	$regions['cheboksary.dpo-anta.ru']['phone'] = '+7 (987) 291-86-08';
	$regions['cheboksary.dpo-anta.ru']['working_hours'] = 'с 10:00 до 20:00';
	$regions['cheboksary.dpo-anta.ru']['ya_coord'] = '55.826511, 49.148924';

	$regions['chelyabinsk.dpo-anta.ru']['title']=' в Челябинске';
	$regions['chelyabinsk.dpo-anta.ru']['modal']=' Челябинск';
	$regions['chelyabinsk.dpo-anta.ru']['address'] = '<span itemprop="addressLocality">г. Челябинск</span>, <span itemprop="streetAddress">пр. Ямашева, д. 93, ТЦ «Савиново», 5 эт., м. Козья слобода</span>';
	$regions['chelyabinsk.dpo-anta.ru']['phone'] = '+7 (987) 291-86-08';
	$regions['chelyabinsk.dpo-anta.ru']['working_hours'] = 'с 10:00 до 20:00';
	$regions['chelyabinsk.dpo-anta.ru']['ya_coord'] = '55.826511, 49.148924';

	$regions['cherepovets.dpo-anta.ru']['title']=' в Череповце';
	$regions['cherepovets.dpo-anta.ru']['modal']=' Череповец';
	$regions['cherepovets.dpo-anta.ru']['address'] = '<span itemprop="addressLocality">г. Череповец</span>, <span itemprop="streetAddress">пр. Ямашева, д. 93, ТЦ «Савиново», 5 эт., м. Козья слобода</span>';
	$regions['cherepovets.dpo-anta.ru']['phone'] = '+7 (987) 291-86-08';
	$regions['cherepovets.dpo-anta.ru']['working_hours'] = 'с 10:00 до 20:00';
	$regions['cherepovets.dpo-anta.ru']['ya_coord'] = '55.826511, 49.148924';

	$regions['cherkessk.dpo-anta.ru']['title']=' в Черкесске';
	$regions['cherkessk.dpo-anta.ru']['modal']=' Черкесск';
	$regions['cherkessk.dpo-anta.ru']['address'] = '<span itemprop="addressLocality">г. Черкесск</span>, <span itemprop="streetAddress">пр. Ямашева, д. 93, ТЦ «Савиново», 5 эт., м. Козья слобода</span>';
	$regions['cherkessk.dpo-anta.ru']['phone'] = '+7 (987) 291-86-08';
	$regions['cherkessk.dpo-anta.ru']['working_hours'] = 'с 10:00 до 20:00';
	$regions['cherkessk.dpo-anta.ru']['ya_coord'] = '55.826511, 49.148924';

	$regions['chita.dpo-anta.ru']['title']=' в Чите';
	$regions['chita.dpo-anta.ru']['modal']=' Чита';
	$regions['chita.dpo-anta.ru']['address'] = '<span itemprop="addressLocality">г. Чита</span>, <span itemprop="streetAddress">пр. Ямашева, д. 93, ТЦ «Савиново», 5 эт., м. Козья слобода</span>';
	$regions['chita.dpo-anta.ru']['phone'] = '+7 (987) 291-86-08';
	$regions['chita.dpo-anta.ru']['working_hours'] = 'с 10:00 до 20:00';
	$regions['chita.dpo-anta.ru']['ya_coord'] = '55.826511, 49.148924';

	$regions['shakhty.dpo-anta.ru']['title']=' в Шахтах';
	$regions['shakhty.dpo-anta.ru']['modal']=' Шахты';
	$regions['shakhty.dpo-anta.ru']['address'] = '<span itemprop="addressLocality">г. Шахты</span>, <span itemprop="streetAddress">пр. Ямашева, д. 93, ТЦ «Савиново», 5 эт., м. Козья слобода</span>';
	$regions['shakhty.dpo-anta.ru']['phone'] = '+7 (987) 291-86-08';
	$regions['shakhty.dpo-anta.ru']['working_hours'] = 'с 10:00 до 20:00';
	$regions['shakhty.dpo-anta.ru']['ya_coord'] = '55.826511, 49.148924';

	$regions['shchyelkovo.dpo-anta.ru']['title']=' в Щелкове';
	$regions['shchyelkovo.dpo-anta.ru']['modal']=' Щёлково';
	$regions['shchyelkovo.dpo-anta.ru']['address'] = '<span itemprop="addressLocality">г. Щёлково</span>, <span itemprop="streetAddress">пр. Ямашева, д. 93, ТЦ «Савиново», 5 эт., м. Козья слобода</span>';
	$regions['shchyelkovo.dpo-anta.ru']['phone'] = '+7 (987) 291-86-08';
	$regions['shchyelkovo.dpo-anta.ru']['working_hours'] = 'с 10:00 до 20:00';
	$regions['shchyelkovo.dpo-anta.ru']['ya_coord'] = '55.826511, 49.148924';

	$regions['elektrostal.dpo-anta.ru']['title']=' в Электростали';
	$regions['elektrostal.dpo-anta.ru']['modal']=' Электросталь';
	$regions['elektrostal.dpo-anta.ru']['address'] = '<span itemprop="addressLocality">г. Электросталь</span>, <span itemprop="streetAddress">пр. Ямашева, д. 93, ТЦ «Савиново», 5 эт., м. Козья слобода</span>';
	$regions['elektrostal.dpo-anta.ru']['phone'] = '+7 (987) 291-86-08';
	$regions['elektrostal.dpo-anta.ru']['working_hours'] = 'с 10:00 до 20:00';
	$regions['elektrostal.dpo-anta.ru']['ya_coord'] = '55.826511, 49.148924';

	$regions['elista.dpo-anta.ru']['title']=' в Элисте';
	$regions['elista.dpo-anta.ru']['modal']=' Элиста';
	$regions['elista.dpo-anta.ru']['address'] = '<span itemprop="addressLocality">г. Элиста</span>, <span itemprop="streetAddress">пр. Ямашева, д. 93, ТЦ «Савиново», 5 эт., м. Козья слобода</span>';
	$regions['elista.dpo-anta.ru']['phone'] = '+7 (987) 291-86-08';
	$regions['elista.dpo-anta.ru']['working_hours'] = 'с 10:00 до 20:00';
	$regions['elista.dpo-anta.ru']['ya_coord'] = '55.826511, 49.148924';

	$regions['engels.dpo-anta.ru']['title']=' в Энгельсе';
	$regions['engels.dpo-anta.ru']['modal']=' Энгельс';
	$regions['engels.dpo-anta.ru']['address'] = '<span itemprop="addressLocality">г. Энгельс</span>, <span itemprop="streetAddress">пр. Ямашева, д. 93, ТЦ «Савиново», 5 эт., м. Козья слобода</span>';
	$regions['engels.dpo-anta.ru']['phone'] = '+7 (987) 291-86-08';
	$regions['engels.dpo-anta.ru']['working_hours'] = 'с 10:00 до 20:00';
	$regions['engels.dpo-anta.ru']['ya_coord'] = '55.826511, 49.148924';

	$regions['yuzhno-sakhalinsk.dpo-anta.ru']['title']=' в Южно-Сахалинске';
	$regions['yuzhno-sakhalinsk.dpo-anta.ru']['modal']=' Южно-Сахалинск';
	$regions['yuzhno-sakhalinsk.dpo-anta.ru']['address'] = '<span itemprop="addressLocality">г. Южно-Сахалинск</span>, <span itemprop="streetAddress">пр. Ямашева, д. 93, ТЦ «Савиново», 5 эт., м. Козья слобода</span>';
	$regions['yuzhno-sakhalinsk.dpo-anta.ru']['phone'] = '+7 (987) 291-86-08';
	$regions['yuzhno-sakhalinsk.dpo-anta.ru']['working_hours'] = 'с 10:00 до 20:00';
	$regions['yuzhno-sakhalinsk.dpo-anta.ru']['ya_coord'] = '55.826511, 49.148924';

	$regions['yakutsk.dpo-anta.ru']['title']=' в Якутске';
	$regions['yakutsk.dpo-anta.ru']['modal']=' Якутск';
	$regions['yakutsk.dpo-anta.ru']['address'] = '<span itemprop="addressLocality">г. Якутск</span>, <span itemprop="streetAddress">пр. Ямашева, д. 93, ТЦ «Савиново», 5 эт., м. Козья слобода</span>';
	$regions['yakutsk.dpo-anta.ru']['phone'] = '+7 (987) 291-86-08';
	$regions['yakutsk.dpo-anta.ru']['working_hours'] = 'с 10:00 до 20:00';
	$regions['yakutsk.dpo-anta.ru']['ya_coord'] = '55.826511, 49.148924';

	$regions['yaroslavl.dpo-anta.ru']['title']=' в Ярославле';
	$regions['yaroslavl.dpo-anta.ru']['modal']=' Ярославль';
	$regions['yaroslavl.dpo-anta.ru']['address'] = '<span itemprop="addressLocality">г. Ярославль</span>, <span itemprop="streetAddress">пр. Ямашева, д. 93, ТЦ «Савиново», 5 эт., м. Козья слобода</span>';
	$regions['yaroslavl.dpo-anta.ru']['phone'] = '+7 (987) 291-86-08';
	$regions['yaroslavl.dpo-anta.ru']['working_hours'] = 'с 10:00 до 20:00';
	$regions['yaroslavl.dpo-anta.ru']['ya_coord'] = '55.826511, 49.148924';
