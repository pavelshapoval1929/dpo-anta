<?php
header("Content-type: text/xml");
require_once($_SERVER['DOCUMENT_ROOT']. '/wp-load.php');
require_once 'regions.php';

function sitemap() {
	$output = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n
    <urlset xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\">\n";
	echo $output;
	//readfile( 'main-sitemap.xsl' );

	$uri = $_SERVER['REQUEST_URI'];
	$path = parse_url($uri, PHP_URL_PATH);
	$scheme = $_SERVER['HTTPS'];
	if ($scheme) $scheme = 'https';
	else $scheme = 'http';
	if ($path == '/sitemap.xml' || $path == '/sitemap.xml/') {
		// Получаем записи и страницы
		$args = array (
			'numberposts' => -1,
			'post_status' => 'publish',
			'post_type' => array('post', 'page', 'kursi')
		);


		$pages = get_posts( $args );

		// получаем таксономии

		$terms = get_terms( array(
			'taxonomy'      => array( 'category', 'kursicat'),
		) );
		$items = array_merge($pages,$terms);
		$priority = "0.8";
		$changefreq = "monthly";
		foreach( $items as $item ) {
			if(!$item->ID){
				$url = get_term_link($item->term_id);
				$modified = '';
			}else{
				$url = get_permalink($item->ID);
				$modified = "\t\t<lastmod>".get_the_modified_date( 'Y-m-d', $item->ID )."</lastmod>\n";
			}
			echo "\t<url>\n\t\t<loc>".$url."</loc>\n".$modified."\t\t<changefreq>".$changefreq."</changefreq>\n\t\t<priority>".$priority."</priority>\n\t</url>\n";
		}
		echo '</urlset>';


		die();
	}


}

sitemap();