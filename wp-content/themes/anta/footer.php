<footer class="footer">
	<div class="footer_sidebar">
	    <?php  switch_to_blog( 1 ); ?>
			<?php if ( is_active_sidebar( 'sidebar_foot_left' ) ) : ?>	
 
				<?php dynamic_sidebar( 'sidebar_foot_left' ); ?>	
 
			<?php endif; ?>
		<div class="col-xs-12 col-md-4 sidebar_foot">
		</div>
			<?php if ( is_active_sidebar( 'sidebar_foot_right' ) ) : ?>	
 
				<?php dynamic_sidebar( 'sidebar_foot_right' ); ?>	
 
			<?php endif; ?>
			<?php restore_current_blog(); ?>
	</div>
      <p>Copyright &copy; 2019</p>
    <script>
        $(document).on('click', '[data-toggle="lightbox"]', function(event) {
            event.preventDefault();
            $(this).ekkoLightbox();
        });
    </script>
    </footer>
    <?php wp_footer(); ?>
  </body>
</html>