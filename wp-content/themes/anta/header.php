﻿<!DOCTYPE html>
<html <?php language_attributes(); ?>>
 <head>
<!-- Yandex.Metrika counter -->
<script type="text/javascript" >
   (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
   m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
   (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

   ym(57069310, "init", {
        clickmap:true,
        trackLinks:true,
        accurateTrackBounce:true,
        webvisor:true,
        ecommerce:"dataLayer"
   });
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/57069310" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-156422557-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-156422557-1');
</script>

   <title><?php bloginfo('name'); ?> &raquo; <?php is_front_page() ? bloginfo('description') : wp_title(''); ?></title>
   <meta charset="<?php bloginfo( 'charset' ); ?>">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>">
   <?php wp_enqueue_script("jquery"); ?>
   <?php wp_head(); ?>
 </head>
 <body <?php body_class(); ?>>
   <header >
   <section class="nav_section">
 
   <div class="container-fluid">
       
      <?php  if ( has_nav_menu( 'start-menu' ) ){ ?>
  
         <nav class="navbar navbar-default main_nav">
 
           <div class="navbar-header">
 
             <button type="button" class="navbar-toggle collapsed" 
              data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
 
                <span class="sr-only">Меню</span>
 
                <span class="icon-bar"></span>
 
                <span class="icon-bar"></span>
 
                <span class="icon-bar"></span>
 
              </button>
 
            </div>
 
            <!-- Collect the nav links, forms, and other content for toggling -->
 
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
 
              <?php /* Primary navigation */
 
                wp_nav_menu( array(
 
                  'theme_location' => 'start-menu',
 
                  'depth' => 2,
 
                  'container' => false,
 
                  'menu_class' => 'nav navbar-nav',
 
                  //Process nav menu using our custom nav walker
 
                  'walker' => new wp_bootstrap_navwalker())
 
                );
 
             ?>
 
           </div><!-- /.navbar-collapse -->
 
         </nav>
         <?php } ?>
 
   </div>
 
 </section>
 <div class="descript">
 <div class="col-xs-12 col-md-4 my-logo">
   <?php //wp_nav_menu( array( 'theme_location' => 'start-menu', 'menu_class' => 'nav-menu' ) ); ?>
   <h1><a href="<?php echo esc_url( home_url( '/' ) ); ?>"><?php bloginfo('name'); ?></a></h1>
   
 <?php //wp_nav_menu( array( 'theme_location' => 'header-menu', 'menu_class' => 'nav-menu' ) ); ?>
 </div>
    <?php if ( is_active_sidebar( 'sidebar_head_center' ) ) : ?> 
 
        <?php dynamic_sidebar( 'sidebar_head_center' ); ?>  
 
    <?php endif; ?>
    <?php if ( is_active_sidebar( 'sidebar_head_right' ) ) : ?> 
 
        <?php dynamic_sidebar( 'sidebar_head_right' ); ?>  
 
    <?php endif; ?>
</div>
 <section class="nav_section">
 
   <div class="container-fluid">
 
         <nav class="navbar navbar-default main_nav">
 
           <div class="navbar-header">
 
             <button type="button" class="navbar-toggle collapsed" 
              data-toggle="collapse" data-target="#bs-example-navbar-collapse-2" aria-expanded="false">
 
                <span class="sr-only">Меню</span>
 
                <span class="icon-bar"></span>
 
                <span class="icon-bar"></span>
 
                <span class="icon-bar"></span>
 
              </button>
 
            </div>
 
            <!-- Collect the nav links, forms, and other content for toggling -->
 
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">
 
              <?php /* Primary navigation */
 
                wp_nav_menu( array(
 
                  'theme_location' => 'header-menu',
 
                  'depth' => 2,
 
                  'container' => false,
 
                  'menu_class' => 'nav navbar-nav',
 
                  //Process nav menu using our custom nav walker
 
                  'walker' => new wp_bootstrap_navwalker())
 
                );
 
             ?>
 
           </div><!-- /.navbar-collapse -->
 
         </nav>
 
   </div>
 
 </section>
 </header>
 