<?php
/*
Template Name: Шаблон: Рабочие профессии
*/
?>

<?php get_header(); ?>
<main >
     <?php  switch_to_blog( 1 ); ?>
  <section class="container-fluid">
    <article class="row">
      <div class="slider-work">
        <?php echo do_shortcode( '[rev_slider alias="slider-1"][/rev_slider]' ); ?>
      </div>
    </article>
    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
    <article class="row">

              <div class="col-sm-2 col-sm-offset-1 col-xs-8 col-xs-offset-2 question">
              <img src="<? echo get_template_directory_uri()?>/img/icons3.png"  alt="">
              <p>
              Уезжаете на вахту?
              </p>
              </div>
              <div class="col-sm-2 col-sm-offset-0 col-xs-8 col-xs-offset-2 question">
              <img src="<? echo get_template_directory_uri()?>/img/icons5.png"  alt="">
              <p>
              Нет времени проходить очное обучение
              </p>
              </div>
              <div class="col-sm-2 col-sm-offset-0 col-xs-8 col-xs-offset-2 question">
              <img src="<? echo get_template_directory_uri()?>/img/icons2.png"  alt="">
              <p>
              Хотите повышения разряда?
              </p>
              </div>
              <div class="col-sm-2 col-sm-offset-0 col-xs-8 col-xs-offset-2 question">
              <img src="<? echo get_template_directory_uri()?>/img/icons4.png"  alt="">
              <p>
              Хотите официально устроиться на работу?
              </p>
              </div>
              <div class="col-sm-2 col-sm-offset-0 col-xs-8 col-xs-offset-2 question">
              <img src="<? echo get_template_directory_uri()?>/img/icons6.png"  alt="">
              <p>
              Есть опыт, но нет документов?
              </p>
              </div>
    </article>
    <?php
        $mass = get_post_meta( $post->ID, 'textrank');
        if (!empty($mass)){ ?>
    <article class="row work">
        <header>
       
          <p>Какой разряд <?php the_title(); ?>а вам нужен?</p>
          
        </header>
        <div class="expert-work">

          <?php the_content();?>

          <script>
              $(function(){ 
                // Отображается 1 вкладка, 
                // т.к. отсчёт начинается с нуля
                  $("#myTab2 li:eq(0) a").tab('show');
              });
          </script>
 
            <ul id="myTab2" class="nav nav-tabs">
              <?
                    $fields = get_post_meta( $post->ID, 'rank');
                    $fieldstext = get_post_meta( $post->ID, 'textrank');
                    $das = 1;
                    foreach ($fields as $field) {
                      $ru = '#panely'.$das;

              ?>
                <li><a data-toggle="tab" href="<? echo $ru; ?>" ><?echo $field; ?></a></li>
             <?  $das= $das+1;
                }
                  $das=1; ?>
             </ul>
 
            <div class="tab-content">
              <? foreach ($fieldstext as $fieldt) {
                  $ru = 'panely'.$das;
                  ?>
                  <div id="<? echo $ru; ?>" class="tab-pane fade">
                      <p><?echo $fieldt; ?></p>
                  </div>
             <? $das= $das+1;
              } ?>


            </div>
        </div>
    </article>
    <?php } ?>
     <?php endwhile; else : ?>
    <article>
        <p>Извините, записи не были найдены!</p>
    </article>
     <?php endif; ?>

    <article class="row">
        <div class="carts">
            <header>
              <p>Сколько стоит обучение <?php the_title(); ?>а ?</p>
            </header>
            <?php restore_current_blog(); ?>
            <!--<div class="col-md-1"> </div>-->
            <div class="col-xs-12 col-md-8">
              <div class="cena row-flex">
                <div class="polovl">
                  <div class="polovl-item">
                    <p>У нас самые низкие цены в Омске!</p>
                  </div>
                  <!--<img src="<? echo get_template_directory_uri()?>/img/slo101.png"  alt="">-->
                </div>
                  <div class="polovr">
                  <div class="polovr-item">
                    <p>Нашли дешевле? Скинте нам прайс и мы сделаем вам скидку</p>
                  </div>
                  <!--<img src="<? echo get_template_directory_uri()?>/img/slo102.png"  alt="">-->
                </div>
              </div>
              <div class="cart">
                  <p>Также, для прохождения обучения вам понадобится:</p>
                  <div class="cart-img">
                    <div class="cart-item">
                        <img src="<? echo get_template_directory_uri()?>/img/icons11.png"  alt="">
                        <P>Заключить<br>договор</P>
                    </div> 
                    <div class="cart-item">
                        <img src="<? echo get_template_directory_uri()?>/img/icons7.png"  alt="">
                        <p>Паспорт</p>
                    </div>
                    <div class="cart-item">
                        <img src="<? echo get_template_directory_uri()?>/img/icons8.png"  alt="">
                        <p>Иногда<br>подтвердить<br>стаж</p>
                    </div>
                    <div class="cart-item">
                        <img src="<? echo get_template_directory_uri()?>/img/icons9.png"  alt="">
                        <p>Предоплата</p>
                    </div>
                    <div class="cart-item">
                        <img src="<? echo get_template_directory_uri()?>/img/icons10.png"  alt="">
                        <p>Фотография<br>если есть</p>
                    </div>                
                  </div>
              </div>
            </div>
              <!--<div class="col-lg-1"></div>-->
              <div class="col-xs-12 col-md-4 bort-lef">
                  <?php echo do_shortcode( '[contact-form-7 id="75" title="Контактная форма 1"]' ); ?>
          
            </div>
          
        </div>
    </article>
    <article class="row">
        <div class="col-xs-12 col-md-12 obraz">
         <p>Образцы выдоваемых документов</p>
            <div class="obraz-galeri">
                <div class="galeri-sect">
                    <div class="galeri-sect-item">
                    <a href="http://dpo-anta.ru/wp-content/uploads/2019/10/Banquet_Table.jpg" data-toggle="lightbox">
                      <img src="http://dpo-anta.ru/wp-content/uploads/2019/10/Banquet_Table.jpg" alt=""  class="img-thumbnail"/></a>
                    </div>
                    <div class="galeri-sect-item">
                      <a href="http://dpo-anta.ru/wp-content/uploads/2019/10/QmKNpLW8CrE.jpg" data-toggle="lightbox">
                      <img src="http://dpo-anta.ru/wp-content/uploads/2019/10/QmKNpLW8CrE.jpg" alt="" class="img-thumbnail"/></a>
                    </div>
                    <div class="galeri-sect-item">
                      <a href="http://dpo-anta.ru/wp-content/uploads/2019/10/feedback-1024x992.jpg" data-toggle="lightbox">
                      <img src="http://dpo-anta.ru/wp-content/uploads/2019/10/feedback-1024x992.jpg" alt="" class="img-thumbnail"/></a>
                    </div>
                </div> 
                <div class="galeri-sect">
                     <div class="galeri-sect-item">
                      <a href="http://dpo-anta.ru/wp-content/uploads/2019/10/Banquet_Table.jpg" data-toggle="lightbox">
                      <img src="http://dpo-anta.ru/wp-content/uploads/2019/10/Banquet_Table.jpg" alt=""  class="img-thumbnail"/></a>
                    </div>
                    <div class="galeri-sect-item">
                      <a href="http://dpo-anta.ru/wp-content/uploads/2019/10/QmKNpLW8CrE.jpg" data-toggle="lightbox">
                      <img src="http://dpo-anta.ru/wp-content/uploads/2019/10/QmKNpLW8CrE.jpg" alt="" class="img-thumbnail"/></a>
                    </div>
                    <div class="galeri-sect-item">
                    <a href="http://dpo-anta.ru/wp-content/uploads/2019/10/feedback-1024x992.jpg" data-toggle="lightbox">
                      <img src="http://dpo-anta.ru/wp-content/uploads/2019/10/feedback-1024x992.jpg" alt="" class="img-thumbnail"/></a>
                    </div>
                </div> 
            </div>


              <!--<ul class="wp-block-gallery columns-3 is-cropped">
              <li class="blocks-gallery-item"><figure>
              <img src="http://dpo-anta.ru/wp-content/uploads/2019/10/Banquet_Table.jpg" alt=""  class="img-thumbnail"/>
              </figure></li>
              <li class="blocks-gallery-item"><figure>
              <img src="http://dpo-anta.ru/wp-content/uploads/2019/10/dsc02323.jpg" alt="" class="img-thumbnail"/>
              </figure></li>
              <li class="blocks-gallery-item"><figure>
              <img src="http://dpo-anta.ru/wp-content/uploads/2019/10/feedback-1024x992.jpg" alt="" class="img-thumbnail"/>
              </figure></li>
              <li class="blocks-gallery-item"><figure>
              <img src="http://dpo-anta.ru/wp-content/uploads/2019/10/Banquet_Table.jpg" alt="" class="img-thumbnail"/>
              </figure></li>
              <li class="blocks-gallery-item"><figure>
              <img src="http://dpo-anta.ru/wp-content/uploads/2019/10/dsc02323.jpg" alt="" class="img-thumbnail"/>
              </figure></li>
              <li class="blocks-gallery-item"><figure>
              <img src="http://dpo-anta.ru/wp-content/uploads/2019/10/feedback-1024x992.jpg" alt="" class="img-thumbnail"/>
              </figure></li>
              </ul>-->
          <div class="attention">
            <p>*Внимание!<br> Опасайтесь подделок. Многие учебные центры работают без должной образовательной лицензии. Проверяйте документы!</p>
          </div>
        </div>
    </article>
    <article class="row no-gutter">
        <div class="stories no-gutter">
        <div class="col-xs-12 col-md-6 item1">
                <div class="col-xs-12 col-md-10 col-md-offset-1 stories-hedrs">
                   <p>Учебный центр <br>“ПрофБизнесСтандарт” <br>Нам можно доверять!</p>
                </div>
                <div class="col-xs-12 col-md-10 col-md-offset-1 stories-cont">
                     <p>Уж более 5 лет работает с ведущими предприятиями газовой, перерабатывающей, строительной, транстпортной, нефтедобывающей, химической, энергетической, лесной промышленности</p>
                </div>
              <div class="col-xs-12 col-md-12 stories-logo">
                  <img src="<? echo get_template_directory_uri()?>/img/gazprom.png" alt="" class=""/>
                  <img src="<? echo get_template_directory_uri()?>/img/rsk.png" alt="" class=""/>
                  <img src="<? echo get_template_directory_uri()?>/img/logo-rosseti.png" alt="" class=""/>
                  <img src="<? echo get_template_directory_uri()?>/img/trans.png" alt="" class=""/>
                  <img src="<? echo get_template_directory_uri()?>/img/rossneft.png" alt="" class=""/>
                  <img src="<? echo get_template_directory_uri()?>/img/Sberbank.png" alt="" class=""/>
                  <img src="<? echo get_template_directory_uri()?>/img/logotip-novatek.png" alt="" class=""/>
                  <img src="<? echo get_template_directory_uri()?>/img/severstal.png" alt="" class=""/>
              </div>
              <button type="button" class="btn btn-primary btn-lg">История учебного центра </button>
        </div>
        <div class="col-xs-12 col-md-6 item2">
          
            <img src="http://dpo-anta.ru/wp-content/uploads/2019/10/stories.jpg" alt="" class=""/>
        </div>
        </div>
    </article>
    <article class="row">
        <div class="col-xs-12 col-md-12 discover">
        <div class="discover-arrows">
              <p class="arrows-hedr">Узнай стоимость прямо сейчас!</p>
          <p>Заполни форму и узнай стоимость обучения</p>
            <button type="button" class="btn btn-primary btn-lg">Узнать стоимость </button>
        </div>
        </div>
    </article>
    <article class="row no-gutter">
        <div class="col-xs-12 col-md-12 licenz">
         <div class="col-xs-12 col-md-6 item1">
          <p>ПБС</p>
          <p>АНО ДПО учебный центр ПрофБизнесСтандарт работает в рамках закона.Все выдаваемые документы проходят проверку в службе безопастности.
            В случае проверки, мы предоставим подтверждаюзие документы заверенные печатью учебного центра</p>

        </div>
        <div class="col-xs-12 col-md-6 item2">
          <p>Лицензии учебного центра</p>
          <div class="item2-img">
          <img src="http://placehold.it/300x150" alt="" class=""/>
          <img src="http://placehold.it/300x150" alt="" class=""/>
          </div>
        </div>
        </div>
    </article>
    <article class="row list">
      <div class="list-heder">
          <div class="col-xs-12 col-md-6 list-left">
            <p>Список профессий по которым <br>идет обучение</p>
          </div>
          <div class="col-xs-12 col-md-6 list-right">
            <p><span>Скидка 20%</span><br>на все специальности,<br>при обучении от 5 человек </p>
          </div>
        
      </div>
      <div class="col-xs-12 col-md-12 list-item">
        
        <div class="panel-group" id="accordion">
  <!-- 1 панель -->
  <div class="panel panel-default">
    <!-- Заголовок 1 панели -->
    <div class="panel-heading">
      <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">20 Самых популярных профессий</a>
      </h4>
    </div>
    <div id="collapseOne" class="panel-collapse collapse in">
      <!-- Содержимое 1 панели -->
      <div class="panel-body">
      <ul>
				 <?php kama_get_most_viewed("num=10 &key=views &cache=1 &days=9 "); ?>
			</ul>
        <p>Bootstrap 3 - это framework для быстрого и гармоничного создания дизайна сайта. Начать изучать эту технологию лучше с <a href="http://itchief.ru/lessons/bootstrap-3/19-introduction-to-twitter-bootstrap-3" target="_blank">урока</a>, который познакомит вас с её возможностями и преимуществами.</p>
      </div>
    </div>
  </div>
  <!-- 2 панель -->
  <div class="panel panel-default">
    <!-- Заголовок 2 панели -->
    <div class="panel-heading">
      <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">Машинисты</a>
      </h4>
    </div>
    <div id="collapseTwo" class="panel-collapse collapse">
      <!-- Содержимое 2 панели -->
      <div class="panel-body">
        <p>Изучение технологии лучше всего начать с простого <a href="http://itchief.ru/lessons/bootstrap-3/20-lesson-2-getting-started-with-twitter-bootstrap-3" target="_blank">урока</a>. На котором изучается как подключить Twitter Bootstrap к своему проекту и вывести с помощью него простейший текст.</p>
      </div>
    </div>
  </div>
  <!-- 3 панель -->
  <div class="panel panel-default">
    <!-- Заголовок 3 панели -->
    <div class="panel-heading">
      <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">Слесари</a>
      </h4>
    </div>
    <div id="collapseThree" class="panel-collapse collapse">
      <!-- Содержимое 3 панели -->
      <div class="panel-body">
      <?php
      echo do_shortcode( '[test_attachment_url $profession = "Лаборант"]' ); ?>
        <p></p>
      </div>
    </div>
  </div>
</div>
      </div>

    </article>
    <article class="row">
        <div class="otziv">
        <p>Довольные клиенты</p>
         <div class="item1">
         <img src="http://placehold.it/263x263" alt="" class=""/>
         <img src="http://placehold.it/263x263" alt="" class=""/>
         <img src="http://placehold.it/263x263" alt="" class=""/>
         <img src="http://placehold.it/263x263" alt="" class=""/>
         <!--</div>
         <div class="item1">-->
         <img src="http://placehold.it/263x263" alt="" class=""/>
         <img src="http://placehold.it/263x263" alt="" class=""/>
         <img src="http://placehold.it/263x263" alt="" class=""/>
         <img src="http://placehold.it/263x263" alt="" class=""/>

         </div>
         </div>
      </article>
    <article class="row">
        <div class="doubt">
            <!--<div class="col-md-1"> </div>-->
            <div class="col-xs-12 col-md-8 doubt-row">
                <div class="doubtl">
                  
                </div>
                  <div class="doubtr">
                  <p><span>Все еще сомневаетесь?</span><br> Позвоните нам или закажите звонок</p>
                  <img src="<? echo get_template_directory_uri()?>/img/doubt.png" alt="" class=""/>
                
                </div>
              
            </div>
             
              <div class="col-xs-12 col-md-4 bort-lef">
                  <?php echo do_shortcode( '[contact-form-7 id="75" title="Контактная форма 1"]' ); ?>
          
            </div>
          
        </div>
    </article>
  </section>
  <?php get_sidebar(); ?>
</main>
<?php get_footer(); ?>