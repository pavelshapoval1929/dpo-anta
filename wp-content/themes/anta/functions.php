<?php
// Эта функция помещает файл Normalize.css в очередь для использования. Первый параметр это имя таблицы стилей, второе это URL. Здесь мы
// используем онлайн версию css файла.
function add_normalize_CSS() {
    wp_enqueue_style( 'normalize-styles', get_template_directory_uri() . '/css/normalize.min.css');
}
// Регистрирует новую боковую панель под названием 'sidebar'
function add_widget_Support() {
                register_sidebar( array(
                                'name' => 'Sidebar',
                                'id' => 'sidebar',
                                'before_widget' => '<div>',
                                'after_widget' => '</div>',
                                'before_title' => '<h2>',
                                'after_title' => '</h2>',
                ) );
                register_sidebar( array(
                                'name' => 'Sidebar Header Center',
                                'id' => 'sidebar_head_center',
                                'before_widget' => '<div class="col-xs-12 col-md-4 sidebar_header">',
                                'after_widget' => '</div>',
                ) );
                register_sidebar( array(
                                'name' => 'Sidebar Header Right',
                                'id' => 'sidebar_head_right',
                                'before_widget' => '<div class="col-xs-12 col-md-4 sidebar_header">',
                                'after_widget' => '</div>',
                ) );
                register_sidebar( array(
                                'name' => 'Sidebar Footer Left',
                                'id' => 'sidebar_foot_left',
                                'before_widget' => '<div class="col-xs-12 col-md-4 sidebar_foot">',
                                'after_widget' => '</div>',
                                'before_title' => '<p class="foot-left"><span class="glyphicon glyphicon-list-alt"></span>',
                                'after_title' => '</p>',
                ) );
                register_sidebar( array(
                                'name' => 'Sidebar Footer Right',
                                'id' => 'sidebar_foot_right',
                                'before_widget' => '<div class="col-xs-12 col-md-4 sidebar_foot">',
                                'after_widget' => '</div>',
                                'before_title' => '<p class="foot-right"><span class="glyphicon glyphicon-map-marker"></span>',
                                'after_title' => '</p>',
                ) );
}
// Подхватывает(hook) инициацию виджета и запускает нашу функцию
add_action( 'widgets_init', 'add_Widget_Support' );

// Регистрирует новое навигационное меню
//function add_Main_Nav() {
 // register_nav_menu('header-menu',__( 'Header Menu' ));
//}
// Подхватывает (hook) init хук-событие, запускает функцию нашего навигационного меню
//add_action( 'init', 'add_Main_Nav' );

register_nav_menus( array( 
        'header-menu' => 'Header Menu', 
        'start-menu' => 'Start Menu', 
        'footer_menu' => 'Footer_menu', 
    ));


/**
* регетрируем js и css bootstrap
*/
function wpt_register_js() {
    wp_register_script('jquery.bootstrap.min', get_template_directory_uri() . '/js/bootstrap.min.js', 'jquery');
    wp_enqueue_script('jquery.bootstrap.min');
    wp_register_script('ekko-lightbox.min', get_template_directory_uri() . '/js/ekko-lightbox.min.js', 'jquery');
    wp_enqueue_script('ekko-lightbox.min');

}
add_action( 'init', 'wpt_register_js' );
function wpt_register_css() {
    wp_register_style( 'bootstrap.min', get_template_directory_uri() . '/css/bootstrap.min.css' );
    wp_enqueue_style( 'bootstrap.min' );
    wp_register_style( 'ekko-lightbox', get_template_directory_uri() . '/css/ekko-lightbox.css' );
    wp_enqueue_style( 'ekko-lightbox' );
}
add_action( 'wp_enqueue_scripts', 'wpt_register_css' );


//Register custom navigation walker

require_once ('wp_bootstrap_navwalker.php');

if (!is_admin()) {
        wp_deregister_script('jquery');
        wp_register_script('jquery', ("https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"), false, '3.4.1');
        wp_enqueue_script('jquery');
}


function new_taxonomies_for_pages() {
 register_taxonomy_for_object_type( 'post_tag', 'page' );
 register_taxonomy_for_object_type( 'category', 'page' );
 }
add_action( 'init', 'new_taxonomies_for_pages' );
if ( ! is_admin() ) {
 add_action( 'pre_get_posts', 'tag_cat_archives' );
}
function tag_cat_archives( $wp_query ) {
$my_taxonomies_array = array('post','page');
 if ( $wp_query->get( 'category_name' ) || $wp_query->get( 'cat' ) )
 $wp_query->set( 'post_type', $my_taxonomies_array );
 
 if ( $wp_query->get( 'tag' ) )
 $wp_query->set( 'post_type', $my_taxonomies_array );
}


add_shortcode('test_attachment_url', 'test_attachment_url');
function test_attachment_url($profession){
    $aaa ='';
    $parametri = array(
      'post_type' => 'page', /* Отбираем только записи. */
      'posts_per_page' => -1,
       'post_parent' => 2, /* Снимаем ограничение на количество показываемых записей на одну страничку. */
    );
    $moi_zapros = null;
    $moi_zapros = new WP_Query($parametri); /* Формируем новый "нестандартный" запрос. */
    if ($moi_zapros->have_posts()):
    	while ($moi_zapros->have_posts()) : $moi_zapros->the_post(); 
    		$idt = get_the_ID();
    		$url = get_permalink($idt);
    		$prof = get_the_title($idt);
    		if ($profession == $prof ) {
    			$aaa = $aaa.'<div class="col-xs-6 col-md-3 work_tab"><a href="'.$url.'">'.get_the_title($idt).'</a></div>';
    		}
    		else {
    			$aaa = $aaa.'<div class="col-xs-6 col-md-3 work_tab"><a href="'.$url.'">'.get_the_title($idt).'</a></div>';
    		}
    			
    	endwhile;
    endif;
    wp_reset_query();  /* Сбрасываем нашу выборку. */

    return $aaa;
}



/* Подсчет количества посещений страниц
---------------------------------------------------------- */
add_action('wp_head', 'kama_postviews');
function kama_postviews() {

/* ------------ Настройки -------------- */
$meta_key       = 'views';  // Ключ мета поля, куда будет записываться количество просмотров.
$who_count      = 0;            // Чьи посещения считать? 0 - Всех. 1 - Только гостей. 2 - Только зарегистрированных пользователей.
$exclude_bots   = 0;            // Исключить ботов, роботов, пауков и прочую нечесть :)? 0 - нет, пусть тоже считаются. 1 - да, исключить из подсчета.

global $user_ID, $post;
	if(is_singular()) {
		$id = (int)$post->ID;
		static $post_views = false;
		if($post_views) return true; // чтобы 1 раз за поток
		$post_views = (int)get_post_meta($id,$meta_key, true);
		$should_count = false;
		switch( (int)$who_count ) {
			case 0: $should_count = true;
				break;
			case 1:
				if( (int)$user_ID == 0 )
					$should_count = true;
				break;
			case 2:
				if( (int)$user_ID > 0 )
					$should_count = true;
				break;
		}
		if( (int)$exclude_bots==1 && $should_count ){
			$useragent = $_SERVER['HTTP_USER_AGENT'];
			$notbot = "Mozilla|Opera"; //Chrome|Safari|Firefox|Netscape - все равны Mozilla
			$bot = "Bot/|robot|Slurp/|yahoo"; //Яндекс иногда как Mozilla представляется
			if ( !preg_match("/$notbot/i", $useragent) || preg_match("!$bot!i", $useragent) )
				$should_count = false;
		}

		if($should_count)
			if( !update_post_meta($id, $meta_key, ($post_views+1)) ) add_post_meta($id, $meta_key, 1, true);
	}
	return true;
}




/**
 * Функция для вывода записей по произвольному полю содержащему числовое значение.
 *
 * Пример вызова:
 *     kama_get_most_viewed( "num=5 &key=views &cache=1 &format={a}{title}{/a} - {date:j.M.Y} ({views}) ({comments})" );
 * 
 * @param string $args {
 *
 *     @type int     $num    (10)    Количество постов.
 *     @type string  $key    (views) Ключ произвольного поля, по значениям которого будет проходить выборка.
 *     @type string  $order  (DESC)  Порядок вывода записей. Чтобы вывести сначала менее просматириваемые устанавливаем order=1
 *     @type string  $format  ('')   Формат выводимых ссылок. По дефолту такой: ({a}{title}{/a}).
 *                                   Можно использовать, например, такой:
 *                                   {date:j.M.Y} - {a}{title}{/a} ({views}, {comments}).
 *     @type int     $days   (0)     Число последних дней, записи которых нужно вывести
 *                                   по количеству просмотров. Если указать год (2011,2010),
 *                                   то будут отбираться популярные записи за этот год.
 *     @type int     $cache  (0)     Использовать кэш или нет.  Варианты 1 - кэширование включено, 0 - выключено (по дефолту).
 *     @type string  $echo   (1)     Выводить на экран или нет. Варианты 1 - выводить (по дефолту), 0 - вернуть для обработки (return).
 * }
 *
 * @return bool|int|mixed|string
 *
 * @ver 1.0
 */
function kama_get_most_viewed( $args = '' ){
	global $wpdb, $post;

	parse_str( $args, $i );

	$num    = isset( $i['num'] )    ? (int) $i['num'] : 10;
	$key    = isset( $i['key'] )    ? sanitize_text_field($i['key']) : 'views';
	$order  = isset( $i['order'] )  ? 'ASC' : 'DESC';
	$days   = isset( $i['days'] )   ? (int) $i['days'] : 2019;
	$format = isset( $i['format'] ) ? stripslashes( $i['format'] ) : '';
	$cache  = isset( $i['cache'] );
	$echo   = isset( $i['echo'] )   ? (int) $i['echo'] : 1;

	if( $cache ){
		$cache_key = (string) md5( __FUNCTION__ . serialize( $args ) );

		//получаем и отдаем кеш если он есть
		if( $cache_out = wp_cache_get( $cache_key ) ){ 
			if( $echo )
				return print( $cache_out );
			else
				return $cache_out;
		}
	}

	if( $days ){
		$AND_days = "AND post_date > CURDATE() - INTERVAL $days DAY";
		if( strlen( $days ) == 4 ){
			$AND_days = "AND YEAR(post_date)=" . $days;
		}
	}

	$sql = "SELECT p.ID, p.post_title, p.post_date, p.guid, p.comment_count, (pm.meta_value+0) AS views
	FROM $wpdb->posts p
		LEFT JOIN $wpdb->postmeta pm ON (pm.post_id = p.ID)
	WHERE pm.meta_key = '$key' $AND_days
		AND p.post_type = 'post'
		AND p.post_status = 'publish'
	ORDER BY views $order LIMIT $num";
	$results = $wpdb->get_results( $sql );
	if( ! $results ){
		return false;
	}

	$out = $x = '';
	preg_match( '!{date:(.*?)}!', $format, $date_m );

	foreach( $results as $pst ){

		$x = ( $x == 'li1' ) ? 'li2' : 'li1';

		if( $pst->ID == $post->ID )
			$x .= ' current-item';

		$Title    = $pst->post_title;
		$a1       = '<a href="' . get_permalink( $pst->ID ) . "\" title=\"{$pst->views} просмотров: $Title\">";
		$a2       = '</a>';
		$comments = $pst->comment_count;
		$views    = $pst->views;

		if( $format ){

			$date    = apply_filters( 'the_time', mysql2date( $date_m[ 1 ], $pst->post_date ) );
			$Sformat = str_replace( $date_m[ 0 ], $date, $format );
			$Sformat = str_replace( [ '{a}', '{title}', '{/a}', '{comments}', '{views}' ], [ $a1, $Title, $a2, $comments, $views, ], $Sformat );
		}
		else
			$Sformat = $a1 . $Title . $a2;

		$out .= "<li class=\"$x\">$Sformat</li>";
	}

	if( $cache )
		wp_cache_add( $cache_key, $out );

	if( $echo )
		echo $out;
	else
		return $out;
}



