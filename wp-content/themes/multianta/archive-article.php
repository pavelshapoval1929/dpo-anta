<?php get_header(); ?>
<main class="wrap">
  <section class="container-fluid">
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
      <article class="row">
        <header>
          <h2><?php the_title(); ?></h2>
        </header>
        <div class="row">

        <?php the_content(); ?>
              </div>
      </article>
<?php endwhile; else : ?>
      <article>
        <p>Извините, записи не были найдены!</p>
      </article>
<?php endif; ?>
  </section><?php get_sidebar(); ?>
</main>
<?php get_footer(); ?>