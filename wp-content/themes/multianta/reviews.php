<?php
/**
Template Name: reviews
*/
include 'regions.php';
?>
<?php get_header(); ?>
<section class="sc11">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
				<?php if( function_exists('kama_breadcrumbs') ) kama_breadcrumbs(); ?>
            </div>
        </div>
    </div>
</section>
<div class="reviews-page-content">
	<section class="">
		<div class="container">
            <div class="page-title"><h1><?php the_title();?></h1></div>
			<div class="row">
<?php
	the_content();
?>
			</div>
		</div>
	</section>
</div>
<?php get_footer(); ?>
