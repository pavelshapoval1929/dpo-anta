<?php get_header(); ?>
    <section class="sc11">
        <p>Ghbdtn category</p>
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <?php 
                        if( function_exists('kama_breadcrumbs') ) kama_breadcrumbs();
                    ?>
                </div>
            </div>
        </div>
    </section>
    <div class="news-content-page">
        <section class="news-content-sc">
            <div class="container">
                <div class="row">
                    <div class="col-lg-4">
                        <div class="wrapper-left">
                            <div class="wrapper-left-content-category">
                                <p class="wrapper-left-content-category-title">Категории</p>
                                <ul>
                                <?php $curTerm = $wp_query->queried_object;
                                    $terms   = get_terms( 'category_new' );
                                    if ( ! empty( $terms ) && ! is_wp_error( $terms ) ) {
                                        foreach ( $terms as $term ) {
                                            $classes = array();
                                            if ( $term->name == $curTerm->name ) {
                                                $classes[] = 'active';
                                            }
                                            echo '<li class="' . implode( ' ', $classes ) . '"><a href="' . get_term_link( $term ) . '">' . $term->name . '</a></li>';
                                        }
                                    } ?>
                                </ul>

                                <ul>
                                    <li class="active">Охрана труда</li>
                                    <li>Пожарная безопасность</li>
                                    <li>Продление сертификаций</li>
                                    <li>Онлайн образование</li>
                                    <li>По приказц министерства образования</li>
                                </ul>
                            </div>
                            <div class="wrapper-left-hot-news">
                                <p class="wrapper-left-hot-news-title">
                                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 384 512">
                                        <g id="Слой_2" data-name="Слой 2">
                                            <g id="Capa_1" data-name="Capa 1">
                                                <path class="cls-1" d="M382.91,299.77c-5.87-76.36-41.42-124.21-72.78-166.44C281.08,94.24,256,60.48,256,10.68a10.67,10.67,0,0,0-5.79-9.48A10.53,10.53,0,0,0,239.13,2C192,35.71,152.72,92.52,139,146.73c-9.53,37.73-10.79,80.16-11,108.18-43.5-9.29-53.35-74.36-53.46-75.07A10.73,10.73,0,0,0,69,171.92a10.61,10.61,0,0,0-9.67-.17c-2.28,1.1-56,28.39-59.11,137.35C0,312.73,0,316.36,0,320,0,425.85,86.14,512,192,512a1.55,1.55,0,0,0,.43,0h.13C298.17,511.68,384,425.67,384,320,384,314.67,382.91,299.77,382.91,299.77ZM192,490.65c-35.29,0-64-30.58-64-68.17,0-1.28,0-2.57.08-4.16.43-15.85,3.44-26.67,6.74-33.87C141,397.74,152.07,410,170,410a10.66,10.66,0,0,0,10.67-10.67c0-15.18.31-32.7,4.09-48.51,3.37-14,11.41-28.94,21.6-40.9,4.53,15.52,13.36,28.08,22,40.34,12.34,17.54,25.1,35.68,27.34,66.6.14,1.84.27,3.68.27,5.66C256,460.07,227.29,490.65,192,490.65Z" /> </g>
                                        </g>
                                    </svg>Свежие новости</p>
                                <div class="wrapper-left-hot-news-item">
                                    <p>Продление сертификации в СтройСофт-Информ в 2020 году по приказу менистра образвания </p>
                                </div>
                                <div class="wrapper-left-hot-news-item">
                                    <p>Продление сертификации в СтройСофт-Информ в 2020 году по приказу менистра образвания </p>
                                </div>
                                <div class="wrapper-left-hot-news-item">
                                    <p>Продление сертификации в СтройСофт-Информ в 2020 году по приказу менистра образвания </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-8">
                        <div class="wrapper-right">
                            <p class="wrapper-right-title">Охрана труда</p>
                            <div class="wrapper-right-item"> <img src="<? echo get_template_directory_uri()?>/img/news-content-sc-img1.jpg" alt="" class="img-fluid">
                                <div class="wrapper-right-item-content">
                                    <div class="wrapper-right-item-content-date">
                                        <p>1 января 2020</p>
                                    </div>
                                    <div class="wrapper-right-item-content-see">
                                        <p>
                                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 326.33">
                                                <g id="Слой_2" data-name="Слой 2">
                                                    <g id="Capa_1" data-name="Capa 1">
                                                        <path class="cls-1" d="M508.74,153.21C504.17,147,395.19,0,256,0S7.82,147,3.25,153.2a16.9,16.9,0,0,0,0,19.92C7.82,179.38,116.81,326.33,256,326.33s248.17-146.95,252.74-153.2A16.87,16.87,0,0,0,508.74,153.21ZM256,292.57C153.47,292.57,64.67,195,38.38,163.15,64.63,131.24,153.25,33.76,256,33.76c102.52,0,191.32,97.51,217.61,129.42C447.36,195.09,358.75,292.57,256,292.57Z" />
                                                        <path class="cls-1" d="M256,61.89A101.28,101.28,0,1,0,357.27,163.16,101.39,101.39,0,0,0,256,61.89Zm0,168.79a67.52,67.52,0,1,1,67.51-67.52A67.6,67.6,0,0,1,256,230.68Z" /> </g>
                                                </g>
                                            </svg>124</p>
                                    </div>
                                </div>
                                <p class="wrapper-right-item-content-text">Продление сертификации в СтройСофт-Информ в 2020 году по приказу менистра образвания </p>
                                <p class="wrapper-right-item-content-text">Комплексная услуга по тендерному сопровождению представляет собой комплекс мероприятий и работ, осуществляемых на всех этапах проведения аукциона или конкурса и направленных на заключение контракта по результатам закупки. При этом не важно, речь идет о процедурах выбора поставщика по 44-ФЗ, 223-ФЗ или любых других видах закупок, включая коммерческие.</p>
                                <p class="wrapper-right-item-content-text"><a href="">Читать полностью <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 135.89 240.82"><defs></defs><g id="Слой_2" data-name="Слой 2"><g id="Capa_1" data-name="Capa 1"><path id="Chevron_Right" data-name="Chevron Right" class="cls-1" d="M132.28,111.82,21.29,3.56a12.7,12.7,0,0,0-17.64,0,11.92,11.92,0,0,0,0,17.17l102.18,99.68L3.66,220.08a11.94,11.94,0,0,0,0,17.19,12.73,12.73,0,0,0,17.65,0L132.29,129A12.05,12.05,0,0,0,132.28,111.82Z"/></g></g></svg></a></p>
                            </div>
                            <div class="wrapper-right-item"> <img src="<? echo get_template_directory_uri()?>/img/news-content-sc-img2.jpg" alt="" class="img-fluid">
                                <div class="wrapper-right-item-content">
                                    <div class="wrapper-right-item-content-date">
                                        <p>1 января 2020</p>
                                    </div>
                                    <div class="wrapper-right-item-content-see">
                                        <p>
                                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 326.33">
                                                <g id="Слой_2" data-name="Слой 2">
                                                    <g id="Capa_1" data-name="Capa 1">
                                                        <path class="cls-1" d="M508.74,153.21C504.17,147,395.19,0,256,0S7.82,147,3.25,153.2a16.9,16.9,0,0,0,0,19.92C7.82,179.38,116.81,326.33,256,326.33s248.17-146.95,252.74-153.2A16.87,16.87,0,0,0,508.74,153.21ZM256,292.57C153.47,292.57,64.67,195,38.38,163.15,64.63,131.24,153.25,33.76,256,33.76c102.52,0,191.32,97.51,217.61,129.42C447.36,195.09,358.75,292.57,256,292.57Z" />
                                                        <path class="cls-1" d="M256,61.89A101.28,101.28,0,1,0,357.27,163.16,101.39,101.39,0,0,0,256,61.89Zm0,168.79a67.52,67.52,0,1,1,67.51-67.52A67.6,67.6,0,0,1,256,230.68Z" /> </g>
                                                </g>
                                            </svg>124</p>
                                    </div>
                                </div>
                                <p class="wrapper-right-item-content-text">Продление сертификации в СтройСофт-Информ в 2020 году по приказу менистра образвания </p>
                                <p class="wrapper-right-item-content-text">Комплексная услуга по тендерному сопровождению представляет собой комплекс мероприятий и работ, осуществляемых на всех этапах проведения аукциона или конкурса и направленных на заключение контракта по результатам закупки. При этом не важно, речь идет о процедурах выбора поставщика по 44-ФЗ, 223-ФЗ или любых других видах закупок, включая коммерческие.</p>
                                <p class="wrapper-right-item-content-text"><a href="">Читать полностью <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 135.89 240.82"><defs></defs><g id="Слой_2" data-name="Слой 2"><g id="Capa_1" data-name="Capa 1"><path id="Chevron_Right" data-name="Chevron Right" class="cls-1" d="M132.28,111.82,21.29,3.56a12.7,12.7,0,0,0-17.64,0,11.92,11.92,0,0,0,0,17.17l102.18,99.68L3.66,220.08a11.94,11.94,0,0,0,0,17.19,12.73,12.73,0,0,0,17.65,0L132.29,129A12.05,12.05,0,0,0,132.28,111.82Z"/></g></g></svg></a></p>
                            </div>
                            <div class="wrapper-right-item"> <img src="<? echo get_template_directory_uri()?>/img/news-content-sc-img1.jpg" alt="" class="img-fluid">
                                <div class="wrapper-right-item-content">
                                    <div class="wrapper-right-item-content-date">
                                        <p>1 января 2020</p>
                                    </div>
                                    <div class="wrapper-right-item-content-see">
                                        <p>
                                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 326.33">
                                                <g id="Слой_2" data-name="Слой 2">
                                                    <g id="Capa_1" data-name="Capa 1">
                                                        <path class="cls-1" d="M508.74,153.21C504.17,147,395.19,0,256,0S7.82,147,3.25,153.2a16.9,16.9,0,0,0,0,19.92C7.82,179.38,116.81,326.33,256,326.33s248.17-146.95,252.74-153.2A16.87,16.87,0,0,0,508.74,153.21ZM256,292.57C153.47,292.57,64.67,195,38.38,163.15,64.63,131.24,153.25,33.76,256,33.76c102.52,0,191.32,97.51,217.61,129.42C447.36,195.09,358.75,292.57,256,292.57Z" />
                                                        <path class="cls-1" d="M256,61.89A101.28,101.28,0,1,0,357.27,163.16,101.39,101.39,0,0,0,256,61.89Zm0,168.79a67.52,67.52,0,1,1,67.51-67.52A67.6,67.6,0,0,1,256,230.68Z" /> </g>
                                                </g>
                                            </svg>124</p>
                                    </div>
                                </div>
                                <p class="wrapper-right-item-content-text">Продление сертификации в СтройСофт-Информ в 2020 году по приказу менистра образвания </p>
                                <p class="wrapper-right-item-content-text">Комплексная услуга по тендерному сопровождению представляет собой комплекс мероприятий и работ, осуществляемых на всех этапах проведения аукциона или конкурса и направленных на заключение контракта по результатам закупки. При этом не важно, речь идет о процедурах выбора поставщика по 44-ФЗ, 223-ФЗ или любых других видах закупок, включая коммерческие.</p>
                                <p class="wrapper-right-item-content-text"><a href="">Читать полностью <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 135.89 240.82"><defs></defs><g id="Слой_2" data-name="Слой 2"><g id="Capa_1" data-name="Capa 1"><path id="Chevron_Right" data-name="Chevron Right" class="cls-1" d="M132.28,111.82,21.29,3.56a12.7,12.7,0,0,0-17.64,0,11.92,11.92,0,0,0,0,17.17l102.18,99.68L3.66,220.08a11.94,11.94,0,0,0,0,17.19,12.73,12.73,0,0,0,17.65,0L132.29,129A12.05,12.05,0,0,0,132.28,111.82Z"/></g></g></svg></a></p>
                            </div>
                            <div class="wrapper-right-item"> <img src="<? echo get_template_directory_uri()?>/img/news-content-sc-img2.jpg" alt="" class="img-fluid">
                                <div class="wrapper-right-item-content">
                                    <div class="wrapper-right-item-content-date">
                                        <p>1 января 2020</p>
                                    </div>
                                    <div class="wrapper-right-item-content-see">
                                        <p>
                                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 326.33">
                                                <g id="Слой_2" data-name="Слой 2">
                                                    <g id="Capa_1" data-name="Capa 1">
                                                        <path class="cls-1" d="M508.74,153.21C504.17,147,395.19,0,256,0S7.82,147,3.25,153.2a16.9,16.9,0,0,0,0,19.92C7.82,179.38,116.81,326.33,256,326.33s248.17-146.95,252.74-153.2A16.87,16.87,0,0,0,508.74,153.21ZM256,292.57C153.47,292.57,64.67,195,38.38,163.15,64.63,131.24,153.25,33.76,256,33.76c102.52,0,191.32,97.51,217.61,129.42C447.36,195.09,358.75,292.57,256,292.57Z" />
                                                        <path class="cls-1" d="M256,61.89A101.28,101.28,0,1,0,357.27,163.16,101.39,101.39,0,0,0,256,61.89Zm0,168.79a67.52,67.52,0,1,1,67.51-67.52A67.6,67.6,0,0,1,256,230.68Z" /> </g>
                                                </g>
                                            </svg>124</p>
                                    </div>
                                </div>
                                <p class="wrapper-right-item-content-text">Продление сертификации в СтройСофт-Информ в 2020 году по приказу менистра образвания </p>
                                <p class="wrapper-right-item-content-text">Комплексная услуга по тендерному сопровождению представляет собой комплекс мероприятий и работ, осуществляемых на всех этапах проведения аукциона или конкурса и направленных на заключение контракта по результатам закупки. При этом не важно, речь идет о процедурах выбора поставщика по 44-ФЗ, 223-ФЗ или любых других видах закупок, включая коммерческие.</p>
                                <p class="wrapper-right-item-content-text"><a href="">Читать полностью <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 135.89 240.82"><defs></defs><g id="Слой_2" data-name="Слой 2"><g id="Capa_1" data-name="Capa 1"><path id="Chevron_Right" data-name="Chevron Right" class="cls-1" d="M132.28,111.82,21.29,3.56a12.7,12.7,0,0,0-17.64,0,11.92,11.92,0,0,0,0,17.17l102.18,99.68L3.66,220.08a11.94,11.94,0,0,0,0,17.19,12.73,12.73,0,0,0,17.65,0L132.29,129A12.05,12.05,0,0,0,132.28,111.82Z"/></g></g></svg></a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
<?php get_footer(); ?>