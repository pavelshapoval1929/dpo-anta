<?php
/**
 * @package WordPress
 * @subpackage Multi Anta
 * @since Multi Anta 1.0
 */

include 'regions.php';
?>
<?php $all_options = get_option('true_options'); ?>
<?php if($_SERVER["HTTP_HOST"] != "dpo-anta.ru") : ?>
<section style="padding: 20px 0 20px 0">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
				<?php
				echo 'Обучение в учебном центре ДПО АНТА '.$regions[$_SERVER['SERVER_NAME']]['title'].' - быстро, качественно профессионально.'//вывести в админку;
				?>
            </div>
        </div>
    </div>
</section>
<?php endif;?>
<footer>
        <div class="layer">
            <div class="form-call-wrapper">
            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svgjs="http://svgjs.com/svgjs" version="1.1" width="25" height="25" x="0" y="0" viewBox="0 0 512.001 512.001" style="enable-background:new 0 0 512 512" xml:space="preserve" class=""><g>
                    <g xmlns="http://www.w3.org/2000/svg">
                        <g>
                            <path d="M284.286,256.002L506.143,34.144c7.811-7.811,7.811-20.475,0-28.285c-7.811-7.81-20.475-7.811-28.285,0L256,227.717    L34.143,5.859c-7.811-7.811-20.475-7.811-28.285,0c-7.81,7.811-7.811,20.475,0,28.285l221.857,221.857L5.858,477.859    c-7.811,7.811-7.811,20.475,0,28.285c3.905,3.905,9.024,5.857,14.143,5.857c5.119,0,10.237-1.952,14.143-5.857L256,284.287    l221.857,221.857c3.905,3.905,9.024,5.857,14.143,5.857s10.237-1.952,14.143-5.857c7.811-7.811,7.811-20.475,0-28.285    L284.286,256.002z" fill="#e4e4e4" data-original="#000000" style="" class=""></path>
                        </g>
                    </g>
                </g>
            </svg>
	        <?php
	            echo do_shortcode('[contact-form-7 id="75" title="Контактная форма 1"]');
	        ?>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-6">
                    <div class="first-content footer-logo">
                        <!--<img src="<? echo get_template_directory_uri()?>/img/logo.png" alt="" class="img-fluid">-->
                        <div class="" style="display: flex; align-items: center; padding: 0; min-width: 275px; position: relative; left: -35px;">
                            <?php echo nanima_logo();?>
                            <div class="logotext-wrap">
                                <a href="<?php echo site_url();?>" style="color: unset">
                                <span class="name">Учебный центр АНТА</span>
                                <span class="after-name">Центр дополнительного профессионального образования</span>
                                </a>
                            </div>
                        </div>
                        <p class="text1">
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24">
                                <path fill="none" d="M0 0h24v24H0z" />
                                <path d="M12 22C6.477 22 2 17.523 2 12S6.477 2 12 2s10 4.477 10 10-4.477 10-10 10zm0-2a8 8 0 1 0 0-16 8 8 0 0 0 0 16zm1-8h4v2h-6V7h2v5z" /> </svg><?php echo $all_options["my_days"];?></p>
                        <p class="text1"><?php echo $all_options["my_vacations"];?></p>
                        <p class="text1" style="text-align: left"><img src="/wp-content/themes/multianta/img/days.png" alt="" style="margin: 0;"></p>
                        <p class="text44">Мы принимаем:</p> <img src="<? echo get_template_directory_uri()?>/img/pay-methods.png" alt="" class="img-fluid"> </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="second-content">
                        <p class="text2">Курсы</p>
	                    <?php
                            wp_nav_menu( [
                                'container'      => '',
                                'menu_class'     => 'footer-list',
                                'menu' => 'footer_menu_1',
                                'theme_location' => 'footer_menu_1'
                            ] );
	                    ?>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="three-content">
                        <p class="text2">О компании</p>
                        <?php
                        wp_nav_menu( [
	                        'container'      => '',
	                        'menu_class'     => 'footer-list',
	                        'menu' => 'footer_menu_2',
	                        'theme_location' => 'footer_menu_2'
                        ] );
                        ?>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="four-content">
                        <p class="text4">
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24">
                                <path fill="none" d="M0 0h24v24H0z" />
                                <path d="M9.366 10.682a10.556 10.556 0 0 0 3.952 3.952l.884-1.238a1 1 0 0 1 1.294-.296 11.422 11.422 0 0 0 4.583 1.364 1 1 0 0 1 .921.997v4.462a1 1 0 0 1-.898.995c-.53.055-1.064.082-1.602.082C9.94 21 3 14.06 3 5.5c0-.538.027-1.072.082-1.602A1 1 0 0 1 4.077 3h4.462a1 1 0 0 1 .997.921A11.422 11.422 0 0 0 10.9 8.504a1 1 0 0 1-.296 1.294l-1.238.884zm-2.522-.657l1.9-1.357A13.41 13.41 0 0 1 7.647 5H5.01c-.006.166-.009.333-.009.5C5 12.956 11.044 19 18.5 19c.167 0 .334-.003.5-.01v-2.637a13.41 13.41 0 0 1-3.668-1.097l-1.357 1.9a12.442 12.442 0 0 1-1.588-.75l-.058-.033a12.556 12.556 0 0 1-4.702-4.702l-.033-.058a12.442 12.442 0 0 1-.75-1.588z" /> </svg>
	                        <?php
                                $domains_phones = get_field('domains_phones', 43);
                                foreach ($domains_phones as $domain){
                                    if($domain['domain'] == preg_replace("/https:\/\//", "", site_url())){
                                        $phone_of_domain = $domain['phone'];
                                    }
                                }
	                        ?>
	                        <?php if($phone_of_domain) : ?>
                                <a href="tel:<?php echo $phone_of_domain; ?>">
			                        <?php echo $phone_of_domain; ?>
                                </a>
	                        <?php else:?>
                            <a href="tel:<? echo $all_options['my_telephone']; ?>">
                                <? echo $all_options['my_telephone']; ?>
                            </a>
	                        <?php endif;?>
                        </p>
                        <p class="text5">Бесплатный звонок по России</p>
						<script data-b24-form="click/71/4qg0cm" data-skip-moving="true">
						(function(w,d,u){
						var s=d.createElement('script');s.async=true;s.src=u+'?'+(Date.now()/180000|0);
						var h=d.getElementsByTagName('script')[0];h.parentNode.insertBefore(s,h);
						})(window,document,'https://cdn-ru.bitrix24.ru/b6368597/crm/form/loader_71.js');
						</script>
                        <button class="call">Заказать звонок</button>


	                    <?php
	                    $domains_addresses = get_field('domains_addresses', 43);
	                    foreach ($domains_addresses as $domain_a){
		                    if($domain_a['domain'] == preg_replace("/https:\/\//", "", site_url())){
			                    $address_of_domain = $domain_a['address'];
		                    }
	                    }
	                    ?>
	                    <?php if($address_of_domain):?>
                        <p class="text6">
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24">
                                <path fill="none" d="M0 0h24v24H0z" />
                                <path d="M12 20.9l4.95-4.95a7 7 0 1 0-9.9 0L12 20.9zm0 2.828l-6.364-6.364a9 9 0 1 1 12.728 0L12 23.728zM12 13a2 2 0 1 0 0-4 2 2 0 0 0 0 4zm0 2a4 4 0 1 1 0-8 4 4 0 0 1 0 8z" />
                            </svg>
		                    <?php echo $address_of_domain; ?>
		                    <?php else:?>
                        <p class="text6">
		                    <?php if (site_url() == 'https://dpo-anta.ru') : ?>
                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24">
                                    <path fill="none" d="M0 0h24v24H0z" />
                                    <path d="M12 20.9l4.95-4.95a7 7 0 1 0-9.9 0L12 20.9zm0 2.828l-6.364-6.364a9 9 0 1 1 12.728 0L12 23.728zM12 13a2 2 0 1 0 0-4 2 2 0 0 0 0 4zm0 2a4 4 0 1 1 0-8 4 4 0 0 1 0 8z" />
                                </svg>
                                <?php echo $all_options['my_address']; ?>
                            <?php endif;?>
		                    <?php endif;?>
                        </p>

                        <p class="text6">
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24">
                                <path fill="none" d="M0 0h24v24H0z" />
                                <path d="M3 3h18a1 1 0 0 1 1 1v16a1 1 0 0 1-1 1H3a1 1 0 0 1-1-1V4a1 1 0 0 1 1-1zm17 4.238l-7.928 7.1L4 7.216V19h16V7.238zM4.511 5l7.55 6.662L19.502 5H4.511z" /> </svg><a class="mailto2" href="mailto: <? echo $all_options['my_email']; ?>"><? echo $all_options['my_email']; ?></a></p>
                        <div class="social-content">
                            <div>
                                <p>Мы в соц сетях:</p>
                            </div>
                            <div style="display: none;">
                                <a href="<?php echo $all_options["my_inst"];?>">
                                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24">
                                        <path fill="none" d="M0 0h24v24H0z" />
                                        <path d="M12 9a3 3 0 1 0 0 6 3 3 0 0 0 0-6zm0-2a5 5 0 1 1 0 10 5 5 0 0 1 0-10zm6.5-.25a1.25 1.25 0 0 1-2.5 0 1.25 1.25 0 0 1 2.5 0zM12 4c-2.474 0-2.878.007-4.029.058-.784.037-1.31.142-1.798.332-.434.168-.747.369-1.08.703a2.89 2.89 0 0 0-.704 1.08c-.19.49-.295 1.015-.331 1.798C4.006 9.075 4 9.461 4 12c0 2.474.007 2.878.058 4.029.037.783.142 1.31.331 1.797.17.435.37.748.702 1.08.337.336.65.537 1.08.703.494.191 1.02.297 1.8.333C9.075 19.994 9.461 20 12 20c2.474 0 2.878-.007 4.029-.058.782-.037 1.309-.142 1.797-.331.433-.169.748-.37 1.08-.702.337-.337.538-.65.704-1.08.19-.493.296-1.02.332-1.8.052-1.104.058-1.49.058-4.029 0-2.474-.007-2.878-.058-4.029-.037-.782-.142-1.31-.332-1.798a2.911 2.911 0 0 0-.703-1.08 2.884 2.884 0 0 0-1.08-.704c-.49-.19-1.016-.295-1.798-.331C14.925 4.006 14.539 4 12 4zm0-2c2.717 0 3.056.01 4.122.06 1.065.05 1.79.217 2.428.465.66.254 1.216.598 1.772 1.153a4.908 4.908 0 0 1 1.153 1.772c.247.637.415 1.363.465 2.428.047 1.066.06 1.405.06 4.122 0 2.717-.01 3.056-.06 4.122-.05 1.065-.218 1.79-.465 2.428a4.883 4.883 0 0 1-1.153 1.772 4.915 4.915 0 0 1-1.772 1.153c-.637.247-1.363.415-2.428.465-1.066.047-1.405.06-4.122.06-2.717 0-3.056-.01-4.122-.06-1.065-.05-1.79-.218-2.428-.465a4.89 4.89 0 0 1-1.772-1.153 4.904 4.904 0 0 1-1.153-1.772c-.248-.637-.415-1.363-.465-2.428C2.013 15.056 2 14.717 2 12c0-2.717.01-3.056.06-4.122.05-1.066.217-1.79.465-2.428a4.88 4.88 0 0 1 1.153-1.772A4.897 4.897 0 0 1 5.45 2.525c.638-.248 1.362-.415 2.428-.465C8.944 2.013 9.283 2 12 2z" /> </svg>
                                </a>
                            </div>
                            <div>
                                <a href="<?php echo $all_options["my_wa"];?>">
                                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24">
                                        <path fill="none" d="M0 0h24v24H0z" />
                                        <path d="M7.253 18.494l.724.423A7.953 7.953 0 0 0 12 20a8 8 0 1 0-8-8c0 1.436.377 2.813 1.084 4.024l.422.724-.653 2.401 2.4-.655zM2.004 22l1.352-4.968A9.954 9.954 0 0 1 2 12C2 6.477 6.477 2 12 2s10 4.477 10 10-4.477 10-10 10a9.954 9.954 0 0 1-5.03-1.355L2.004 22zM8.391 7.308c.134-.01.269-.01.403-.004.054.004.108.01.162.016.159.018.334.115.393.249.298.676.588 1.357.868 2.04.062.152.025.347-.093.537a4.38 4.38 0 0 1-.263.372c-.113.145-.356.411-.356.411s-.099.118-.061.265c.014.056.06.137.102.205l.059.095c.256.427.6.86 1.02 1.268.12.116.237.235.363.346.468.413.998.75 1.57 1l.005.002c.085.037.128.057.252.11.062.026.126.049.191.066a.35.35 0 0 0 .367-.13c.724-.877.79-.934.796-.934v.002a.482.482 0 0 1 .378-.127c.06.004.121.015.177.04.531.243 1.4.622 1.4.622l.582.261c.098.047.187.158.19.265.004.067.01.175-.013.373-.032.259-.11.57-.188.733a1.155 1.155 0 0 1-.21.302 2.378 2.378 0 0 1-.33.288 3.71 3.71 0 0 1-.125.09 5.024 5.024 0 0 1-.383.22 1.99 1.99 0 0 1-.833.23c-.185.01-.37.024-.556.014-.008 0-.568-.087-.568-.087a9.448 9.448 0 0 1-3.84-2.046c-.226-.199-.435-.413-.649-.626-.89-.885-1.562-1.84-1.97-2.742A3.47 3.47 0 0 1 6.9 9.62a2.729 2.729 0 0 1 .564-1.68c.073-.094.142-.192.261-.305.127-.12.207-.184.294-.228a.961.961 0 0 1 .371-.1z" /> </svg>
                                </a>
                            </div>
                            <div style="display: none;">
                                <a href="<?php echo $all_options["my_telegram"];?>">
                                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24">
                                        <path fill="none" d="M0 0h24v24H0z" />
                                        <path d="M12 20a8 8 0 1 0 0-16 8 8 0 0 0 0 16zm0 2C6.477 22 2 17.523 2 12S6.477 2 12 2s10 4.477 10 10-4.477 10-10 10zm-3.11-8.83l-2.498-.779c-.54-.165-.543-.537.121-.804l9.733-3.76c.565-.23.885.061.702.79l-1.657 7.82c-.116.557-.451.69-.916.433l-2.551-1.888-1.189 1.148c-.122.118-.221.219-.409.244-.187.026-.341-.03-.454-.34l-.87-2.871-.012.008z" /> </svg>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <div class="footer-bottom">
        <script type="text/javascript">
            privacy({company: 'ДПО АНТА', date: '<?= date('d/m/Y')?>'});
        </script>
        <div class="container">
            <div class="row">
                <div class="col-lg-4">
                    <p><a href="javascript:void(0);" class="privacy">Политика конфиденциальности</a></p>
                </div>
                <div class="col-lg-4">
                    <p>© ЦДПО <?php echo date('Y');?>. Все права защищены</p>
                </div>
                <div class="col-lg-4">
                    <p> <img src="<? echo get_template_directory_uri()?>/img/sunglasses.svg" alt=""><a href="#" class="bvi-open">Версия для слабовидящих</a></p>
                </div>
            </div>
        </div>
    </div>
<?php wp_footer(); ?>
<!-- Yandex.Metrika counter -->
<script type="text/javascript" >
   (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
   m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
   (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

   ym(86621768, "init", {
        clickmap:true,
        trackLinks:true,
        accurateTrackBounce:true,
        webvisor:true
   });
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/86621768" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
<script>
        (function(w,d,u){
                var s=d.createElement('script');s.async=true;s.src=u+'?'+(Date.now()/60000|0);
                var h=d.getElementsByTagName('script')[0];h.parentNode.insertBefore(s,h);
        })(window,document,'https://cdn-ru.bitrix24.ru/b6368597/crm/site_button/loader_27_v371bt.js');
</script>
</body>
</html>