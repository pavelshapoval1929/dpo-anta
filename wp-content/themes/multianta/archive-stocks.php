<?php
/* Template Name: Archive Page stocks */
get_header();
?>
<div class="container">
	<div class="row">
		<div class="col-lg-12">
			<div class="share-breadcrumbs">
			<?php
				if( function_exists('kama_breadcrumbs') ) kama_breadcrumbs();
			?>
			</div>
		</div>
	</div>
</div>
<div class="stocks-content-page">
	<section class="stocks-content-sc">
		<div class="container">
            <div class="page-title"><h1>Акции</h1></div>
			<?php
				$args = array(
					'post_type' => 'stocks',
					'posts_per_page' => -1,
					'orderby' => 'DESC',
				);
				$query = new WP_Query( $args );

			?>
			<?php if (!empty($query->posts)) :?>
			    <?php foreach ($query->posts as $post) :?>
				<?php
					$attr = array(
						'class' => "share-img",
					);
					?>
					<div class="row share-row">
						<div class="col-md-3 col-sm-12">
							<?php the_post_thumbnail('full', $attr); ?>
						</div>
						<div class="col-md-9 col-sm-12">
							<div class="share-text">
								<h2 class="share-title">
									<?php
										echo $post->post_title;
									?>
								</h2>
								<?php if($post->post_excerpt != "") : ?>
									<div class="less">
										<?php
											echo $post->post_excerpt;
										?>
										<span class='readmore_add'>..читать все</span>
									</div>
									<div class="full not-show">
										<?php
											echo $post->post_content;
										?>
										<span class='readless_add'>свернуть</span>
									</div>
								<?php else :?>
								<div class="full">
									<?php
										echo $post->post_content;
									?>
								</div>
								<?php endif;?>

							</div>
							<div class="share_btn">
								<button>Заказать звонок</button>
							</div>
						</div>
					</div>
				<?php endforeach;?>
			<?php endif;?>
		</div>
	</section>
</div>

<section class="sc10">
    <div class="container">
        <div class="wrapper">
            <div class="row">
                <div class="col-lg-6">
                    <div class="wrapper-left">
						<?php get_search_form(); ?>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="wrapper-right">
                        <p>Все курсы по алфавиту</p>
                        <div class="alphabite-content">
							<?php
							$posts = get_posts('post_type=kursi&orderby=title&numberposts=-1&order=ASC');

							foreach( $posts as $k => $post ){

								// первая буква
								$fl = get_first_letter( $post->post_title );
								$prev_fl = isset( $posts[ ($k-1) ] ) ? get_first_letter( $posts[ ($k-1) ]->post_title ) : '';
								if( $prev_fl !== $fl ){ ?>
                                    <div>
                                        <p><a href="/?s=<? echo $fl;?>"><? echo $fl;?></a></p>
                                    </div>
								<?}
							}
							wp_reset_postdata();
							?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php get_footer(); ?>
