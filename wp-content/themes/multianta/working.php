<?php
/*
Template Name: Шаблон: Рабочие профессии
*/
?>
<?php get_header(); ?>
<?php  $arMeta = get_post_custom(); ?>
<?
$all_options = get_option('true_options');
$all_gorod = do_shortcode('[multiplication var="'.$all_options['my_gorod'].'"]');
include 'regions.php';
?>

<div class="main-content">
        <div class="sc1">
            <div class="header-content_hero-slider-single">
                <div class="header-content_hero-slider1" style="background: url(<?php echo get_the_post_thumbnail_url() ?>) no-repeat top;">
                    <div class="header-content_hero">
                        <div class="container">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="header-content_hero-slider-item">
                                        <!--<h1 class="header-content_hero-title"><?/*= $arMeta["hero_title"][0];*/?></h1>-->
                                        <?php $toponim = $regions[$_SERVER['SERVER_NAME']]['title'];?>
                                        <h1 class="header-content_hero-title"><?php the_title(); echo $toponim;?></h1>
                                        <p class="header-content_hero-desc"><?= $arMeta["hero_desc"][0];?></p>
                                        <button class="header-content_hero-button">Подробнее</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</div>
<div class="main-page-content">
    <section class="sc11">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <?php 
                        if( function_exists('kama_breadcrumbs') ) kama_breadcrumbs();
                    ?>
                </div>
            </div>
        </div>
    </section>
    <section class="sc12">
        <div class="container">
            <!--<h2 class="sc12-title">
                Учебный центр квалификации работников “ДПО АНТА” -
                <span>дополнительное профессиональное образование, переподготовка</span>
            </h2>-->
            <h2 class="sc12-title">
                <?php echo get_field('under_capt');?>
            </h2>
            <div class="row">
                <?php
                    //the_title();
                    the_content();
                ?>

                <?php /*while( have_posts() ) : the_post();
                        //$post = get_post();
                        $more = 1; // отображаем полностью всё содержимое поста
                        //the_content(); // выводим контент
	                wp_reset_postdata();
                endwhile; */?>
                <?php //the_content();?>


            </div>
        </div>
    </section>
    <section class="sc3">
        <div class="container">
            <h2 class="sc3-title">Что мы гарантируем</h2>
            <div class="row">
                <div class="col-lg-4 col-6">
                    <div class="wrapper"> <img src="<? echo get_template_directory_uri()?>/img/sc3-wrapper-ico1.svg" alt="" class="img-fluid">
                        <p class="sc3-wrapper__title">Быстро</p>
                        <p class="sc3-wrapper__desc">От 2 до 4 дней Доставка курьером 0₽</p>
                    </div>
                </div>
                <div class="col-lg-4 col-6">
                    <div class="wrapper"> <img src="<? echo get_template_directory_uri()?>/img/sc3-wrapper-ico2.svg" alt="" class="img-fluid">
                        <p class="sc3-wrapper__title">Все удостоверения в одном месте</p>
                        <p class="sc3-wrapper__desc">Рабочие специальности, охрана труда, ПТМ, ГО и ЧС</p>
                    </div>
                </div>
                <div class="col-lg-4 col-6">
                    <div class="wrapper"> <img src="<? echo get_template_directory_uri()?>/img/sc3-wrapper-ico3.svg" alt="" class="img-fluid">
                        <p class="sc3-wrapper__title">Доступные цены </p>
                        <p class="sc3-wrapper__desc">Лучшие цены в городе</p>
                    </div>
                </div>
                <div class="col-lg-4 col-6">
                    <div class="wrapper"> <img src="<? echo get_template_directory_uri()?>/img/sc3-wrapper-ico4.svg" alt="" class="img-fluid">
                        <p class="sc3-wrapper__title">Дистанционное обучение</p>
                        <p class="sc3-wrapper__desc"> - без отрыва от работы и производства</p>
                    </div>
                </div>
                <div class="col-lg-4 col-6">
                    <div class="wrapper"> <img src="<? echo get_template_directory_uri()?>/img/sc3-wrapper-ico5.svg" alt="" class="img-fluid">
                        <p class="sc3-wrapper__title">Все законно </p>
                        <p class="sc3-wrapper__desc">Учебный центр имеет все лицензии и аккредитации - работаем по договору</p>
                    </div>
                </div>
                <div class="col-lg-4 col-6">
                    <div class="wrapper"> <img src="<? echo get_template_directory_uri()?>/img/sc3-wrapper-ico6.svg" alt="" class="img-fluid">
                        <p class="sc3-wrapper__title">Все сведения заносим в реестр</p>
                        <p class="sc3-wrapper__desc">Выдаем документы гос образца</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="sc15">
        <div class="container">
            <h2 class="sc15-title">Какие документы получают после обучения</h2>
            <div class="row">
                <div class="col-lg-4">
                    <a href="">
                        <div class="wrapper wrapper1">
                            <div class="overlay-content">
                                <div>
                                    <p>Свидетельство о профессии рабочего, должности служащего </p>
                                </div>
                                <div> <img src="<? echo get_template_directory_uri()?>/img/zoom-ico-white.svg" alt="" class="img-fluid"></div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-lg-4">
                    <a href="">
                        <div class="wrapper wrapper2">
                            <div class="overlay-content">
                                <div>
                                    <p>Свидетельство о профессии рабочего, должности служащего </p>
                                </div>
                                <div> <img src="<? echo get_template_directory_uri()?>/img/zoom-ico-white.svg" alt="" class="img-fluid"></div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-lg-4">
                    <a href="">
                        <div class="wrapper wrapper3">
                            <div class="overlay-content">
                                <div>
                                    <p>Свидетельство о профессии рабочего, должности служащего </p>
                                </div>
                                <div> <img src="<? echo get_template_directory_uri()?>/img/zoom-ico-white.svg" alt="" class="img-fluid"></div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-lg-4">
                    <a href="">
                        <div class="wrapper wrapper4">
                            <div class="overlay-content">
                                <div>
                                    <p>Свидетельство о профессии рабочего, должности служащего </p>
                                </div>
                                <div> <img src="<? echo get_template_directory_uri()?>/img/zoom-ico-white.svg" alt="" class="img-fluid"></div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-lg-4">
                    <a href="">
                        <div class="wrapper wrapper5">
                            <div class="overlay-content">
                                <div>
                                    <p>Свидетельство о профессии рабочего, должности служащего </p>
                                </div>
                                <div> <img src="<? echo get_template_directory_uri()?>/img/zoom-ico-white.svg" alt="" class="img-fluid"></div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-lg-4">
                    <a href="">
                        <div class="wrapper wrapper6">
                            <div class="overlay-content">
                                <div>
                                    <p>Свидетельство о профессии рабочего, должности служащего </p>
                                </div>
                                <div> <img src="<? echo get_template_directory_uri()?>/img/zoom-ico-white.svg" alt="" class="img-fluid"></div>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </section>
    <? if ($arMeta['break_1']): ?>
        <section class="sc16">
            <div class="container">
                <h2 class="sc16-title">Список специальностей</h2>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="wrapper">
                            <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                                <?php for ($i=1; $i <= 8; $i++) { 
                                    $activ = ($i == 1) ? 'active' : '';
                                    if (array_key_exists('break_'.$i, $arMeta)) { ?>
                                    <li class="nav-item"> 
                                        <a class="nav-link <?= $activ;?>" id="pills-tab<?= $i;?>-tab" data-toggle="pill" href="#pills-tab<?= $i;?>" role="tab" aria-controls="pills-tab<?= $i;?>" aria-selected="true">
                                            Разряд <?= $i; ?>
                                        </a> 
                                    </li>
                                <? } ?>
                                <? } ?>
                            </ul>
                            <div class="tab-content" id="pills-tabContent">
                            <?
                               for ($a=1; $a <= 8; $a++) {
                                    $activs = ($a == 1) ? 'active' : '';
                                    if (array_key_exists('break_'.$a, $arMeta)) { ?>
                                    <div class="tab-pane fade show <?= $activs;?>" id="pills-tab<?= $a;?>" role="tabpanel" aria-labelledby="pills-tab<?= $a;?>-tab">
                                        <table>                                   
                                                <tbody>
                                                    <tr>
                                                        <td> <?= $arMeta['break_'.$a][0]; ?></td>
                                                    </tr>                    
                                                </tbody>
                                        </table>
                                    </div>
                                <? } ?>
                            <? } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    <? endif; ?>
    <section class="sc17">
        <div class="container">
            <h2 class="sc17-title">Этапы работы</h2>
            <div class="row">
                <div class="col">
                    <div class="wrapper"> <img src="<? echo get_template_directory_uri()?>/img/sc17-ico1.png" alt="" class="img-fluid">
                        <p class="wrapper-title">Заявка</p>
                        <p class="wrapper-desc">Оставьте заявку на сайте или позвоните по телефону</p>
                    </div>
                </div>
                <div class="col">
                    <div class="wrapper"> <img src="<? echo get_template_directory_uri()?>/img/sc17-ico2.png" alt="" class="img-fluid">
                        <p class="wrapper-title">Консультация</p>
                        <p class="wrapper-desc">Консультация со специалистом. Обсуждаем индивидульные условия работы, строки и стоимость</p>
                    </div>
                </div>
                <div class="col">
                    <div class="wrapper"> <img src="<? echo get_template_directory_uri()?>/img/sc17-ico3.png" alt="" class="img-fluid">
                        <p class="wrapper-title">Договор</p>
                        <p class="wrapper-desc">Заключаем договор. Для этого вам понадобится: паспорт, фотография 3*4, документ об образовании, трудовая книжка </p>
                    </div>
                </div>
                <div class="col">
                    <div class="wrapper"> <img src="<? echo get_template_directory_uri()?>/img/sc17-ico4.png" alt="" class="img-fluid">
                        <p class="wrapper-title">Материалы</p>
                        <p class="wrapper-desc">Заключаем договор. Для Мы высылаем вам методиеские материалы на почту или флешку</p>
                    </div>
                </div>
                <div class="col">
                    <div class="wrapper"> <img src="<? echo get_template_directory_uri()?>/img/sc17-ico5.png" alt="" class="img-fluid">
                        <p class="wrapper-title">Доставка</p>
                        <p class="wrapper-desc">В течении 2-3 рабочих дней вы получите документы в офисеили доставкой курьером</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="sc5">
        <div class="container">
            <h2 class="sc5-title">Наши лицензии</h2>
            <div class="row">
                <div class="col-lg-4 col-6">
                    <a data-fancybox="images2" href="<? echo get_template_directory_uri()?>/img/sertificate-img1.jpg"><img src="<? echo get_template_directory_uri()?>/img/sertificate-img1.jpg" class="img-fluid"></a>
                    <div class="sc5-bottom-content">
                        <div>
                            <p>На право оказывать образовательные услуги</p>
                        </div>
                        <div><img src="<? echo get_template_directory_uri()?>/img/zoom-ico.svg" alt="" class="img-fluid"></div>
                    </div>
                </div>
                <div class="col-lg-4 col-6">
                    <a data-fancybox="images2" href="<? echo get_template_directory_uri()?>/img/sertificate-img2.jpg"><img src="<? echo get_template_directory_uri()?>/img/sertificate-img2.jpg" class="img-fluid"></a>
                    <div class="sc5-bottom-content">
                        <div>
                            <p>На право оказывать образовательные услуги</p>
                        </div>
                        <div><img src="<? echo get_template_directory_uri()?>/img/zoom-ico.svg" alt="" class="img-fluid"></div>
                    </div>
                </div>
                <div class="col-lg-4 col-6">
                    <a data-fancybox="images2" href="<? echo get_template_directory_uri()?>/img/sertificate-img3.jpg"><img src="<? echo get_template_directory_uri()?>/img/sertificate-img3.jpg" class="img-fluid"></a>
                    <div class="sc5-bottom-content">
                        <div>
                            <p>На право оказывать образовательные услуги</p>
                        </div>
                        <div><img src="<? echo get_template_directory_uri()?>/img/zoom-ico.svg" alt="" class="img-fluid"></div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="sc18">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <div class="row">
                        <div class="col-6">
                            <div class="wrapper">
                                <p class="wrapper-title">6 лет </p>
                                <p class="wrapper-desc">опыт работы на рынке</p>
                            </div>
                            <div class="wrapper">
                                <p class="wrapper-title">10 000+</p>
                                <p class="wrapper-desc">обученных спецалистов по всей РФ с 2014 года</p>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="wrapper">
                                <p class="wrapper-title">500+ </p>
                                <p class="wrapper-desc">опыт работы на рынке</p>
                            </div>
                            <div class="wrapper">
                                <p class="wrapper-title">20</p>
                                <p class="wrapper-desc">филиалов по всей россии</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6"> <img src="<? echo get_template_directory_uri()?>/img/sc3-img1.png" alt="" class="img-fluid"> </div>
            </div>
        </div>
    </section>
    <section class="sc4">
        <div class="container">
            <h2 class="sc4-title">Отзывы наших учеников</h2>
            <div class="row">
                <div class="col-lg-12">
                    <div class="sc4-review-slider">
                        <div class="sc4-review-slider__item">
                            <a data-fancybox="images1" href="<? echo get_template_directory_uri()?>/img/review-content-img1.jpg"><img src="<? echo get_template_directory_uri()?>/img/review-content-img1.jpg" class="img-fluid"></a>
                        </div>
                        <div class="sc4-review-slider__item">
                            <a data-fancybox="images1" href="<? echo get_template_directory_uri()?>/img/review-content-img1.jpg"><img src="<? echo get_template_directory_uri()?>/img/review-content-img1.jpg" class="img-fluid"></a>
                        </div>
                        <div class="sc4-review-slider__item">
                            <a data-fancybox="images1" href="<? echo get_template_directory_uri()?>/img/review-content-img1.jpg"><img src="<? echo get_template_directory_uri()?>/img/review-content-img1.jpg" class="img-fluid"></a>
                        </div>
                        <div class="sc4-review-slider__item">
                            <a data-fancybox="images1" href="<? echo get_template_directory_uri()?>/img/review-content-img1.jpg"><img src="<? echo get_template_directory_uri()?>/img/review-content-img1.jpg" class="img-fluid"></a>
                        </div>
                        <div class="sc4-review-slider__item">
                            <a data-fancybox="images1" href="<? echo get_template_directory_uri()?>/img/review-content-img1.jpg"><img src="<? echo get_template_directory_uri()?>/img/review-content-img1.jpg" class="img-fluid"></a>
                        </div>
                        <div class="sc4-review-slider__item">
                            <a data-fancybox="images1" href="<? echo get_template_directory_uri()?>/img/review-content-img1.jpg"><img src="<? echo get_template_directory_uri()?>/img/review-content-img1.jpg" class="img-fluid"></a>
                        </div>
                    </div>
                    <div class="sc4-review-slider-arrow-content">
                        <div class="sc4-review-slider__prev"><img src="<? echo get_template_directory_uri()?>/img/sc4-review-slider__prev.svg" alt="" class="img-fluid"></div>
                        <div class="sc4-review-slider__next"><img src="<? echo get_template_directory_uri()?>/img/sc4-review-slider__next.svg" alt="" class="img-fluid"></div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="sc6">
        <div class="container">
            <h2 class="sc6-title">Наши клиенты</h2>
            <div class="row">
                <div class="col-lg-12">
                    <div class="our-client-slider">
                        <div class="our-client-slider__item"> <img src="<? echo get_template_directory_uri()?>/img/our-client-img1.png" alt="" class="img-fluid"> </div>
                        <div class="our-client-slider__item"> <img src="<? echo get_template_directory_uri()?>/img/our-client-img2.png" alt="" class="img-fluid"> </div>
                        <div class="our-client-slider__item"> <img src="<? echo get_template_directory_uri()?>/img/our-client-img3.png" alt="" class="img-fluid"> </div>
                        <div class="our-client-slider__item"> <img src="<? echo get_template_directory_uri()?>/img/our-client-img4.png" alt="" class="img-fluid"> </div>
                        <div class="our-client-slider__item"> <img src="<? echo get_template_directory_uri()?>/img/our-client-img1.png" alt="" class="img-fluid"> </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <?php /*<section class="sc8">
        <div class="container">
            <div class="row">
            <div class="col-lg-6 order-lg-1 order-12">
                <? $all_flatness = do_shortcode('[multiplication_flatness var="'.$all_options['my_flatness'].'"]');?>
                <h2 class="sc8-title">Блок с seo текстом и видео</h2>
                <p>Для совершенствования знаний, продвижения по карьерной лестнице или смены сферы деятельности в целом многим специалистам требуется прохождение курсов дополнительного профессионального образования. Учебный центр ДПО «АНТА» предоставляет услуги по подготовке, переподготовке, аттестации, повышению квалификации работников и руководителей компаний или физических лиц. Территориально мы расположены в <?= $all_flatness ?>, но благодаря эффективным методикам дистанционного образования учиться у нас можно, находясь в любом городе РФ.</p><p><a class="text-tab" href="#">Читать далее</a></p>
            </div>
            <div class="col-lg-6 order-lg-12 order-1">
                <div class="video-container">
                    <div class="youtube" id="XQYpFkU31qE"></div>
                </div>
            </div>
            <div class="col-lg-12 order-1 order-12 text-order" style="display: none;">
                <p>Учебный центр в <?= $all_gorod; ?></p>
                    <p>Предлагаем большой список направлений и тем, которые строго отобраны под те или иные задачи. Наш учебный центр повышения квалификации подойдет для:</p>
                    <p>&bull; руководителей компаний;</p>
                    <p>&bull; специалистов разных сфер;</p>
                    <p>&bull; рабочих;</p>
                    <p>&bull; физических лиц, планирующих устроиться на работу по новой специальности.</p>
                    <p>Как проходит обучение?</p>
                    <p>Выберете подходящий образовательный курс на сайте ДПО &laquo;АНТА&raquo;. Создайте заявку или свяжитесь с нами по указанному номеру телефона. После этого от получения диплома вас будет отделять всего 3 простых шага:</p>
                    <p>1. Оформление договора.</p>
                    <p>2. Прохождение обучения (лекции, тренинги, самостоятельные работы).</p>
                    <p>3. Сдача экзаменов.</p>
                    <p>Наш учебный центр выдает дипломы установленного образца, с занесением всех данных в росреестр, которые подходят для предоставления в любые организации, работающие на территории РФ.</p>
                    <p>Преимущества ДПО &laquo;АНТА&raquo;</p>
                    <p>Предлагаем обучение на стационаре и заочное, с применением современных методик дистанционного образования. Повысить квалификацию или получить новую профессию теперь можно без отрыва от основной работы &ndash; в любое удобное время. Учитесь вечерами, на выходных, во время командировки или даже на больничном. Понадобится лишь смартфон, ноутбук или планшет с подключением к интернету.</p>
                    <p>Преподаватели нашего учебного центра &ndash; действующие практики. Это означает, что обучение у нас происходит только по самым передовым методикам и с учетом актуальной информации. Среди других преимуществ ДПО &laquo;АНТА&raquo;:</p>
                    <p>&bull; 2500 выданных дипломов и удостоверений;</p>
                    <p>&bull; более 100 разнообразных курсов и тренингов;</p>
                    <p>&bull; эффективные авторские методики преподавания;</p>
                    <p>&bull; регулярное обновление и совершенствование образовательных программ;</p>
                    <p>&bull; консультационная поддержка выпускников;</p>
                    <p>&bull; доступные цены и постоянно действующие скидки.</p>
                    <p>Для удобства пользователей сайт учебного центра предлагает список курсов в алфавитном порядке. Просто выберите подходящий и убедитесь, что в него входят необходимые для изучения темы. Все изучаемые дисциплины подробно расписаны, уточнить любые нюансы можно, обратившись к менеджеру.
                </p>
            </div>
        </div>
        </div>
    </section> */?>
    <section class="sc9">
        <div class="container">
            <div class="wrapper">
                <div class="row">
                    <div class="col-lg-6"> <img src="<? echo get_template_directory_uri()?>/img/sc9-img1.png" alt="" class="img-fluid"> </div>
                    <div class="col-lg-6">
                        <h2>Скидки до 20% <br><span>от 5 человек</span></h2>
                        <form action="" method="post">
                            <input type="text" name="name" placeholder="Ваше имя" class="sc9-wrapper-form-input">
                            <input type="text" name="phone" placeholder="Номер телефона" class="sc9-wrapper-form-input">
                            <input type="email" name="email" placeholder="E-mail" class="sc9-wrapper-form-input">
                            <input type="submit" value="Получить скидку" class="sc9-wrapper-form-submit">
                            <p><img src="<? echo get_template_directory_uri()?>/img/verified.png" alt="" class="img-fluid">Ваши данные защищены и не будут переданы третьим лицам</p>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="sc10">
        <div class="container">
            <div class="wrapper">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="wrapper-left">
                            <?php get_search_form(); ?>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="wrapper-right">
                            <p>Все курсы по алфавиту</p>
                            <div class="alphabite-content">
                                <div>
                                    <p><a href="">A</a></p>
                                </div>
                                <div>
                                    <p><a href="">A</a></p>
                                </div>
                                <div>
                                    <p><a href="">A</a></p>
                                </div>
                                <div>
                                    <p><a href="">A</a></p>
                                </div>
                                <div>
                                    <p><a href="">A</a></p>
                                </div>
                                <div>
                                    <p><a href="">A</a></p>
                                </div>
                                <div>
                                    <p><a href="">A</a></p>
                                </div>
                                <div>
                                    <p><a href="">A</a></p>
                                </div>
                                <div>
                                    <p><a href="">A</a></p>
                                </div>
                                <div>
                                    <p><a href="">A</a></p>
                                </div>
                                <div>
                                    <p><a href="">A</a></p>
                                </div>
                                <div>
                                    <p><a href="">A</a></p>
                                </div>
                                <div>
                                    <p><a href="">A</a></p>
                                </div>
                                <div>
                                    <p><a href="">A</a></p>
                                </div>
                                <div>
                                    <p><a href="">A</a></p>
                                </div>
                                <div>
                                    <p><a href="">A</a></p>
                                </div>
                                <div>
                                    <p><a href="">A</a></p>
                                </div>
                                <div>
                                    <p><a href="">A</a></p>
                                </div>
                                <div>
                                    <p><a href="">A</a></p>
                                </div>
                                <div>
                                    <p><a href="">A</a></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<?php get_footer(); ?>