<?php get_header(); ?>
<section class="sc11">
	<div class="container">
        <div class="breadcrumbs-wrap breadcrumb-clients">
            <?php
			    if( function_exists('kama_breadcrumbs') ) kama_breadcrumbs();
			?>
        </div>
        <div class="page-title"><h1><?php the_title();?></h1></div>
		<div class="row" id="ajax-posts">
			<?php $our_clients = get_field('our_clients');?>
			<?php foreach ($our_clients as $key => $our_client) : ?>
                <?php if($key < '8') :?>
                <div class="col-md-3 col-sm-6 col-xs-12 clients clients-elem">
                    <div class="item-wrap">
                        <div class="img-wrap">
                            <img src="<?php echo $our_client["our_client_img"]["url"];?>" alt="<?php echo $our_client["our_client_img"]["alt"];?>">
                        </div>
                        <div class="name-wrap">
                            <span class=""><?php echo $our_client["our_client_name"];?></span>
                        </div>
                        <div class="prof-wrap">
                            <span class="prof-text">Специальность: </span><span class="prof-value"><?php echo $our_client["our_client_profession"];?></span>
                        </div>

                    </div>
                </div>
                <?php endif;?>
			<?php endforeach;?>
		</div>
        <div class="load_more-wrap">
            <button id="more_posts" class="load_more">Загрузить еще</button>
        </div>
	</div>
</section>

<?php get_footer();
