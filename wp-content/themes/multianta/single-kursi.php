<?php
/*
Template Name: single-kursi
Template Post Type: kursi
*/
get_header();
$slide_capt = get_field('slide_capt');
$slide_pre_capt = get_field('slide_pre_capt');
$slide_image = get_field('slide_image');
if($slide_image != NULL){
    $banner = $slide_image['url'];
} else {
	$banner = get_stylesheet_directory_uri(). '/img/header-content_hero-slider-img1.jpg';
}
include 'regions.php';


?>
    <div class="banner-wrap">
        <div class="banner" style="background-image: url('<?php echo $banner;?>'); background-size: cover;" >
            <div class="header-content_hero">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="header-content_hero-slider-item">
	                            <?php $city = $regions[$_SERVER['SERVER_NAME']]['title'];?>
                                <?php if($city) : ?>
                                <h1 class="header-content_hero-title"><?php echo $slide_capt.$city;?></h1>
                                <?php else : ?>
                                <h1 class="header-content_hero-title"><?php echo $slide_capt;?></h1>
                                <?php endif;?>
                                <div class="header-content_hero-desc"><?php echo $slide_pre_capt;?></div>
                                <?php if(strpos($_SERVER['REQUEST_URI'], 'kursy-povysheniya-kvalifikacii') != false) : ?>
                                    <span class="header-content_hero-button call-form" style="cursor: pointer;">Получить удостоверение</span>
                                <?php else : ?>
                                    <span class="header-content_hero-button call-form" style="cursor: pointer;">Пройти обучение</span>
                                <?php endif;?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
	<div class="container">
        <div class="row">
            <div class="col-md-12 breadcrumbs-wrap">

                <?php
                    if( function_exists('kama_breadcrumbs') ) kama_breadcrumbs();


                    $kurs_price = get_field('kurs_price');
                    $razryad = get_field('razryad');
                    $study_period = get_field('study_period');

                ?>




            </div>
        </div>
    </div>
    <section class="sc2">
        <div class="container">
            <?php if(strpos($_SERVER['REQUEST_URI'], 'kursy-povysheniya-kvalifikacii')) : ?>
                <h2 class="sc2-title title-1">Учебный центр повышения квалификации “ДПО АНТА” - <span>дополнительное профессиональное образование, переподготовка</span></h2>
            <?php else : ?>
                <h2 class="sc2-title title-2">Учебный центр повышения квалификации “ДПО АНТА” - <span>дополнительное профессиональное образование, переподготовка</span></h2>
	        <?php endif;?>
            <div class="" id="content">
                <?php if(get_field('content_caption')):?>
                    <h2 style="text-align: center;" class="caption_add_h">
                        <span class="caption_add">
                            <?php echo get_field('content_caption');?>
                        </span>
                    </h2>
                <?php endif;?>
		        <?php
                    $content = get_the_content();
		            echo wpautop(parse_all_content($content));
		        ?>
            </div>
            <div class="fields">
                <?php if(get_field('program_view')) : ?>
                    <p class="field-row"><span class="field-label"><span class="star">&bull;</span>Вид образовательной программы: </span><?php echo get_field('program_view');?></p>
                <?php endif;?>
                <?php if(get_field('study_form')) : ?>
                    <p class="field-row"><span class="field-label"><span class="star">&bull;</span>Форма обучения: </span><?php echo get_field('study_form');?></p>
                <?php endif;?>
                <?php if(get_field('your_docs')) : ?>
                    <p class="field-row"><span class="field-label"><span class="star">&bull;</span>Вы получите: </span><?php echo get_field('your_docs');?></p>
                <?php endif;?>
                <?php if(get_field('requirements')) : ?>
                    <p class="field-row"><span class="field-label"><span class="star">&bull;</span>Требования: </span><?php echo get_field('requirements');?></p>
                <?php endif;?>
                <?php if($kurs_price) : ?>
                    <p class="field-row"><span class="field-label"><span class="star">&bull;</span>Стоимость: </span><?php echo $kurs_price;?></p>
                <?php endif;?>
	            <?php if($razryad) : ?>
                    <p class="field-row"><span class="field-label"><span class="star">&bull;</span>Разряд: </span><?php echo $razryad;?></p>
	            <?php endif;?>
	            <?php if($study_period) : ?>
                    <p class="field-row"><span class="field-label"><span class="star">&bull;</span>Количество часов: </span><?php echo $study_period;?></p>
	            <?php endif;?>
            </div>
            <?php /*if($kurs_price) : */?><!--
            <div class="info-wrap">
                <span class="info-label"><span class="star">*</span>Стоимость: </span>
                <span class="info-text"><?php /*echo $kurs_price;*/?></span>
            </div>
            <?php /*endif;*/?>
            <?php /*if($razryad) : */?>
            <div class="info-wrap">
                <span class="info-label"><span class="star">*</span>Разряд: </span>
                <span class="info-text"><?php /*echo $razryad;*/?></span>
            </div>
            <?php /*endif;*/?>
            <?php /*if($study_period) : */?>
            <div class="info-wrap">
                <span class="info-label"><span class="star">*</span>Количество часов: </span>
                <span class="info-text"><?php /*echo $study_period;*/?></span>
            </div>
            --><?php /*endif;*/?>
	        <?php if(strpos($_SERVER['REQUEST_URI'], 'kursy-povysheniya-kvalifikacii') != false) : ?>
                <div style="text-align: center"><span class="header-content_hero-button call-form" style="cursor: pointer;">Получить диплом</span></div>
	        <?php else : ?>
                <div style="text-align: center"><span class="header-content_hero-button call-form" style="cursor: pointer;">Получить удостоверение</span></div>
	        <?php endif;?>
        </div>
    </section>
    <section class="sc13">
        <div class="container">
            <h2 class="sc15-title block-title">Какие документы получают после обучения</h2>
            <br>
            <div class="row">
	            <?php $documenty = get_field('documenty');?>
                <?php if($documenty != NULL) : ?>
                    <?php foreach($documenty as $item) : ?>
                    <div class="col-lg-6" style="padding: 0;">
                        <div class="wrapper wrapper3 document-wrapper">
                            <div class="overlay-content">
                                <div class="document-name-wrap">
                                    <p><?php echo $item['doc_name'];?></p>
                                </div>
                                <div class="document-image-wrap">
                                    <a data-fancybox="images2" href="<?php echo $item["doc_image"]["url"];?>" class="images2">
                                        <img src="<?php echo $item["doc_image"]["url"];?>" alt="" class="img-fluid fancybox" rel="fancybox">
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php endforeach;?>
                <?php else:?>
                    <div>
                        Документы будут представлены в ближайшее время
                    </div>
                <?php endif;?>
            </div>
        </div>
    </section>
    <section class="sc5">
        <div class="container">
            <h2 class="sc5-title">Наши лицензии</h2>
			<?php $licence = get_field('licence');?>

            <div class="row">
				<?php if($licence != NULL) : ?>
					<?php foreach($licence as $item) : ?>
                        <div class="col-lg-4 col-6">
                            <a data-fancybox="images2" href="<?php echo $item["licence_image"]["url"];?>">
                                <img src="<?php echo $item["licence_image"]["url"];?>" class="img-fluid">
                            </a>
                            <div class="sc5-bottom-content">
                                <div class="licence_name">
                                    <p><?php echo $item['licence_name'];?></p>
                                </div>
                                <div class="licence_popup">
                                    <img src="<? echo get_template_directory_uri()?>/img/zoom-ico.svg" alt="" class="img-fluid">
                                </div>
                            </div>
                        </div>
					<?php endforeach;?>
				<?php else:?>
                    <div>
                        Лицензии будут представлены в ближайшее время
                    </div>
				<?php endif;?>
            </div>
        </div>
    </section>
    <!--тут список специальностей с ценами таблица-->
    <div class="search-content-page-content">
        <section class="search-content-table-sc">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h2 class="sc5-title" style="text-align: center; font-weight: 700">Список специальностей</h2>
                    <br>
                    <div class="wrapper">
                        <?php
                            global $post;

                            $term = wp_get_post_terms($post->ID, 'kursicat');

                            $profile = get_field('profile');
                            $args = array(
                                'post_type' => 'kursi',
                                'posts_per_page' => 5,
                                'taxonomy' => 'kursicat',
	                            'orderby' => 'rand',
                                'tax_query' => array(
	                                array(
		                                'taxonomy' => 'kursicat',
		                                'field' => 'slug',
		                                'terms' => $term[0]->slug
	                                )
                                ),
                                'meta_query' => array(
	                                'relation' => 'AND',
	                                array(
		                                'key'     => 'profile',
		                                'value'   => $profile,
		                                'compare' => 'LIKE',
	                                ),
                                )
                            );
                            $query = new WP_Query( $args );
                        ?>
                        <?php if (!empty($query->posts)) :?>
                        <table>
                            <thead>
                            <tr>
                                <td class="radius">Название направления специальности</td>
                                <td>Цена (руб)</td>
                                <td></td>
                                <td class="radius"></td>
                            </tr>
                            </thead>
                            <tbody>
	                            <?php foreach ($query->posts as $post) : ?>
                                <?php

                                   $kurs_price = get_field('kurs_price', $post->ID);

                                ?>
                                <tr>
                                    <td data-label="Специальность" class="label"><a href="<?php the_permalink() ?>" class="prof-title"><?php echo $post->post_title;?></a></td>
                                    <td data-label="Цена" class="label study-period"><?php echo $kurs_price;?></td>
                                    <td><span class="call-form study-begin">пройти обучение</span></td>
                                    <td class="no-wrap"><a href="#" class="call-form" style="color: #ff2626;">Хочу скидку</a></td>
                                </tr>
	                            <?php endforeach; ?>
                            </tbody>
                        </table>
                        <?php
                            else :
                                echo "В данной категории еще нет записей";
                            endif;
                        ?>
	                    <?php wp_reset_postdata(); ?>
                    </div>
                </div>
            </div>
        </div>
        </section>
    </div>
    <section class="sc17">
        <div class="container">
            <h2 class="sc17-title block-title">Этапы работы</h2>
            <br><br>
            <div class="row">
                <div class="col">
                    <div class="wrapper stage-img"> <img src="<? echo get_template_directory_uri()?>/img/sc17-ico1.png" alt="" class="img-fluid">
                        <p class="wrapper-title">Заявка</p>
                        <p class="wrapper-desc">Оставьте заявку на сайте или позвоните по телефону</p>
                    </div>
                </div>
                <div class="col">
                    <div class="wrapper stage-img"> <img src="<? echo get_template_directory_uri()?>/img/sc17-ico2.png" alt="" class="img-fluid">
                        <p class="wrapper-title">Консультация</p>
                        <p class="wrapper-desc">Консультация со специалистом. Обсуждаем индивидульные условия работы, строки и стоимость</p>
                    </div>
                </div>
                <div class="col">
                    <div class="wrapper stage-img"> <img src="<? echo get_template_directory_uri()?>/img/sc17-ico3.png" alt="" class="img-fluid">
                        <p class="wrapper-title">Договор</p>
                        <p class="wrapper-desc">Заключаем договор. Для этого вам понадобится: паспорт, фотография 3*4, документ об образовании, трудовая книжка </p>
                    </div>
                </div>
                <div class="col">
                    <div class="wrapper stage-img"> <img src="<? echo get_template_directory_uri()?>/img/sc17-ico4.png" alt="" class="img-fluid">
                        <p class="wrapper-title">Материалы</p>
                        <p class="wrapper-desc">Заключаем договор. Для Мы высылаем вам методиеские материалы на почту или флешку</p>
                    </div>
                </div>
                <div class="col">
                    <div class="wrapper stage-img"> <img src="<? echo get_template_directory_uri()?>/img/sc17-ico5.png" alt="" class="img-fluid">
                        <p class="wrapper-title">Доставка</p>
                        <p class="wrapper-desc">В течении 2-3 рабочих дней вы получите документы в офисеили доставкой курьером</p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="sc3">
        <div class="container">
            <h2 class="sc3-title block-title">Что мы гарантируем</h2>
            <div class="row">
                <div class="col-lg-4 col-md-6 custom-width">
                    <div class="wrapper why-choose choose-taxonomy-page">
                        <img src="<? echo get_template_directory_uri()?>/img/sc3-wrapper-ico1.svg" alt="" class="img-fluid svg-wrap">
                        <div class="choose-text-wrap">
                            <p class="sc3-wrapper__title">Быстро</p>
                            <p class="sc3-wrapper__desc">От 2 до 4 дней Доставка курьером 0₽</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 custom-width">
                    <div class="wrapper why-choose choose-taxonomy-page">
                        <img src="<? echo get_template_directory_uri()?>/img/sc3-wrapper-ico2.svg" alt="" class="img-fluid svg-wrap">
                        <div class="choose-text-wrap">
                            <p class="sc3-wrapper__title">Все удостоверения в одном месте</p>
                            <p class="sc3-wrapper__desc">Рабочие специальности, охрана труда, ПТМ, ГО и ЧС</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 custom-width">
                    <div class="wrapper why-choose choose-taxonomy-page">
                        <img src="<? echo get_template_directory_uri()?>/img/sc3-wrapper-ico3.svg" alt="" class="img-fluid svg-wrap">
                        <div class="choose-text-wrap">
                            <p class="sc3-wrapper__title">Доступные цены </p>
                            <p class="sc3-wrapper__desc">Лучшие цены в городе</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 custom-width">
                    <div class="wrapper why-choose choose-taxonomy-page">
                        <img src="<? echo get_template_directory_uri()?>/img/sc3-wrapper-ico4.svg" alt="" class="img-fluid svg-wrap">
                        <div class="choose-text-wrap">
                            <p class="sc3-wrapper__title">Дистанционное обучение</p>
                            <p class="sc3-wrapper__desc"> - без отрыва от работы и производства</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 custom-width">
                    <div class="wrapper why-choose choose-taxonomy-page">
                        <img src="<? echo get_template_directory_uri()?>/img/sc3-wrapper-ico5.svg" alt="" class="img-fluid svg-wrap">
                        <div class="choose-text-wrap">
                            <p class="sc3-wrapper__title">Все законно </p>
                            <p class="sc3-wrapper__desc">Учебный центр имеет все лицензии и аккредитации - работаем по договору</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 custom-width">
                    <div class="wrapper why-choose choose-taxonomy-page">
                        <img src="<? echo get_template_directory_uri()?>/img/sc3-wrapper-ico6.svg" alt="" class="img-fluid svg-wrap">
                        <div class="choose-text-wrap">
                            <p class="sc3-wrapper__title">Все сведения заносим в реестр</p>
                            <p class="sc3-wrapper__desc">Выдаем документы гос образца</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="sc4">
        <div class="container">
            <h2 class="sc4-title">Отзывы наших учеников</h2>
            <div class="row">
                <div class="col-lg-12">
	                <?php
                        $args = array(
                            'post_type' => 'wpm-testimonial',
                            'posts_per_page' => -1,
                            'orderby' => 'rand',
                        );
	                    $query = new WP_Query( $args );
	                ?>
                    <?php if (!empty($query->posts)) :?>
                    <div class="sc4-review-slider">
                        <?php foreach ($query->posts as $post) :?>
                            <?php
                                $attr = array(
                                'class'	=> 'img-fluid'
                                );
                                $testimonial_img = get_the_post_thumbnail( $post->ID, $size = 'full', $attr );
	                            $testimonial_link = get_the_post_thumbnail_url( $post->ID );
                            ?>
                        <div class="sc4-review-slider__item">
                            <a data-fancybox="images1" href="<?php echo $testimonial_link;?>"><?php echo $testimonial_img;?></a>
                        </div>
                        <?php endforeach;?>
                    </div>

                    <div class="sc4-review-slider-arrow-content">
                        <div class="sc4-review-slider__prev"><img src="<? echo get_template_directory_uri()?>/img/sc4-review-slider__prev.svg" alt="" class="img-fluid"></div>
                        <div class="sc4-review-slider__next"><img src="<? echo get_template_directory_uri()?>/img/sc4-review-slider__next.svg" alt="" class="img-fluid"></div>
                    </div>
                    <?php endif;?>
                </div>
            </div>
        </div>
    </section>

    <section class="sc9">
        <div class="container">
            <div class="wrapper">
                <div class="row">
                    <div class="col-lg-6"> <img src="<? echo get_template_directory_uri()?>/img/sc9-img1.png" alt="" class="img-fluid"> </div>
                    <div class="col-lg-6">
                        <h2>Скидки до 20% <br><span>от 5 человек</span></h2>
                        <?php echo do_shortcode('[contact-form-7 id="972" title="получить скидку"]')?>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="sc6">
        <div class="container">
			<?php
			$clients = get_field('clients', 1183);
			?>
            <h2 class="sc6-title">Наши клиенты</h2>
            <div class="row">
                <div class="col-lg-12">
                    <div class="our-client-slider">
						<?php foreach ($clients as $client) :?>
							<?php foreach ($client as $client_elem) :?>
                                <div class="our-client-slider__item">
                                    <img src="<? echo $client_elem["url"];?>" alt="<?php echo $client_elem["alt"];?>" class="img-fluid">
                                </div>
							<?php endforeach;?>
						<?php endforeach;?>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="sc10">
        <div class="container">
            <div class="wrapper">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="wrapper-left">
		                    <?php get_search_form(); ?>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="wrapper-right">
                            <p>Все курсы по алфавиту</p>
                            <div class="alphabite-content">
	                            <?php
	                            $posts = get_posts('post_type=kursi&orderby=title&numberposts=-1&order=ASC');

	                            foreach( $posts as $k => $post ){

		                            // первая буква
		                            $fl = get_first_letter( $post->post_title );
		                            $prev_fl = isset( $posts[ ($k-1) ] ) ? get_first_letter( $posts[ ($k-1) ]->post_title ) : '';
		                            if( $prev_fl !== $fl ){ ?>
                                        <div>
                                            <p><a href="/?s=<? echo $fl;?>"><? echo $fl;?></a></p>
                                        </div>
		                            <?}
	                            }
	                            wp_reset_postdata();
	                            ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php
get_footer();