<?php
get_header();
$term = get_queried_object();
$term_id = get_queried_object_id();
$image_id = get_term_meta( $term_id, '_thumbnail_id', 1 );
$image_url = wp_get_attachment_image_url( $image_id, 'full' );
$taxonomy = 'kursicat';
$licence = get_field('licence', $taxonomy . '_' . $term_id );
$documenty = get_field('documenty', $taxonomy . '_' . $term_id);
$seo_text = get_field('seo_text', $taxonomy . '_' . $term_id);
$seo_text = parse_all_content($seo_text);
//$term->description;
//$term->name;

?>

<div class="banner-wrap">
    <div class="banner" style="background-image: url('<?php echo $image_url;?>'); background-size: cover;" >
        <div class="header-content_hero">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="header-content_hero-slider-item">
                            <h1 class="header-content_hero-title"><?php echo get_field('slide_capt', $taxonomy . '_' . $term_id);?></h1>
                            <div class="header-content_hero-desc"><?php echo get_field('slide_pre_capt', $taxonomy . '_' . $term_id);?></div>
                            <?php if(strpos($_SERVER['REQUEST_URI'], 'kursy-povysheniya-kvalifikacii') != false) : ?>
                                <span class="header-content_hero-button call-form" style="cursor: pointer">Получить удостоверение</span>
                            <?php else : ?>
                                <span class="header-content_hero-button call-form" style="cursor: pointer">Пройти обучение</span>
                            <?php endif;?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-md-12 breadcrumbs-wrap">
            <?php
                if( function_exists('kama_breadcrumbs') ) kama_breadcrumbs();
            ?>
        </div>
    </div>
</div>
<section class="sc2">
    <div class="container">
        <h2 class="sc2-title">Учебный центр повышения квалификации “ДПО АНТА” - <span>дополнительное профессиональное образование, переподготовка</span></h2>
        <div class="" id="content">
	        <?php echo $seo_text;?>
        </div>
    </div>
</section>
<section class="sc13">
    <div class="container">
        <h2 class="sc15-title block-title">Какие документы получают после обучения</h2>
        <br>
        <div class="row">

            <?php if($documenty != NULL) : ?>
                <?php foreach($documenty as $item) : ?>
                    <div class="col-lg-6" style="padding: 0;">
                        <div class="wrapper wrapper3 document-wrapper">
                            <div class="overlay-content">
                                <div class="document-name-wrap">
                                    <p><?php echo $item['doc_name'];?></p>
                                </div>
                                <div class="document-image-wrap">
                                    <a data-fancybox="images2" href="<?php echo $item["doc_image"]["url"];?>" class="images2">
                                        <img src="<?php echo $item["doc_image"]["url"];?>" alt="" class="img-fluid fancybox" rel="fancybox">
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endforeach;?>
            <?php else:?>
                <div>
                    Документы будут представлены в ближайшее время
                </div>
            <?php endif;?>
            <?php wp_reset_postdata();?>
        </div>
    </div>
</section>
<!--тут список профессий-->
<div class="search-content-page-content">
<?php
    $field = get_field_object('profile');
?>
<section class="search-content-table-sc">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h2 class="sc5-title block-title">Список профессий</h2>

                <?php if($_SERVER['REQUEST_URI'] == '/obuchenie-rabochim-professiyam/') : ?>
                <div class="choices-wrap">
                <?php
                    $cat_request = explode('?', $_SERVER['REQUEST_URI']);
                    $cat_url = $cat_request['0'];
                ?>

                <?php foreach ($field["choices"] as $choice) : ?>
                    <a href="<?php echo site_url() .$cat_url. "?profile=" . $choice;?>" class="choice-link"><?php echo $choice;?></a>
                <?php endforeach;?>

                </div>
                <?php else : ?>
                <br>
                <?php endif;?>
                <div class="wrapper">
	                <?php
                        $profile = $_GET["profile"];
                        if($profile){
	                        $params = array(
		                        'tax_query' => array(
			                        array(
				                        'taxonomy' => 'kursicat',
				                        'field' => 'slug',
				                        'terms' => $term->slug
			                        )
		                        ),
		                        'meta_query' => array(
			                        array(
				                        'key' => 'profile',
				                        'value' => $profile,
			                        ),
		                        ),

	                        );
                        } else {
	                        $params = array(
		                        'tax_query' => array(
			                        array(
				                        'taxonomy' => 'kursicat',
				                        'field' => 'slug',
				                        'terms' => $term->slug
			                        )
		                        ),
		                        'post_per_page' => -1,
		                        'post_type' => 'kursi'
                            );
                        }

                        $query = new WP_Query( $params );
                    ?>
                    <?php if (!empty($query->posts)) :?>
                        <table class="professions_table">
                            <thead>
                            <tr>
                                <td class="radius">Специальность</td>
                                <td class="no-wrap">Срок обучения</td>
                                <td class="no-wrap">цена (руб)</td>
                                <td></td>
                                <td class="radius"></td>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($query->posts as $post) : ?>
                                <?php
                                    $setTitle = get_the_title();
	                                $period = get_field('study_period');
	                                $price = get_field('kurs_price');
                                ?>
                                <tr>
                                    <td data-label="Специальность" class="label"><a href="<?php the_permalink() ?>" class="prof-title dark"><?php the_title() ?></td>
                                    <td data-label="Срок обучения" class="label no-wrap">&nbsp;<?php echo $period;?></td>
                                    <td data-label="цена (руб)" class="label no-wrap">&nbsp;<?php echo $price;?></td>
                                    <td class="no-wrap"><a href="#" class="call-form study-begin">пройти обучение</a></td>
                                    <td class="no-wrap"><a href="#" class="call-form">Хочу скидку</a></td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                        <div class="show-all-wrap">
                            <span class="show-all">показать все</span>
                        </div>
                    <?php
                    else :
                        echo "В данной категории еще нет записей";
                    endif;
                    ?>
	                <?php wp_reset_postdata(); ?>
                </div>
            </div>
        </div>
    </div>
</section>
</div>
<!--тут список профессий-->
    <section class="sc3">
        <div class="container">
            <h2 class="sc3-title block-title">Что мы гарантируем</h2>
            <div class="row">
                <div class="col-lg-4 col-md-6 custom-width">
                    <div class="wrapper why-choose choose-taxonomy-page">
                        <img src="<? echo get_template_directory_uri()?>/img/sc3-wrapper-ico1.svg" alt="" class="img-fluid svg-wrap">
                        <div class="choose-text-wrap">
                            <p class="sc3-wrapper__title">Быстро</p>
                            <p class="sc3-wrapper__desc">От 2 до 4 дней Доставка курьером 0₽</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 custom-width">
                    <div class="wrapper why-choose choose-taxonomy-page">
                        <img src="<? echo get_template_directory_uri()?>/img/sc3-wrapper-ico2.svg" alt="" class="img-fluid svg-wrap">
                        <div class="choose-text-wrap">
                            <p class="sc3-wrapper__title">Все удостоверения в одном месте</p>
                            <p class="sc3-wrapper__desc">Рабочие специальности, охрана труда, ПТМ, ГО и ЧС</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 custom-width">
                    <div class="wrapper why-choose choose-taxonomy-page">
                        <img src="<? echo get_template_directory_uri()?>/img/sc3-wrapper-ico3.svg" alt="" class="img-fluid svg-wrap">
                        <div class="choose-text-wrap">
                            <p class="sc3-wrapper__title">Доступные цены </p>
                            <p class="sc3-wrapper__desc">Лучшие цены в городе</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 custom-width">
                    <div class="wrapper why-choose choose-taxonomy-page">
                        <img src="<? echo get_template_directory_uri()?>/img/sc3-wrapper-ico4.svg" alt="" class="img-fluid svg-wrap">
                        <div class="choose-text-wrap">
                            <p class="sc3-wrapper__title">Дистанционное обучение</p>
                            <p class="sc3-wrapper__desc"> - без отрыва от работы и производства</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 custom-width">
                    <div class="wrapper why-choose choose-taxonomy-page">
                        <img src="<? echo get_template_directory_uri()?>/img/sc3-wrapper-ico5.svg" alt="" class="img-fluid svg-wrap">
                        <div class="choose-text-wrap">
                            <p class="sc3-wrapper__title">Все законно </p>
                            <p class="sc3-wrapper__desc">Учебный центр имеет все лицензии и аккредитации - работаем по договору</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 custom-width">
                    <div class="wrapper why-choose choose-taxonomy-page">
                        <img src="<? echo get_template_directory_uri()?>/img/sc3-wrapper-ico6.svg" alt="" class="img-fluid svg-wrap">
                        <div class="choose-text-wrap">
                            <p class="sc3-wrapper__title">Все сведения заносим в реестр</p>
                            <p class="sc3-wrapper__desc">Выдаем документы гос образца</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


<section class="sc5">
    <div class="container">
        <h2 class="sc5-title">Наши лицензии</h2>

        <div class="row">
            <?php if($licence != NULL) : ?>
                <?php foreach($licence as $item) : ?>
                    <div class="col-lg-4 col-6">
                        <a data-fancybox="images2" href="<?php echo $item["licence_image"]["url"];?>">
                            <img src="<?php echo $item["licence_image"]["url"];?>" class="img-fluid">
                        </a>
                        <div class="sc5-bottom-content">
                            <div class="licence_name">
                                <p><?php echo $item['licence_name'];?></p>
                            </div>
                            <div class="licence_popup">
                                <img src="<? echo get_template_directory_uri()?>/img/zoom-ico.svg" alt="" class="img-fluid">
                            </div>
                        </div>
                    </div>
                <?php endforeach;?>
            <?php else:?>
                <div>
                    Лицензии будут представлены в ближайшее время
                </div>
            <?php endif;?>
	        <?php wp_reset_postdata(); ?>
        </div>


    </div>
</section>



<section class="sc4">
    <div class="container">
        <h2 class="sc4-title">Отзывы наших учеников</h2>
        <div class="row">
            <div class="col-lg-12">
                <?php
                $args = array(
                    'post_type' => 'wpm-testimonial',
                    'posts_per_page' => -1,
                    'orderby' => 'rand',
                );
                $query = new WP_Query( $args );
                ?>
                <?php if (!empty($query->posts)) :?>
                    <div class="sc4-review-slider">
                        <?php foreach ($query->posts as $post) :?>
                            <?php
                            $attr = array(
                                'class'	=> 'img-fluid'
                            );
                            $testimonial_img = get_the_post_thumbnail( $post->ID, $size = 'full', $attr );
	                        $testimonial_link = get_the_post_thumbnail_url( $post->ID );
                            ?>
                            <div class="sc4-review-slider__item">
                                <a data-fancybox="images1" href="<?php echo $testimonial_link;?>"><?php echo $testimonial_img;?></a>
                            </div>
                        <?php endforeach;?>
                    </div>

                    <div class="sc4-review-slider-arrow-content">
                        <div class="sc4-review-slider__prev"><img src="<? echo get_template_directory_uri()?>/img/sc4-review-slider__prev.svg" alt="" class="img-fluid"></div>
                        <div class="sc4-review-slider__next"><img src="<? echo get_template_directory_uri()?>/img/sc4-review-slider__next.svg" alt="" class="img-fluid"></div>
                    </div>
                <?php endif;?>
	            <?php wp_reset_postdata(); ?>
            </div>
        </div>
    </div>
</section>
<section class="sc9">
    <div class="container">
        <div class="wrapper">
            <div class="row">
                <div class="col-lg-6"> <img src="<? echo get_template_directory_uri()?>/img/sc9-img1.png" alt="" class="img-fluid"> </div>
                <div class="col-lg-6">
                    <h2>Скидки до 20% <br><span>от 5 человек</span></h2>
                    <?php echo do_shortcode('[contact-form-7 id="972" title="получить скидку"]')?>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="sc6">
    <div class="container">
        <?php
        $clients = get_field('clients', 1183);
        ?>
        <h2 class="sc6-title">Наши клиенты</h2>
        <div class="row">
            <div class="col-lg-12">
                <div class="our-client-slider">
                    <?php foreach ($clients as $client) :?>
                        <?php foreach ($client as $client_elem) :?>
                            <div class="our-client-slider__item">
                                <img src="<? echo $client_elem["url"];?>" alt="<?php echo $client_elem["alt"];?>" class="img-fluid">
                            </div>
                        <?php endforeach;?>
                    <?php endforeach;?>
	                <?php wp_reset_postdata(); ?>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="sc10">
    <div class="container">
        <div class="wrapper">
            <div class="row">
                <div class="col-lg-6">
                    <div class="wrapper-left">
		                <?php get_search_form(); ?>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="wrapper-right">
                        <p>Все курсы по алфавиту</p>
                        <div class="alphabite-content">
	                        <?php
	                        $posts = get_posts('post_type=kursi&orderby=title&numberposts=-1&order=ASC');

	                        foreach( $posts as $k => $post ){

		                        // первая буква
		                        $fl = get_first_letter( $post->post_title );
		                        $prev_fl = isset( $posts[ ($k-1) ] ) ? get_first_letter( $posts[ ($k-1) ]->post_title ) : '';
		                        if( $prev_fl !== $fl ){ ?>
                                    <div>
                                        <p><a href="/?s=<? echo $fl;?>"><? echo $fl;?></a></p>
                                    </div>
		                        <?}
	                        }
	                        wp_reset_postdata();
	                        ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php


//echo '<img src="'. $image_url .'" alt="" />';
/*echo '<pre>';
var_dump($term, $term->name, $term->description);
echo '</pre>';*/


?>

<?php
get_footer();
