<?php
/**
 * Template Name: prices
 * Template Post Type: post, page, product
 */

get_header();

?>

<section class="sc11">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<?php
					if( function_exists('kama_breadcrumbs') ) kama_breadcrumbs();
				?>
			</div>
		</div>
	</div>
</section>
<div class="search-content-page-content">
<section class="search-content-table-sc">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
                <div class="page-title"><h1>Цены на обучение</h1></div>
				<div class="choices-wrap">
					<?php
						$fields = get_custom_fields_for_prices('profile');

                        $cat_request = explode('?', $_SERVER['REQUEST_URI']);
						$cat_url = $cat_request['0'];
					?>

					<?php foreach ($fields as $choice) : ?>
						<a href="<?php echo site_url() .$cat_url. "?profile=" . $choice->meta_value;?>" class="choice-link"><?php echo $choice->meta_value;?></a>
					<?php endforeach;?>

				</div>
				<div class="wrapper">
					<?php
					$profile = $_GET["profile"];
					if($profile){
						$args = array(
							'post_type' => 'kursi',
							'posts_per_page' => -1,
							'taxonomy' => 'kursicat',
							'meta_query' => array(
								array(
									'key' => 'profile',
									'value' => $profile,
								),
							),

						);
					} else {
						$args = array(
							'post_type' => 'kursi',
							'posts_per_page' => -1,
							'taxonomy' => 'kursicat'

						);
					}

					$query = new WP_Query( $args );

					?>
					<?php if (!empty($query->posts)) :?>
						<table class="professions_table">
							<thead>
							<tr>
								<td class="radius">Специальность</td>
								<td class="no-wrap">Срок обучения</td>
								<td class="no-wrap">цена (руб)</td>
								<td></td>
								<td class="radius"></td>
							</tr>
							</thead>
							<tbody>
							<?php foreach ($query->posts as $post) : ?>
								<?php
								$setTitle = get_the_title();
								$period = get_field('study_period');
								$price = get_field('kurs_price');
								?>
								<tr>
									<td data-label="Специальность" class="label"><a href="<?php the_permalink() ?>" class="prof-title dark"><?php the_title() ?></td>
									<td data-label="Срок обучения&nbsp;" class="label no-wrap"><?php echo $period;?></td>
									<td data-label="цена (руб)&nbsp;" class="label no-wrap"><?php echo $price;?></td>
                                    <td><a href="#" class="call-form no-wrap study-begin">пройти обучение</a></td>
									<td><a href="#" class="call-form no-wrap">Хочу скидку</a></td>
								</tr>
							<?php endforeach; ?>
							</tbody>
						</table>
						<div class="show-all-wrap">
							<span class="show-all">показать все</span>
						</div>
					<?php
					else :
						echo "В данной категории еще нет записей";
					endif;
					?>
					<?php wp_reset_postdata(); ?>
				</div>
			</div>
		</div>
	</div>
</section>
</div>
<section class="sc9">
	<div class="container">
		<div class="wrapper">
			<div class="row">
				<div class="col-lg-6"> <img src="<? echo get_template_directory_uri()?>/img/sc9-img1.png" alt="" class="img-fluid"> </div>
				<div class="col-lg-6">
					<h2>Скидки до 20% <br><span>от 5 человек</span></h2>
					<!--<form action="" method="post">
						<input type="text" name="name" placeholder="Ваше имя" class="sc9-wrapper-form-input">
						<input type="text" name="phone" placeholder="Номер телефона" class="sc9-wrapper-form-input">
						<input type="email" name="email" placeholder="E-mail" class="sc9-wrapper-form-input">
						<input type="submit" value="Получить скидку" class="sc9-wrapper-form-submit">
						<p><img src="img/verified.png" alt="" class="img-fluid">Ваши данные защищены и не будут переданы третьим лицам</p>
					</form>-->
					<?php echo do_shortcode('[contact-form-7 id="972" title="получить скидку"]')?>
				</div>
			</div>
		</div>
	</div>
</section>

<?php
get_footer();

