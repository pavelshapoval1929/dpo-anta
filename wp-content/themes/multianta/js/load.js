(function ($) {
    $(document).ready(function($){

        function load_posts(items_count){
            var ajaxurl = "/wp-admin/admin-ajax.php";
            var str = '&action=clients_loader' + '&key=' + items_count;

            $.ajax({
                type: "POST",
                dataType: "html",
                url: ajaxurl,
                data: str,
                success: function(data){

                    $("#ajax-posts").append($data);
                    var $data = $(data);

                    if($data.length) {
                        $("#ajax-posts").append($data);

                    } else {
                       console.log('отображены все элементы');
                        $(".load_more-wrap").text('Отображены все элементы');
                        $("#more_posts").hide();
                    }
                },
                error : function(jqXHR, textStatus, errorThrown) {
                    $loader.html(jqXHR + " :: " + textStatus + " :: " + errorThrown);
                }

            });
            return false;
        }

        $("#more_posts").on("click",function(){ // When btn is pressed.
            var items_count = $('.clients-elem').length;
            load_posts(items_count);
        });
    });
})(jQuery);