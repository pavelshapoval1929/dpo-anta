 jQuery(document).ready(function($) {
         $(function () {
             $(".phone").mask("+7(999) 999-9999");
         });
         $('.call, .share_btn button, .call-form').on('click', function (e) {
            e.preventDefault();
            $('.layer').fadeIn(100);
         });
         $('.layer').on('click', function () {
            $(this).fadeOut(100);
         });
         $('.form-call-wrapper').on('click', function (e) {
             e.preventDefault();
             e.stopPropagation();
         });
         $('.form-call-wrapper svg').on('click', function (e) {
             $('.layer').fadeOut(100);
         });

         $('.images2').attr('data-fancybox','images2');


         $('.text-tab').click(function(event){
                event.preventDefault();
                $('.text-tab').hide();
              $('.text-order').show(600);
            });
         $('.wpcf7-form-control.wpcf7-submit.call-submit').on('click', function () {
             $(this).parents('form').submit();
         });
         var url_prof = location.href;
         $('.hidden-prof').val(url_prof);

         $('.header-content_hero-slider').slick({
             dots: true
             , infinite: true
             , autoplay: true
             , autoplaySpeed: 3000
             , slidesToShow: 1
             , fade: true
             , focusOnSelect: true
             , adaptiveHeight: true
             , cssEase: 'linear'
             , slidesToScroll: 1
         });


         $("#button1").click(function () {
             $('html, body').animate({
                 scrollTop: $("#about").offset().top
             }, 1500);
         });

         $('.sc4-review-slider').slick({
             dots: false
             , speed: 1500
             , infinite: true
             , autoplay: true
             , autoplaySpeed: 3000
             , slidesToShow: 5
             , centerMode: true
             , slidesToScroll: 1
             , nextArrow: '.sc4-review-slider__next'
             , prevArrow: '.sc4-review-slider__prev'
             , responsive: [
                 {
                     breakpoint: 1024
                     , settings: {
                         slidesToShow: 5
                         , slidesToScroll: 1
                         , infinite: true
                         , dots: false
                         , autoplay: true
                         , autoplaySpeed: 3000
                     , }
            }
                 ,{
                     breakpoint: 600
                     , settings: {
                         slidesToShow: 3
                         , slidesToScroll: 1
                         , infinite: true
                         , dots: false
                         , autoplay: true
                         , autoplaySpeed: 3000
                     , }
            }
                 , {
                     breakpoint: 480
                     , settings: {
                         dots: false
                         , slidesToShow: 1
                         , slidesToScroll: 1
                         , infinite: true
                         , dots: false
                         , autoplay: true
                         , autoplaySpeed: 3000
                     , }
            }]
         });


         $('.our-client-slider').slick({
             dots: true
             , speed: 1500
             , infinite: true
             , autoplay: true
             , autoplaySpeed: 3000
             , slidesToShow: 4
             , slidesToScroll: 1
             , responsive: [
                 {
                     breakpoint: 1024
                     , settings: {
                         slidesToShow: 3
                         , slidesToScroll: 1
                         , infinite: true
                         , dots: false
                         , autoplay: true
                         , autoplaySpeed: 3000
                     , }
            }
                 ,{
                     breakpoint: 600
                     , settings: {
                         slidesToShow: 2
                         , slidesToScroll: 1
                         , infinite: true
                         , dots: true
                         , autoplay: true
                         , autoplaySpeed: 3000
                     , }
            }
                 , {
                     breakpoint: 480
                     , settings: {
                         dots: true
                         , slidesToShow: 1
                         , slidesToScroll: 1
                         , infinite: true
                         , dots: true
                         , autoplay: true
                         , autoplaySpeed: 3000
                     , }
            }]
         });
        $('.testimonial-content-wrap').each(function(){
            var string = $(this).text();
            //console.log(string);
            if((string.length)>1000){
                $(this).next('.readmore-wrap').find('.readmore-span').show();
            }
            //console.log(string.length);
        });
        $('.readmore-span').on('click', function () {
            $(this).parent().siblings('.testimonial-content-wrap').removeClass('testimonial-hidden');
            $(this).hide();
            $(this).parent().find('.readless').show();
        });
        $('.readless').on('click', function () {
            $(this).parent().siblings('.testimonial-content-wrap').addClass('testimonial-hidden');
            $(this).hide();
            $(this).parent().find('.readmore-span').show();
        });

         $('.choice-link').each(function () {
             var location = window.location.href;
             var link = this.href;
             if(location == link) {
                 $(this).addClass('active');
             }
         });

         $(document).on('click', '.readmore_add', function () {
             $(this).parents('.share-text').find('.less').addClass('not-show');
             $(this).parents('.share-text').find('.full').removeClass('not-show');
         });
         $(document).on('click', '.readless_add', function () {
             $(this).parents('.share-text').find('.less').removeClass('not-show');
             $(this).parents('.share-text').find('.full').addClass('not-show');
         });

         $('.show-all').on('click', function () {
            $(this).parent('.show-all-wrap').siblings('table').removeClass('professions_table');
            $(this).hide();
         });
         $('.show_contacts').on('click', function () {
             $(this).toggleClass('rotate');
             $('.header-content').slideToggle(100);
         });




});