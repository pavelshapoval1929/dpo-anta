<?php get_header(); ?>
<? $countSearch = get_search_query();?>
<? $getSearch = (mb_strlen($countSearch) == 1) ? true : false ;?>
<div class="search-content-page-content">
    <section class="search-content-content-sc">
        <div class="search-content-content-sc-answer">
            <div class="container">
                <div class="row">
                    <div class="col-12 search-1">
                        <div class="text1">Результат поиска:&nbsp;</div>
                    </div>
                    <div class="col-12 search-1">
                    	<div class="text1">"<?php echo $countSearch; ?>"</div>
                    </div>
                    <div class="col-12 search-1">
                        <p class="text2"> <a href="/">Закрыть
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24">
                                <path fill="none" d="M0 0h24v24H0z" />
                                <path d="M12 10.586l4.95-4.95 1.414 1.414-4.95 4.95 4.95 4.95-1.414 1.414-4.95-4.95-4.95 4.95-1.414-1.414 4.95-4.95-4.95-4.95L7.05 5.636z" />
                            </svg>
                            </a> </p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="search-content-table-sc">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="wrapper">
	                    <?php
	                        $args = array_merge( $wp_query->query, array( 'post_type' => array("post", "kursi")) );
	                        query_posts($args); ?>
                        	<?php if (have_posts()) :?>
                                <p style="font-weight: bold">Найдено:</p>
                        	<table>
                                <thead>
                                    <tr>
                                        <td>Специальность</td>
                                        <td>Срок обучения</td>
                                        <td>цена (руб)</td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                </thead>
                                <tbody>
                                <? $e = 1; ?>
                                <? while (have_posts()) : the_post(); ?>
                                <?php
                                    $post = get_post();
                                    $kurs_price = get_field('kurs_price', $post->ID);
                                    $study_period = get_field('study_period', $post->ID);
                                ?>
                                    <? if($getSearch) :?>
                                        <? //echo $e; ?>
                                            <? $setTitle = get_the_title();?>
                                            <? $str = mb_substr($setTitle, 0, 1, 'utf-8');?>
                                            <? //echo $str; ?>
                                            <? if($str == $countSearch) { ?>
    			                                <tr>
    			                                    <td data-label="Специальность" class="search-label"><a href="<?php the_permalink() ?>" class="prof-title"><?php the_title() ?></td>
    			                                    <td data-label="Срок обучения">&nbsp;<?php echo $study_period;?></td>
    			                                    <td data-label="цена (руб)">&nbsp;<?php echo $kurs_price;?></td>
    			                                    <td><a href="" class="call study-begin">пройти обучение</a></td>
    			                                    <td><a href="" class="call">Заказать звонок</a></td>
    			                                </tr>
                                            <? } ?>
                                    <? else : ?>
                                        <? //echo $e; ?>
                                                <tr>
                                                    <td data-label="Специальность" class="search-label"><a href="<?php the_permalink() ?>" class="prof-title"><?php the_title() ?></td>
                                                    <td data-label="Срок обучения">&nbsp;<?php echo $study_period;?></td>
                                                    <td data-label="цена (руб)">&nbsp;<?php echo $kurs_price;?></td>
                                                    <td><a href="" class="call study-begin">пройти обучение</a></td>
                                                    <td><a href="" class="call">Заказать звонок</a></td>
                                                </tr>
                                    <? endif; ?>
                                    <? //$e++; ?>
    							<? endwhile; ?>
                                </tbody>
                            </table>
                            <?php
    							else :
    							echo "Извините по Вашему результату ничего не найдено";
    							endif;
    						?>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<?php get_footer(); ?>