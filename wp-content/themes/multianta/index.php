<?php
/*
Template Name: Шаблон: Главная
*/
?>
<?php get_header(); ?>
<div class="main-content">
<? $all_options = get_option('true_options');
$all_gorod = do_shortcode('[multiplication var="'.$all_options['my_gorod'].'"]');
?>
    <div class="sc1">
        <div class="header-content_hero-slider">
            <?php $args = array(
                            'post_type' => 'slider',
                            'taxonomy' => 'home'
                        );
            $p = get_posts( $args );

            foreach ( $p as $post ) {
                setup_postdata( $post );
                ?>
                <div class="header-content_hero-slider-item" style="background-image: url('<?php the_post_thumbnail_url(full) ?>'); background-size: cover;" >
                    <div class="header-content_hero">
                        <div class="container">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="header-content_hero-slider-item">
                                        <p class="header-content_hero-title"><?= get_the_content() ?></p>
                                        <?php $excerpt = get_the_excerpt();?>
                                        <?php if($excerpt !='') : ?>
                                        <p class="header-content_hero-desc"><?php echo $excerpt;?></p>
                                        <?php endif;?>
                                        <script data-b24-form="click/71/4qg0cm" data-skip-moving="true">
                                    (function(w,d,u){
                                    var s=d.createElement('script');s.async=true;s.src=u+'?'+(Date.now()/180000|0);
                                    var h=d.getElementsByTagName('script')[0];h.parentNode.insertBefore(s,h);
                                    })(window,document,'https://cdn-ru.bitrix24.ru/b6368597/crm/form/loader_71.js');
                                    </script>
                                        <button class="header-content_hero-button call">Подробнее</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        <?php } wp_reset_postdata(); ?>
        </div>
    </div>
</div>
<section class="sc2">
    <div class="container">
        <h1 class="sc2-title"><?php echo get_field('mainpage_title');?></h1>
        <div class="row">
	        <?php
	            $link_block = get_field('link_block');
	        ?>
            <?php foreach ($link_block as $item):?>
            <div class="col-lg-2 col-6">
                <a href="<?php echo $item['link_link'];?>">
                    <div class="wrapper">
                        <?php echo $item['link_svg']?>
                        <h3><?php echo $item['link_text']?></h3>
                    </div>
                </a>
            </div>
            <?php endforeach;?>
        </div>
    </div>
</section>
<section class="sc3">
    <div class="container">
        <h2 class="sc3-title">Почему нас выбирают</h2>
        <div class="row">
            <div class="col-lg-12">
                <div class="row">
	                <?php
	                    $choose_block_left = get_field('choose_block_left');
	                ?>
	                <?php foreach ($choose_block_left as $item) : ?>
                        <div class="col-lg-4 col-md-6 custom-width">
                            <div class="wrapper why-choose">
                                <div class="svg-wrap"><?php echo $item['choose_image_left'];?></div>
                                <div class="choose-text-wrap">
                                <p class="sc3-wrapper__title"><?php echo $item['choose_capt_left'];?></p>
                                <p class="sc3-wrapper__desc"><?php echo $item['choose_text_left'];?></p>
                                </div>
                            </div>
                        </div>
	                <?php endforeach;?>
                </div>
            </div>
            <div class="col-lg-12 separate-block">
                <?php $choose_image = get_field('choose_main_image');?>
                <img src="<?php echo $choose_image['url'];?>" alt="" class="img-fluid">
                <div class="wrapper-right">
                    <?php
                        $choose_block = get_field('choose_block');
                    ?>
                    <div class="row">
                        <?php foreach ($choose_block as $item) : ?>
                        <div class="col-lg-6 col-6">
                            <div class="wrapper-right-item">
                                <p class="wrapper-right-item-title"><?php echo $item['choose_capt'];?></p>
                                <p class="wrapper-right-item-desc"><?php echo $item['choose_text'];?></p>
                            </div>
                        </div>
                        <?php endforeach;?>
                    </div>
                    <p class="wrapper-right-more-info">
                        <?php if($regions[$_SERVER['SERVER_NAME']]):?>
                            <a href="<?php echo str_replace('https://dpo-anta.ru', site_url(), get_field('more'));?>">Подробнее </a>
                        <?php else:?>
                            <a href="<?php echo get_field('more');?>">Подробнее </a>
                        <?php endif;?>
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="sc4">
    <div class="container">
        <h2 class="sc4-title">Отзывы наших учеников</h2>
        <div class="row">
            <div class="col-lg-12">
                <?php
                $args = array(
                    'post_type' => 'wpm-testimonial',
                    'posts_per_page' => -1,
                    'orderby' => 'rand',
                );
                $query = new WP_Query( $args );
                ?>
                <?php if (!empty($query->posts)) :?>
                    <div class="sc4-review-slider">
                        <?php foreach ($query->posts as $post) :?>
                            <?php
                            $attr = array(
                                'class'	=> 'img-fluid'
                            );
                            $testimonial_img = get_the_post_thumbnail( $post->ID, $size = 'full', $attr );
                            $testimonial_link = get_the_post_thumbnail_url( $post->ID );
                            ?>
                            <div class="sc4-review-slider__item">
                                <a data-fancybox="images1" href="<? echo $testimonial_link;?>"><?php echo $testimonial_img;?></a>
                            </div>
                        <?php endforeach;?>
                    </div>

                    <div class="sc4-review-slider-arrow-content">
                        <div class="sc4-review-slider__prev"><img src="<? echo get_template_directory_uri()?>/img/sc4-review-slider__prev.svg" alt="" class="img-fluid"></div>
                        <div class="sc4-review-slider__next"><img src="<? echo get_template_directory_uri()?>/img/sc4-review-slider__next.svg" alt="" class="img-fluid"></div>
                    </div>
                <?php endif;?>
            </div>
        </div>
    </div>
</section>
<section class="sc5">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="wrapper-top-form">
                    <p>Оставьте имя телефон или почту и <span>получите коммерческое предложение</span></p>
                    <?php echo do_shortcode('[contact-form-7 id="1191" title="Получить скидку (главная)"]');?>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="sc5">
    <div class="container">
        <h2 class="sc5-title">Наши лицензии</h2>

        <div class="row">
            <?php $licence = get_field('licence', 43); ?>
            <?php if($licence != NULL) : ?>
                <?php foreach($licence as $item) : ?>
                    <div class="col-lg-4 col-6">
                        <a data-fancybox="images2" href="<?php echo $item["licence_image"]["url"];?>">
                            <img src="<?php echo $item["licence_image"]["url"];?>" class="img-fluid">
                        </a>
                        <div class="sc5-bottom-content">
                            <div class="licence_name">
                                <p><?php echo $item['licence_name'];?></p>
                            </div>
                            <div class="licence_popup">
                                <img src="<? echo get_template_directory_uri()?>/img/zoom-ico.svg" alt="" class="img-fluid">
                            </div>
                        </div>
                    </div>
                <?php endforeach;?>
            <?php else:?>
                <div>
                    Лицензии будут представлены в ближайшее время
                </div>
            <?php endif;?>

        </div>


    </div>
</section>
<section class="sc6">
    <div class="container">
        <?php
            $clients = get_field('clients', 1183);
        ?>
        <h2 class="sc6-title">Наши клиенты</h2>
        <div class="row">
            <div class="col-lg-12">
                <div class="our-client-slider">
                    <?php foreach ($clients as $client) :?>
                        <?php foreach ($client as $client_elem) :?>
                            <div class="our-client-slider__item">
                                <img src="<? echo $client_elem["url"];?>" alt="<?php echo $client_elem["alt"];?>" class="img-fluid">
                            </div>
	                    <?php endforeach;?>
                    <?php endforeach;?>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="sc7" style="display: none">
    <div class="container">
        <div class="row">
            <div class="col-lg-6">
                <h2 class="sc7-title">Новости отрасли </h2>
                    <?php $args = array(
                            'post_type' => 'news', // Указываем наш новый тип записи
                            'posts_per_page' => 5,
                        );
                        $p = get_posts( $args );
                        foreach ( $p as $post ) {
                            setup_postdata( $post );
                            ?>
                            <a href="<?= the_permalink() ?>">
                                <div class="wrapper">
                                    <p class="sc7-wrapper__time"><?= the_date() ?></p>
                                    <p class="sc7-wrapper__text"><?= get_the_excerpt() ?></p>
                                </div>
                            </a>
                    <?php } wp_reset_postdata(); ?>
                <p class="view-all-news"><a href="<?php echo get_home_url( null, 'news/', 'http' ); ?>">Все новости <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24"><path fill="none" d="M0 0h24v24H0z"/><path d="M13.172 12l-4.95-4.95 1.414-1.414L16 12l-6.364 6.364-1.414-1.414z"/></svg></a></p>
            </div>
            <div class="col-lg-6">
                <h2 class="sc7-title">Полезные статьи</h2>
                    <?php $args = array(
                            'post_type' => 'article', // Указываем наш новый тип записи
                            'posts_per_page' => 5,
                        );
                        $p = get_posts( $args );
                        foreach ( $p as $post ) {
                            setup_postdata( $post );
                            ?>
                            <a href="<?= the_permalink() ?>">
                                <div class="wrapper">
                                    <p class="sc7-wrapper__time"><?= the_date() ?></p>
                                    <p class="sc7-wrapper__text"><?= get_the_excerpt() ?></p>
                                </div>
                            </a>
                    <?php } wp_reset_postdata(); ?>
                <p class="view-all-news"><a href="<?php echo get_home_url( null, 'article/', 'http' ); ?>">Все статьи <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24"><path fill="none" d="M0 0h24v24H0z"/><path d="M13.172 12l-4.95-4.95 1.414-1.414L16 12l-6.364 6.364-1.414-1.414z"/></svg></a></p>
            </div>
        </div>
    </div>
</section>
<section class="sc8 mainpage-block">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 order-lg-1 order-12">
            	<? $all_flatness = do_shortcode('[multiplication_flatness var="'.$all_options['my_flatness'].'"]');?>
                <h2 class="sc8-title">История компании</h2>
                <?php
                    $excerpt = get_the_excerpt();
                    echo parse_text($excerpt);
                ?>
                <p><a class="text-tab" href="#">Читать далее</a></p>
            </div>
            <div class="col-lg-6 order-lg-12 order-1">
                <div class="video-container">
                    <?php if($regions[$_SERVER['SERVER_NAME']]):?>
                    <div class="youtube" style="width: 100%; height: 100%"><?php echo str_replace('https://dpo-anta.ru', site_url(), get_field('youtube_video'));?></div>
                    <?php else:?>
                    <div class="youtube" style="width: 100%; height: 100%"><?php echo get_field('youtube_video');?></div>
                    <?php endif;?>
                </div>
            </div>
            <div class="col-lg-12 order-1 order-12 text-order" style="display: none;">
                <?php
                    if($regions[$_SERVER['SERVER_NAME']]['title']){
                        $city = $regions[$_SERVER['SERVER_NAME']]['title'];
                    } else {
	                    $city = 'в Омске';
                    }
                ?>
            	<p>Учебный центр <?= $city ?></p>
                    <?php the_content();?>
				</p>
            </div>
        </div>
    </div>
</section>
<section class="sc9">
    <div class="container">
        <div class="wrapper">
            <div class="row">
                <div class="col-lg-6"> <img src="<? echo get_template_directory_uri()?>/img/sc9-img1.png" alt="" class="img-fluid"> </div>
                <div class="col-lg-6">
                    <h2>Скидки до 20% <br><span>от 5 человек</span></h2>
                    <script data-b24-form="inline/69/ir1m4o" data-skip-moving="true">
                        (function(w,d,u){
                        var s=d.createElement('script');s.async=true;s.src=u+'?'+(Date.now()/180000|0);
                        var h=d.getElementsByTagName('script')[0];h.parentNode.insertBefore(s,h);
                        })(window,document,'https://cdn-ru.bitrix24.ru/b6368597/crm/form/loader_69.js');
                        </script>
                    <?php //echo do_shortcode('[contact-form-7 id="972" title="получить скидку"]')?>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="sc10">
    <div class="container">
        <div class="wrapper">
            <div class="row">
                <div class="col-lg-6">
                    <div class="wrapper-left">
                        <?php get_search_form(); ?>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="wrapper-right">
                        <p>Все курсы по алфавиту</p>
                        <div class="alphabite-content">
	                        <?php
	                        $posts = get_posts('post_type=kursi&orderby=title&numberposts=-1&order=ASC');

	                        foreach( $posts as $k => $post ){

		                        // первая буква
		                        $fl = get_first_letter( $post->post_title );
		                        $prev_fl = isset( $posts[ ($k-1) ] ) ? get_first_letter( $posts[ ($k-1) ]->post_title ) : '';
		                        if( $prev_fl !== $fl ){ ?>
                                    <div>
                                        <p><a href="/?s=<? echo $fl;?>"><? echo $fl;?></a></p>
                                    </div>
		                        <?}
	                        }
	                        wp_reset_postdata();
	                        ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php get_footer(); ?>