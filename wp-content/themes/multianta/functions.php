<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/wp-content/themes/multianta/inc/func-professions.php');
    function wptuts_scripts_custom()
{
    // Register the script like this for a theme:
    wp_register_script( 'previewYouTube', get_stylesheet_directory_uri()  . '/js/previewYouTube.js',true );
    // For either a plugin or a theme, you can then enqueue the script:
    wp_enqueue_script( 'previewYouTube' );
    // Register the script like this for a theme:
    wp_register_script( 'script', get_stylesheet_directory_uri()  . '/js/script.js',true );
    // For either a plugin or a theme, you can then enqueue the script:
    wp_enqueue_script( 'script' );
    // Register the script like this for a theme:
    wp_register_script( 'slickmin', get_stylesheet_directory_uri()  . '/js/slick.min.js',false );
    // For either a plugin or a theme, you can then enqueue the script:
    wp_enqueue_script( 'slickmin' );
    wp_register_script( 'maskedinput', get_stylesheet_directory_uri()  . '/js/jquery.maskedinput.min.js',false );
    // For either a plugin or a theme, you can then enqueue the script:
    wp_enqueue_script( 'maskedinput' );
    wp_register_script( 'fancybox', get_stylesheet_directory_uri()  . '/js/jquery.fancybox.min.js',false );
    // For either a plugin or a theme, you can then enqueue the script:
    wp_enqueue_script( 'fancybox' );
     wp_register_script( 'bootstrap', get_stylesheet_directory_uri()  . '/js/bootstrap.min.js',false );
    // For either a plugin or a theme, you can then enqueue the script:
    wp_enqueue_script( 'bootstrap' );
    wp_enqueue_style( 'normalize', get_stylesheet_directory_uri().'/css/normalize.css', false );
    wp_enqueue_style( 'slick', get_stylesheet_directory_uri().'/css/slick.css', false );
    wp_enqueue_style( 'jquery.fancybox.min', get_stylesheet_directory_uri().'/css/jquery.fancybox.min.css', false );
    wp_enqueue_style( 'hamburgers.min', get_stylesheet_directory_uri().'/css/hamburgers.min.css', false );
    wp_enqueue_style( 'bootstrap', get_stylesheet_directory_uri().'/css/bootstrap.min.css', false );
    wp_enqueue_style( 'style', get_stylesheet_directory_uri().'/style.css', false );
    //wp_enqueue_script('readmore', get_stylesheet_directory_uri() . '/js/readmore.min.js', false, '1', true);
	// Register the script like this for a theme:
	wp_register_script( 'privacy', get_stylesheet_directory_uri()  . '/js/privacy.js',true );
	// For either a plugin or a theme, you can then enqueue the script:
	wp_enqueue_script( 'privacy' );
	//
	wp_register_script( 'load', get_stylesheet_directory_uri()  . '/js/load.js',true );
	// For either a plugin or a theme, you can then enqueue the script:
	wp_enqueue_script( 'load' );
}
add_action( 'wp_enqueue_scripts', 'wptuts_scripts_custom' );

add_theme_support( 'post-thumbnails' );

// Склонятор
function multiplication_func_name( $atts ) {
    $argname = file_get_contents('http://morphos.io/api/inflect-name?name='.$atts['var'].'&_format=json');
    $name = json_decode($argname,true);
    $name= $name['cases'][1];
    return $name;
}
add_shortcode( 'multiplication_name', 'multiplication_func_name' );


function multiplication_func( $atts ) {
    $argsres = file_get_contents('http://morphos.io/api/inflect-geographical-name?name='.$atts['var'].'&_format=json');
    $res = json_decode($argsres,true);
    foreach ($res['cases'] as $key => $value) {
        //$all_res = $all_res.' '.$value;
    }
    $res = $res['cases'][5];
    return $res;
}
add_shortcode( 'multiplication', 'multiplication_func' );

function multiplication_func_flatness( $atts ) {
    $argsres = file_get_contents('http://morphos.io/api/inflect-geographical-name?name='.$atts['var'].'&_format=json');
    $res = json_decode($argsres,true);
    foreach ($res['cases'] as $key => $value) {
        $all_res = $all_res.' '.$value;
    }
    $res = $res['cases'][2];
    return $res;
}
add_shortcode( 'multiplication_flatness', 'multiplication_func_flatness' );


function add_new_taxonomies() {
/* создаем функцию с произвольным именем и вставляем
в неё register_taxonomy() */
    register_taxonomy('platform',
        array('page'),
        array(
            'hierarchical' => true,
            /* true - по типу рубрик, false - по типу меток,
            по умолчанию - false */
            'labels' => array(
                /* ярлыки, нужные при создании UI, можете
                не писать ничего, тогда будут использованы
                ярлыки по умолчанию */
                'name' => 'Категории стриниц',
                'singular_name' => 'Категория страниц',
                'search_items' =>  'Найти категория',
                'popular_items' => 'Популярные категории',
                'all_items' => 'Все категории',
                'parent_item' => null,
                'parent_item_colon' => null,
                'edit_item' => 'Редактировать категорию',
                'update_item' => 'Обновить категорию',
                'add_new_item' => 'Добавить новую категорию',
                'new_item_name' => 'Название новой категории',
                'separate_items_with_commas' => 'Разделяйте категории запятыми',
                'add_or_remove_items' => 'Добавить или удалить категорию',
                'choose_from_most_used' => 'Выбрать из наиболее часто используемых Категорий',
                'menu_name' => 'Категории'
            ),
            'public' => true,
            /* каждый может использовать таксономию, либо
            только администраторы, по умолчанию - true */
            'show_in_nav_menus' => true,
            /* добавить на страницу создания меню */
            'show_ui' => true,
            /* добавить интерфейс создания и редактирования */
            'show_tagcloud' => true,
            /* нужно ли разрешить облако тегов для этой таксономии */
            'update_count_callback' => '_update_post_term_count',
            /* callback-функция для обновления счетчика $object_type */
            'query_var' => true,
            /* разрешено ли использование query_var, также можно
            указать строку, которая будет использоваться в качестве
            него, по умолчанию - имя таксономии */
            'rewrite' => array(
            /* настройки URL пермалинков */
                'slug' => '', // ярлык
                'hierarchical' => true // разрешить вложенность

            ),
        )
    );
}
add_action( 'init', 'add_new_taxonomies', 0 );

// function add_new_taxonomies_metka() {
// /* создаем функцию с произвольным именем и вставляем
// в неё register_taxonomy() */
//     register_taxonomy('platforms',
//         array('page'),
//         array(
//             'hierarchical' => false,
//             /* true - по типу рубрик, false - по типу меток,
//             по умолчанию - false */
//             'labels' => array(
//                 /* ярлыки, нужные при создании UI, можете
//                 не писать ничего, тогда будут использованы
//                 ярлыки по умолчанию */
//                 'name' => 'Метки стриниц',
//                 'singular_name' => 'Метка страниц',
//                 'search_items' =>  'Найти метку',
//                 'popular_items' => 'Популярные метки',
//                 'all_items' => 'Все метки',
//                 'parent_item' => null,
//                 'parent_item_colon' => null,
//                 'edit_item' => 'Редактировать метку',
//                 'update_item' => 'Обновить метку',
//                 'add_new_item' => 'Добавить новую метку',
//                 'new_item_name' => 'Название новой метки',
//                 'separate_items_with_commas' => 'Разделяйте метки запятыми',
//                 'add_or_remove_items' => 'Добавить или удалить метку',
//                 'choose_from_most_used' => 'Выбрать из наиболее часто используемых меток',
//                 'menu_name' => 'метки'
//             ),
//             'public' => true,
//             /* каждый может использовать таксономию, либо
//             только администраторы, по умолчанию - true */
//             'show_in_nav_menus' => true,
//             /* добавить на страницу создания меню */
//             'show_ui' => true,
//             /* добавить интерфейс создания и редактирования */
//             'show_tagcloud' => true,
//             /* нужно ли разрешить облако тегов для этой таксономии */
//             'update_count_callback' => '_update_post_term_count',
//             /* callback-функция для обновления счетчика $object_type */
//             'query_var' => true,
//             /* разрешено ли использование query_var, также можно
//             указать строку, которая будет использоваться в качестве
//             него, по умолчанию - имя таксономии */
//             'rewrite' => array(
//             /* настройки URL пермалинков */
//                 'slug' => '', // ярлык
//                 'hierarchical' => true // разрешить вложенность

//             ),
//         )
//     );
// }
// add_action( 'init', 'add_new_taxonomies_metka', 0 );

function wpschool_register_taxonomy() {
    register_taxonomy_for_object_type( 'post_tag', 'page' );
}

function wpschool_display_taxonomy( $wp_query ) {
    // Добавление тегов
    if ( ( ( is_archive() || is_search() ) && $wp_query->get( 'tag' ) ) && ( ! $wp_query->get( 'post_type' ) ) ) {
    $wp_query->set( 'post_type', 'any' );
    }
}

add_action( 'pre_get_posts', 'wpschool_display_taxonomy' );
add_action( 'init', 'wpschool_register_taxonomy' );

function page_excerpt() {
add_post_type_support('page', array('excerpt'));
}
add_action('init', 'page_excerpt');

//add_filter('request', 'true_expanded_request_post_tags');

// смена запроса
add_filter('request', 'true_smenit_request', 1, 1 );

function true_smenit_request( $query ){

    $taxonomia_name = 'platform'; // укажите название таксономии здесь, это также могут быть рубрики category или метки post_tag

    // запросы для дочерних элементов будут отличаться, поэтому нам потребуется дополнительная проверка
    if( $query['page'] ) :
        $dochernia = true; // эту переменную задаём для себя, она нам потребуется дальше
        $urlyarlyk = $query['page']; // это ярлык данного термина/рубрики/метки
    else:
        $dochernia = false;
        $urlyarlyk = $query['name']; // как видите, здесь ярлык хранится в другой переменной запроса
    endif;


    $termin = get_term_by('slug', $urlyarlyk, $taxonomia_name); // получаем элемент таксономии по ярлыку

    if ( isset( $urlyarlyk ) && $termin && !is_wp_error( $termin )): // если такого элемента не существует, прекращаем выполнение кода

        // для страниц дочерних элементов код немного отличается
        if( $dochernia ) {
            unset( $query['page'] );
            $parent = $termin->parent;
            while( $parent ) {
                $parent_term = get_term( $parent, $taxonomia_name);
                $urlyarlyk = $parent_term->slug . '/' . $urlyarlyk; // нам нужно получить полный путь, состоящий из ярлыка текущего элемента и всех его родителей
                $parent = $parent_term->parent;
            }
        } else {
            unset($query['name']);
        }

        switch( $taxonomia_name ): // параметры запроса для рубрик и меток отличаются от других таксономий
            case 'category':{
                $query['category_name'] = $urlyarlyk;
                break;
            }
            case 'post_tag':{
                $query['tag'] = $urlyarlyk;
                break;
            }
            default:{
                $query[$taxonomia_name] = $urlyarlyk;
                break;
            }
        endswitch;

    endif;

    return $query;

}

// смена самой ссылки
add_filter( 'term_link', 'true_smena_permalink', 10, 3 );

function true_smena_permalink( $url, $term, $taxonomy ){

    $taxonomia_name = 'platform'; // название таксономии, тут всё понятно
    $taxonomia_slug = 'platform'; // ярлык таксономии - зависит от параметра rewrite, указанного при создании и может отличаться от названия,
    // как например таксономия меток это post_tag, а ярлык по умолчанию tag

    // выходим из функции, если указанного ярлыка таксономии нет в URL или если название таксономии не соответствует
    if ( strpos($url, $taxonomia_slug) === FALSE || $taxonomy != $taxonomia_name ) return $url;

    $url = str_replace('/' . $taxonomia_slug, '', $url); // если мы ещё тут, выполняем замену в URL

    return $url;
}

add_action( 'init', 'true_register_news' ); // Использовать функцию только внутри хука init

function true_register_news() {
    $labels = array(
        'name' => 'Новости',
        'singular_name' => 'Новость', // админ панель Добавить->Функцию
        'add_new' => 'Добавить новость',
        'add_new_item' => 'Добавить новую новость', // заголовок тега <title>
        'edit_item' => 'Редактировать новость',
        'new_item' => 'Новая новость',
        'all_items' => 'Все новости',
        'view_item' => 'Просмотр новостей на сайте',
        'search_items' => 'Искать новости',
        'not_found' =>  'Новости не найдены.',
        'not_found_in_trash' => 'В корзине нет новостей.',
        'menu_name' => 'Новости' // ссылка в меню в админке
    );
    $args = array(
        'labels' => $labels,
        'public' => true, // благодаря этому некоторые параметры можно пропустить
        'publicly_queryable' => true,
        'menu_icon' => 'dashicons-cart', // иконка корзины
        'rewrite' => array( 'slug' => 'news', 'with_front' => false ),
        'menu_position' => 5,
        'hierarchical' => true,
        'has_archive' => 'news',
        'supports' => array( 'title', 'editor', 'excerpt', 'thumbnail', 'comments'),
        'taxonomies' => array('category_new')
    );
    register_post_type('news',$args);
}

// Создаем новую таксономию для
add_action( 'init', 'create_category_new_taxonomies', 0 );

function create_category_new_taxonomies(){
  $labels = array(
    'name' => _x( 'Категории ', 'taxonomy general name' ),
    'singular_name' => _x( 'Категория ', 'taxonomy singular name' ),
    'search_items' =>  __( 'Найти категорию' ),
    'all_items' => __( 'Все категории' ),
    'parent_item' => __( 'Родительская категория' ),
    'parent_item_colon' => __( 'Родительская категория' ),
    'edit_item' => __( 'Родительская категория' ),
    'update_item' => __( 'Обновить катгорию' ),
    'add_new_item' => __( 'Добавить новую катгорию' ),
    'new_item_name' => __( 'Название новой категории' ),
    'menu_name' => __( 'Категории' ),
  );

  register_taxonomy('category_new', array('category_new'), array(
    'hierarchical' => true,
    'labels' => $labels,
    'show_ui' => true,
    'query_var' => true,
    'rewrite' => array( 'slug' => 'news' ),
  ));

}

add_action( 'init', 'true_register_article' ); // Использовать функцию только внутри хука init

function true_register_article() {
    $labels = array(
        'name' => 'Статьи',
        'singular_name' => 'Статья', // админ панель Добавить->Функцию
        'add_new' => 'Добавить статья',
        'add_new_item' => 'Добавить новую статья', // заголовок тега <title>
        'edit_item' => 'Редактировать статья',
        'new_item' => 'Новая статья',
        'all_items' => 'Все статьи',
        'view_item' => 'Просмотр статей на сайте',
        'search_items' => 'Искать статьи',
        'not_found' =>  'Статьи не найдены.',
        'not_found_in_trash' => 'В корзине нет статей.',
        'menu_name' => 'Статьи' // ссылка в меню в админке
    );
    $args = array(
        'labels' => $labels,
        'public' => true, // благодаря этому некоторые параметры можно пропустить
        'publicly_queryable' => true,
        'menu_icon' => 'dashicons-cart', // иконка корзины
        'rewrite' => array( 'slug' => 'article', 'with_front' => false ),
        'menu_position' => 5,
        'hierarchical' => true,
        'has_archive' => 'article',
        'supports' => array( 'title', 'editor', 'excerpt', 'thumbnail', 'comments'),
        'taxonomies' => array('post_tag')
    );
    register_post_type('article',$args);
}


add_action( 'init', 'true_register_slider' );

function true_register_slider() {
    $labels = array(
        'name' => 'Слайдер',
        'singular_name' => 'Слайдер', // админ панель Добавить->Функцию
        'add_new' => 'Добавить слайд',
        'add_new_item' => 'Добавить новый слайд', // заголовок тега <title>
        'edit_item' => 'Редактировать слайд',
        'new_item' => 'Новый слайд',
        'all_items' => 'Все слайды',
        'view_item' => 'Просмотреть слайд на сайте',
        'search_items' => 'Искать слайд',
        'not_found' =>  'Слайды не найдены.',
        'not_found_in_trash' => 'В корзине нет слайдов.',
        'menu_name' => 'Слайдер' // ссылка в меню в админке
    );
    $args = array(
        'labels' => $labels,
        'public' => true, // благодаря этому некоторые параметры можно пропустить
        'publicly_queryable' => true,
        'menu_icon' => 'dashicons-cart', // иконка корзины
        'rewrite' => array( 'slug' => 'slider', 'with_front' => false ),
        'menu_position' => 5,
        'hierarchical' => true,
        'has_archive' => 'slider',
        'supports' => array( 'title', 'editor', 'excerpt', 'thumbnail', 'comments'),
        'taxonomies' => array('category_slider')
    );
    register_post_type('slider',$args);
}

// Создаем новую таксономию для
add_action( 'init', 'create_category_slider_taxonomies', 0 );

function create_category_slider_taxonomies(){
  $labels = array(
    'name' => _x( 'Категории ', 'taxonomy general name' ),
    'singular_name' => _x( 'Категория ', 'taxonomy singular name' ),
    'search_items' =>  __( 'Найти категорию' ),
    'all_items' => __( 'Все категории' ),
    'parent_item' => __( 'Родительская категория' ),
    'parent_item_colon' => __( 'Родительская категория' ),
    'edit_item' => __( 'Родительская категория' ),
    'update_item' => __( 'Обновить катгорию' ),
    'add_new_item' => __( 'Добавить новую катгорию' ),
    'new_item_name' => __( 'Название новой категории' ),
    'menu_name' => __( 'Категории' ),
  );

  register_taxonomy('category_slider', array('category_slider'), array(
    'hierarchical' => true,
    'labels' => $labels,
    'show_ui' => true,
    'query_var' => true,
    'rewrite' => array( 'slug' => 'slider' ),
  ));

}



$true_page = 'myparameters.php'; // это часть URL страницы, рекомендую использовать строковое значение, т.к. в данном случае не будет зависимости от того, в какой файл вы всё это вставите

/*
 * Функция, добавляющая страницу в пункт меню Настройки
 */
function true_options() {
    global $true_page;
    add_menu_page( 'Параметры темы', 'Параметры темы', 1 , $true_page, 'true_option_page');
}
add_action('admin_menu', 'true_options');

/**
 * Возвратная функция (Callback)
 */
function true_option_page(){
    global $true_page;
    ?><div class="wrap">
        <h2>Дополнительные параметры сайта</h2>
        <form method="post" enctype="multipart/form-data" action="options.php">
            <?php
            settings_fields('true_options'); // меняем под себя только здесь (название настроек)
            do_settings_sections($true_page);
            ?>
            <p class="submit">
                <input type="submit" class="button-primary" value="<?php _e('Save Changes') ?>" />
            </p>
        </form>
    </div><?php
}

/*
 * Регистрируем настройки
 * Мои настройки будут храниться в базе под названием true_options (это также видно в предыдущей функции)
 */
function true_option_settings() {
    global $true_page;
    // Присваиваем функцию валидации ( true_validate_settings() ). Вы найдете её ниже
    register_setting( 'true_options', 'true_options', 'true_validate_settings' ); // true_options

    // Добавляем секцию
    add_settings_section( 'true_section_1', 'Контакты сайта', '', $true_page );

    // Создадим текстовое поле в первой секции
    $true_field_params = array(
        'type'      => 'text', // тип
        'id'        => 'my_gorod',
        'break_'      => 'Пример обычного текстового поля.', // описание
        'label_for' => 'my_gorod' // позволяет сделать название настройки лейблом (если не понимаете, что это, можете не использовать), по идее должно быть одинаковым с параметром id
    );
    add_settings_field( 'my_gorod_field', 'Город', 'true_option_display_settings', $true_page, 'true_section_1', $true_field_params );

    $true_field_params = array(
        'type'      => 'text', // тип
        'id'        => 'my_flatness',
        'break_'      => 'Пример обычного текстового поля.', // описание
        'label_for' => 'my_flatness' // позволяет сделать название настройки лейблом (если не понимаете, что это, можете не использовать), по идее должно быть одинаковым с параметром id
    );
    add_settings_field( 'my_flatness_field', 'Область', 'true_option_display_settings', $true_page, 'true_section_1', $true_field_params );


    $true_field_params = array(
        'type'      => 'text', // тип
        'id'        => 'my_telephone',
        'break_'      => 'Пример обычного текстового поля.', // описание
        'label_for' => 'my_telephone' // позволяет сделать название настройки лейблом (если не понимаете, что это, можете не использовать), по идее должно быть одинаковым с параметром id
    );
    add_settings_field( 'my_text_field', 'Телефон', 'true_option_display_settings', $true_page, 'true_section_1', $true_field_params );

    $true_field_params = array(
        'type'      => 'text', // тип
        'id'        => 'my_address',
        'break_'      => 'Пример обычного текстового поля.', // описание
        'label_for' => 'my_address' // позволяет сделать название настройки лейблом (если не понимаете, что это, можете не использовать), по идее должно быть одинаковым с параметром id
    );
    add_settings_field( 'my_address_field', 'Адрес', 'true_option_display_settings', $true_page, 'true_section_1', $true_field_params );

    $true_field_params = array(
        'type'      => 'text', // тип
        'id'        => 'my_email',
        'break_'      => 'Пример обычного текстового поля.', // описание
        'label_for' => 'my_email' // позволяет сделать название настройки лейблом (если не понимаете, что это, можете не использовать), по идее должно быть одинаковым с параметром id
    );
    add_settings_field( 'my_email_field', 'Email', 'true_option_display_settings', $true_page, 'true_section_1', $true_field_params );

	$true_field_params = array(
		'type'      => 'text', // тип
		'id'        => 'my_wa',
		'break_'      => 'Введите значение', // описание
		'label_for' => 'my_wa' // позволяет сделать название настройки лейблом (если не понимаете, что это, можете не использовать), по идее должно быть одинаковым с параметром id
	);
	add_settings_field( 'my_wa_field', 'Whatsapp', 'true_option_display_settings', $true_page, 'true_section_1', $true_field_params );

	$true_field_params = array(
		'type'      => 'text', // тип
		'id'        => 'my_inst',
		'break_'      => 'Введите значение', // описание
		'label_for' => 'my_inst' // позволяет сделать название настройки лейблом (если не понимаете, что это, можете не использовать), по идее должно быть одинаковым с параметром id
	);
	add_settings_field( 'my_inst_field', 'Instagram', 'true_option_display_settings', $true_page, 'true_section_1', $true_field_params );

	$true_field_params = array(
		'type'      => 'text', // тип
		'id'        => 'my_telegram',
		'break_'      => 'Введите значение', // описание
		'label_for' => 'my_telegram' // позволяет сделать название настройки лейблом (если не понимаете, что это, можете не использовать), по идее должно быть одинаковым с параметром id
	);
	add_settings_field( 'my_telegram_field', 'Telegram', 'true_option_display_settings', $true_page, 'true_section_1', $true_field_params );


	$true_field_params = array(
		'type'      => 'text', // тип
		'id'        => 'my_days',
		'break_'      => 'Введите значение', // описание
		'label_for' => 'my_days' // позволяет сделать название настройки лейблом (если не понимаете, что это, можете не использовать), по идее должно быть одинаковым с параметром id
	);
	add_settings_field( 'my_days_field', 'Режим работы', 'true_option_display_settings', $true_page, 'true_section_1', $true_field_params );

	$true_field_params = array(
		'type'      => 'text', // тип
		'id'        => 'my_vacations',
		'break_'      => 'Введите значение', // описание
		'label_for' => 'my_vacations' // позволяет сделать название настройки лейблом (если не понимаете, что это, можете не использовать), по идее должно быть одинаковым с параметром id
	);
	add_settings_field( 'my_vacations_field', 'Выходные', 'true_option_display_settings', $true_page, 'true_section_1', $true_field_params );


	$true_field_params = array(
        'type'      => 'textarea', // тип
        'id'        => 'my_cart',
        'break_'      => 'Пример обычного текстового поля.', // описание
        'label_for' => 'my_cart' // позволяет сделать название настройки лейблом (если не понимаете, что это, можете не использовать), по идее должно быть одинаковым с параметром id
    );
    add_settings_field( 'my_cart_field', 'Карта', 'true_option_display_settings', $true_page, 'true_section_1', $true_field_params );

    $true_field_params = array(
        'type'      => 'textarea', // тип
        'id'        => 'my_text_seo',
        'break_'      => '', // описание
        'label_for' => 'my_text_seo' // позволяет сделать название настройки лейблом (если не понимаете, что это, можете не использовать), по идее должно быть одинаковым с параметром id
    );
    add_settings_field( 'my_text_seo_field', 'SEO текст', 'true_option_display_settings', $true_page, 'true_section_1', $true_field_params );

    // Создадим textarea в первой секции
    $true_field_params = array(
        'type'      => 'textarea',
        'id'        => 'my_textarea',
        'break_'      => 'Пример большого текстового поля.'
    );
    add_settings_field( 'my_textarea_field', 'Большое текстовое поле', 'true_option_display_settings', $true_page, 'true_section_1', $true_field_params );
}
add_action( 'admin_init', 'true_option_settings' );

/*
 * Функция отображения полей ввода
 * Здесь задаётся HTML и PHP, выводящий поля
 */
function true_option_display_settings($args) {
    extract( $args );

    $option_name = 'true_options';

    $o = get_option( $option_name );

    switch ( $type ) {
        case 'text':
            $o[$id] = esc_attr( stripslashes($o[$id]) );
            echo "<input class='regular-text' type='text' id='$id' name='" . $option_name . "[$id]' value='$o[$id]' />";
            echo ($break_ != '') ? "<br /><span class='break_ription'>$break_</span>" : "";
        break;
        case 'textarea':
            $o[$id] = esc_attr( stripslashes($o[$id]) );
            echo "<textarea class='code large-text' cols='50' rows='10' type='text' id='$id' name='" . $option_name . "[$id]'>$o[$id]</textarea>";
            echo ($break_ != '') ? "<br /><span class='break_ription'>$break_</span>" : "";
        break;
    }
}

/*
 * Функция проверки правильности вводимых полей
 */
function true_validate_settings($input) {
    foreach($input as $k => $v) {
        $valid_input[$k] = trim($v);

        /* Вы можете включить в эту функцию различные проверки значений, например
        if(! задаем условие ) { // если не выполняется
            $valid_input[$k] = ''; // тогда присваиваем значению пустую строку
        }
        */
    }
    return $valid_input;
}


// add_filter('category_link', function($a){
//     return str_replace( 'category_new/', '', $a );
// }, 99 );



add_action( 'after_setup_theme', 'pbs_setuplogo' );
    function pbs_setuplogo() {
        add_theme_support( 'custom-logo', array(
            'width' => 250,
            'height' => 100,
            'flex-width' => true,
            'flex-height' => true,
            'header-text' => array('site-title', 'site-break_ription')
    ));
}

/**
Custom logo.
*/
function nanima_logo() {
    $custom_logo_id = get_theme_mod( 'custom_logo' );
    if(is_home()){
        $html = wp_get_attachment_image( $custom_logo_id, 'full', false, array(
        'class' => 'custom-logo img-fluid',
        'itemprop' => 'url image',
        'alt' => get_bloginfo('name')
        ) );
    } else {
        $html = sprintf( '<a href="%1$s" class="custom-logo-link" rel="home" title="'. get_bloginfo('name') .'" itemprop="url">%2$s</a>',
        esc_url( home_url( '/' ) ),
        wp_get_attachment_image( $custom_logo_id, 'full', false, array(
        'class' => 'custom-logo img-fluid',
        'itemprop' => 'url image',
        'alt' => get_bloginfo('name'))
        ) );
    }
    return $html;
}
add_filter( 'get_custom_logo', 'nanima_logo' );


// Подхватывает(hook) инициацию виджета и запускает нашу функцию
add_action( 'widgets_init', 'add_Widget_Support' );

// Регистрирует новое навигационное меню
//function add_Main_Nav() {
// register_nav_menu('header-menu',__( 'Header Menu' ));
//}
// Подхватывает (hook) init хук-событие, запускает функцию нашего навигационного меню
//add_action( 'init', 'add_Main_Nav' );

register_nav_menus( array(
    'header-menu' => 'Header Menu',
    'start-menu' => 'Start Menu',
    'footer_menu_1' => 'Footer_menu_1',
    'footer_menu_2' => 'Footer_menu_2',
));

require_once ('functions/wp_bootstrap_navwalker.php');
require_once ('functions/breadcrumb.php');
require_once ('functions/term-image.php');
/*отсюда*/
//add_filter(
//    'nav_menu_css_class',
//    function($classes, $item) {
//        /*
//         * Переменная $classes содержит
//         * Array(
//         *   [1] => menu-item
//         *   [2] => menu-item-type-post_type
//         *   [3] => menu-item-object-page
//         *   [4] => menu-item-1911
//         * )
//         */
//        // переопределяем переменную $classes
//        $classes = [1 => 'nav-item'];
//        if ($item->current) {
//            // добавляем класс active для текущей страницы
//            $classes[2] = 'active';
//        }
//        return $classes;
//    },
//    10,
//    2
//);

function mtc_nav_menu_css_classes( $classes, $item, $args, $depth ) {
	if ( in_array( $args->theme_location, [ 'main_menu_top' ] ) ) {
		$classes = [
			'active'
		];
	}
	return $classes;
}

add_filter( 'nav_menu_css_class', 'mtc_nav_menu_css_classes', 10, 4 );

/*add_filter(
    'nav_menu_link_attributes',
    function($atts, $item, $args, $depth) {
        // добавляем класс nav-link для элементов <a>
        $atts['class'] = 'nav-link';
        return $atts;
    },
    10,
    4
);*/

function onwp_search_results_per_page_func($query) {
 // запрос на странице поиска
 if (!is_admin() && $query->is_main_query() && $query->is_search()) {
 $query->set('posts_per_page', 250);
 }
    return $query;
 }
add_action('pre_get_posts', 'onwp_search_results_per_page_func');

add_filter( 'get_search_form', 'my_search_form' );
function my_search_form( $form ) {

    $form = '
    <form role="search" method="get" id="searchform" action="' . home_url( '/' ) . '" >
        <p>Поиск по специальностям</p>
        <div class="input-content">
            <input type="hidden" value="1" name="s" />
            <input type="hidden" value="kursi" name="post_type" />
            <div class="input-content-input">
                <input type="text" class="sc10-wrapper-input" placeholder="Введите название курса..."value="' . get_search_query() . '" name="s" id="s" /> 
            </div>
            <div class="input-content-button">
                <button type="submit" id="searchsubmit" class="sc10-wrapper-submit">
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24">
                        <path fill="none" d="M0 0h24v24H0z" />
                        <path d="M18.031 16.617l4.283 4.282-1.415 1.415-4.282-4.283A8.96 8.96 0 0 1 11 20c-4.968 0-9-4.032-9-9s4.032-9 9-9 9 4.032 9 9a8.96 8.96 0 0 1-1.969 5.617zm-2.006-.742A6.977 6.977 0 0 0 18 11c0-3.868-3.133-7-7-7-3.868 0-7 3.132-7 7 0 3.867 3.132 7 7 7a6.977 6.977 0 0 0 4.875-1.975l.15-.15z" /> </svg> Найти</button>
            </div>
        </div>
    </form>';

    return $form;
}

// allow SVG uploads
add_filter('upload_mimes', 'custom_upload_mimes');
function custom_upload_mimes ( $existing_mimes=array() ) {
  $existing_mimes['svg'] = 'image/svg+xml';
  return $existing_mimes;
}
function fix_svg() {
    echo '<style type="text/css">
          .attachment-266x266, .thumbnail img {
               width: 100% !important;
               height: auto !important;
          }
          </style>';
 }
 add_action('admin_head', 'fix_svg');


 /**
 * Функция возвращает алфавитный указатель указанной категории
 * @param int $cat - id категори
**/

function get_category_alphabet($cat = 1) {
    $lib = range( 'a', 'z' );

    $posts = get_posts([
        'category' => $cat,
        'numberposts' => 999,
    ]);

    foreach ($posts as $post) {
        $letter = strtoupper(mb_substr($post->post_title, 0, 1));

        if (isset($lib[$letter])) {
            $lib[$letter] .= '<li><a href="' . get_permalink($post->ID) . '">' . $post->post_title . '</a>';
        }
    }

    $out = '';

    foreach ($lib as $letter => $value) {
        if (!$value) {
            continue;
        }

        $out .= '
        <div class="wrapper-letter">
            <span class="letter">' . $letter . '</span>
            <ul>' . $value . '</ul>
        </div>            
        ';
    }

    return $out;
}

// Custom Fields Start
// Подключение функции мета-блока my_extra_fields
//add_action('admin_init', 'my_extra_fields', 1);
function my_extra_fields() {
    add_meta_box( 'extra_fields', 'Дополнительные поля', 'extra_fields_box_page_func', 'page', 'normal', 'high'  );
}
// HTML код блока для статических страниц
function extra_fields_box_page_func( $post ){
?>
<p>
&nbsp;
<em>Заголовк слайда:</em>
<label><input type="text" name="extra[hero_title]" value="<?php echo get_post_meta($post->ID, 'hero_title', 1); ?>" style="width:100%" /></label>
</p>
<p>
&nbsp;
<em>Подзоголовок слайда:</em>
<label><input type="text" name="extra[hero_desc]" value="<?php echo get_post_meta($post->ID, 'hero_desc', 1); ?>" style="width:100%" /></label>
</p>
<p>
&nbsp;
<em>Цена:</em>
<label><input type="text" name="extra[price]" value="<?php echo get_post_meta($post->ID, 'price', 1); ?>" style="width:100%" /></label>
</p>
<p>
&nbsp;
<em>Разряд 1:</em>
<textarea type="text" name="extra[break_1]" style="width:100%;height:50px;"><?php echo get_post_meta($post->ID, 'break_1', 1); ?></textarea>
</p>
<p>
&nbsp;
<em>Разряд 2:</em>
<textarea type="text" name="extra[break_2]" style="width:100%;height:50px;"><?php echo get_post_meta($post->ID, 'break_2', 1); ?></textarea>
</p>
<p>
&nbsp;
<em>Разряд 3:</em>
<textarea type="text" name="extra[break_3]" style="width:100%;height:50px;"><?php echo get_post_meta($post->ID, 'break_3', 1); ?></textarea>
</p>
<p>
&nbsp;
<em>Разряд 4:</em>
<textarea type="text" name="extra[break_4]" style="width:100%;height:50px;"><?php echo get_post_meta($post->ID, 'break_4', 1); ?></textarea>
</p>
<p>
&nbsp;
<em>Разряд 5:</em>
<textarea type="text" name="extra[break_5]" style="width:100%;height:50px;"><?php echo get_post_meta($post->ID, 'break_5', 1); ?></textarea>
</p>
<p>
&nbsp;
<em>Разряд 6:</em>
<textarea type="text" name="extra[break_6]" style="width:100%;height:50px;"><?php echo get_post_meta($post->ID, 'break_6', 1); ?></textarea>
</p>
<p>
&nbsp;
<em>Разряд 7:</em>
<textarea type="text" name="extra[break_7]" style="width:100%;height:50px;"><?php echo get_post_meta($post->ID, 'break_7', 1); ?></textarea>
</p>
<p>
&nbsp;
<em>Разряд 8:</em>
<textarea type="text" name="extra[break_8]" style="width:100%;height:50px;"><?php echo get_post_meta($post->ID, 'break_8', 1); ?></textarea>
</p>
<input type="hidden" name="extra_fields_nonce" value="<?php echo wp_create_nonce(__FILE__); ?>" />
<?php
}
// Активация обновления полей при сохранении
add_action('save_post', 'my_extra_fields_update', 0);
// Сохранение данных при сохранении страницы
function my_extra_fields_update( $post_id ){
    // базовая проверка
    if (
           empty( $_POST['extra'] )
        || ! wp_verify_nonce( $_POST['extra_fields_nonce'], __FILE__ )
        || wp_is_post_autosave( $post_id )
        || wp_is_post_revision( $post_id )
    )
        return false;

    // Все ОК! Теперь, нужно сохранить/удалить данные
    $_POST['extra'] = array_map( 'sanitize_text_field', $_POST['extra'] );
    foreach( $_POST['extra'] as $key => $value ){
        if( empty($value) ){
            delete_post_meta( $post_id, $key ); // удаляем поле если значение пустое
            continue;
        }

        update_post_meta( $post_id, $key, $value ); // add_post_meta() работает автоматически
    }

    return $post_id;
}
// функция add_post_meta() срабатывает автоматически
// end Custom Fields
function get_custom_field($metakey, $term_id){
    global $wpdb;
    $custom_field = $wpdb->get_row(
	    "SELECT `meta_value` FROM wp_termmeta WHERE `meta_key` =	'$metakey' AND `term_id` = '$term_id'"
    );
    return $custom_field;
}
//для цен
function get_custom_fields_for_prices($metakey){
	global $wpdb;
	$custom_fields = $wpdb->get_results(
		"SELECT DISTINCT `meta_value` FROM wp_postmeta WHERE `meta_key` =	'$metakey'"
	);
	return $custom_fields;
}
/*отсюда*/
function get_meta_for_SEO() {
    if(!is_admin()){
	    include 'regions.php';
	    return $regions[$_SERVER['SERVER_NAME']]['title'];
    }

}
function get_meta_title_for_SEO(){
    if(!is_admin()){
        include 'regions.php';
	    return $regions[$_SERVER['SERVER_NAME']]['modal'];
    }
}

//дескрипшен
add_filter( 'wpseo_metadesc', 'joe_remove_yoast_meta' );
function joe_remove_yoast_meta( $filter ){
	// Добавьте сюда свои условия

	global $post;
	$term_object = get_queried_object();

	//дескрипшен для профессий
	if(is_page_template('working.php') || is_page_template('single-kursi.php')){
	    //по умолчанию работает автогенерация
		if($filter == $post->post_title){
			if(strpos($_SERVER['REQUEST_URI'], 'povyshenie-kvalifikacii')){
				$toponim = get_toponim($post->post_title);
				$region = get_meta_for_SEO();
				$filter = 'Профессиональная переподготовка и повышение квалификации по курсу '.mb_strtolower($toponim).$region. '. Посмотреть цены, программу обучения и оставить заявку можно на нашем сайте.';
            } else {
				$toponim = get_toponim($post->post_title);
				$region = get_meta_for_SEO();
				$filter = 'Пройти дистанционное обучение на '.mb_strtolower($toponim).$region. ' вы можете в центре ДПО АНТА. Посмотреть цены, программу обучения и оставить заявку можно на нашем сайте.';
            }

        } else {
		    //если задано в админке - выводим из админки
			$region = get_meta_title_for_SEO();
			$region = mb_substr( $region, 1);
			$region_e = get_meta_for_SEO();
			$region_e = mb_substr( $region_e, 1);

			/*if(strpos($filter, '[toponim]')){
				$filter = str_replace('[toponim]', "&nbsp;$region", $filter);
				return $filter;
			}
			if(strpos($filter, '[toponime]')){
				$filter = str_replace('[toponime]', "&nbsp;$region_e", $filter);
				return $filter;
			}*/
			if($region){
				return $filter.' '.$region;
			} else {
				return $filter;
			}
		    //return $filter; //вставить [toponim] и [toponime] //если не идет ставим $region
        }

    } elseif ($term_object->taxonomy == "platform" || $term_object->taxonomy == "kursicat"){ //для категорий

	    $region = get_meta_for_SEO();
		if($region){
		    //если есть регион то автогенерация
			if(strpos($filter, '[toponime]')){
				$filter = str_replace('[toponime]', $region, $filter);
				return $filter;
			}
			$filter = $term_object->name.$region. ' по низкой цене'; //попробовать сделать так же, если по умолчанию то автогенерация, если админка то админка
        } else {
		    //если нет региона то выводим из админки, автогенерация не работает, только админка
			if(strpos($filter, '[toponime]')){
				$filter = str_replace('[toponime]', $region, $filter);
				return $filter;
			}
			return $filter;
        }
		//
    } else { //для остальных страниц
		$region = get_meta_title_for_SEO();
		if($region){
			if($filter == $post->post_title){ //если есть регион и в админке не задали мета
			    $filter = $post->post_title.' -'.$region;
			} else {
			    //если в админке задали мета. + к ним рисую регион
			    return $filter.' -'.$region.'.'; //добавить дефис
            }
		} else {
		    //если нет региона то выводим из адмики
			return $filter.'.';
        }
    }

	if( is_archive() ){ //archive-news нет дескрипшена, т.к. seo-yoast его не видит вообще. //нужно будет переделать блок новостей
	    if($term_object->taxonomy != "platform"){ //для остальных категорий, но не для категорий профессйи
		    $region = get_meta_for_SEO();
		    $cat_name = get_queried_object()->name;
		    //$filter = 'Категория '.mb_strtolower($cat_name).$region;
		    $filter = 'Обучение на '.mb_strtolower($cat_name).$region. ' по низкой цене';
        } elseif($term_object->has_archive == "news"){ //для категории новости
		    $region = get_meta_title_for_SEO();
		    if($region){ //если есть регион
			    $filter = 'Новости -'.$region;
		    } else { //если нету региона
			    $filter = 'Новости';
		    }
	    } else {
	        //в остальных случаях когда отрабатывает archive.php
	        return $filter;
        }

	}
	return $filter;
}

//тайтл
add_filter('wpseo_title', 'new_title_filter', 10 );
function new_title_filter($title) {
	global $post;
	$term_object = get_queried_object();
	//для профессий
	if(is_page_template('working.php') || is_page_template('single-kursi.php')){
	    //die('123');
	    //если в админке по умолчанию, то автогенерация
		if($title == $post->post_title){
            if(strpos($_SERVER['REQUEST_URI'], 'povyshenie-kvalifikacii')){
	            $toponim = get_toponim($post->post_title);
	            $region = get_meta_for_SEO();
	            if($region) {
	                $filter = 'Повышение квалификации по курсу '.mb_strtolower($toponim).$region. ' дистанционно';
	            } else {
		            $filter = 'Повышение квалификации по курсу '.mb_strtolower($toponim).  ' дистанционно';
                }
            } else {
	            $toponim = get_toponim($post->post_title);
	            $region = get_meta_for_SEO();
	            $filter = 'Обучение на '.mb_strtolower($toponim).$region. ' на курсах дистанционно';
            }

        } else { //если задано в админке, выводим из админки
		    //сюда воткнуть [toponim] или [toponime]
			$region = get_meta_title_for_SEO();
			$region = mb_substr( $region, 1);
			$region_e = get_meta_for_SEO();
			$region_e = mb_substr( $region_e, 1);

			if(strpos($title, '[toponim]')){
				$title = str_replace('[toponim]', "&nbsp;$region", $title);
				return $title;
			}
			if(strpos($title, '[toponime]')){
			    $title = str_replace('[toponime]', "&nbsp;$region_e", $title);
				return $title;
            }
			if($region){
			    return $title.' '.$region;
			} else {
				return $title;
            }
        }

	} elseif(!is_archive()) {
	    //если не архивная страница (например главная)
		$region = get_meta_title_for_SEO();
		if($region){
			if($title == $post->post_title){
			    $filter = $post->post_title.' -'.$region; //сюда воткнуть [toponim] или [toponime]
			} else {
				if($region){
					return $title .' -'.$region;
				} else {
					return $title;
				}
            }
		} elseif(!is_archive()) {
			//$filter = $post->post_title;
            $filter = $title;
		}
	}
	if( is_archive() ){
	    //если категория для профессий
		if($term_object->taxonomy == "platform" || $term_object->taxonomy == "kursicat"){

            $region = get_meta_for_SEO();
		    if($region){
			    //если есть регион
			    //$filter = $term_object->name.' -'.$region; //сюда воткнуть [toponim] или [toponime]
                //$filter = str_replace('[toponim]' , $region, $title);
                $filter = str_replace('[toponime]' , $region, $title);
            } else {
			    //если нет региона
			    $filter = str_replace('[toponime]' , $region, $title);
		        //return $title;
			    //$filter = $term_object->name;
            }

        } elseif($term_object->has_archive == "news"){
		    //для новостей
			$region = get_meta_title_for_SEO();
			if($region){
			    //если регион
			    $filter = 'Новости -'.$region;
			} else {
			    //если нет региона
				$filter = 'Новости';
            }
        } else {
		    //в остальных случаях выводим из админки //для отдельной новости вывести тайтл
			return false;
        }
	}

	return $filter;
}



function parse_content(){
	$region = get_meta_title_for_SEO();
	$region = mb_substr( $region, 1);
	$region_e = get_meta_for_SEO();
	$region_e = mb_substr( $region_e, 1);
	$content = get_the_content();
	/*if($region || $region_e){
	    if(strpos($content, '[toponim]')){
	        $toponim = str_replace('[toponim]', "$region", $content);
	    } else {
		    $toponim = str_replace('[toponime]', "$region_e", $content);
        }
	} else {
		if(strpos($content, '[toponime]')){
			$toponim = str_replace('[toponime]', '&nbsp;в Новосибирскe', $content);
		} else {
			$toponim = str_replace('[toponim]', "&nbsp;Новосибирск", $content);
		}

	}
	return $toponim;*/
	if($region || $region_e){
	    $new_content = str_replace('[toponim]', "&nbsp;$region", $content);
	    $new_content_final = str_replace('[toponime]', "&nbsp;$region_e", $new_content);
	} else {
		$new_content = str_replace('[toponim]', "&nbsp;Омск", $content);
		$new_content_final = str_replace('[toponime]', "&nbsp;в Омске", $new_content);
    }
	return $new_content_final;
}
add_filter( 'the_content', 'parse_content' );

function parse_all_content($content){
	$region = get_meta_title_for_SEO();
	$region = mb_substr( $region, 1);
	$region_e = get_meta_for_SEO();
	$region_e = mb_substr( $region_e, 1);
	if($region || $region_e){
		$new_content = str_replace('[toponim]', "&nbsp;$region", $content);
		$new_content_final = str_replace('[toponime]', "&nbsp;$region_e", $new_content);
	} else {
		$new_content = str_replace('[toponim]', "&nbsp;Омск", $content);
		$new_content_final = str_replace('[toponime]', "&nbsp;в Омске", $new_content);
	}
	return $new_content_final;
}

function parse_text($text){
	$region = get_meta_for_SEO();
	$region_e = get_meta_for_SEO();
	if($region || $region_e) {
		if ( strpos( $text, '[toponim]' ) ) {
			$text = str_replace( '[toponim]', "$region", $text );
		} else {
			$text = str_replace( '[toponime]', "$region_e", $text );
		}
	} else {
		if(strpos($text, '[toponime]')){
			$text = str_replace('[toponime]', 'в Омске', $text);
		} else {
			$text = str_replace('[toponim]', "Омск", $text);
		}
    }
    return $text;
}
add_filter( 'init', 'parse_text' );

/*custom post types kursi*/

add_action( 'init', 'register_kursi_post_type' );
function register_kursi_post_type() {

	register_taxonomy( 'kursicat', [ 'kursi' ], [
		'label'                 => 'Раздел курса', // определяется параметром $labels->name
		'labels'                => array(
			'name'              => 'Разделы курсов',
			'singular_name'     => 'Раздел курса',
			'search_items'      => 'Искать Раздел курса',
			'all_items'         => 'Все Разделы курсов',
			'parent_item'       => 'Родит. раздел курса',
			'parent_item_colon' => 'Родит. раздел курса:',
			'edit_item'         => 'Ред. Раздел курса',
			'update_item'       => 'Обновить Раздел курса',
			'add_new_item'      => 'Добавить Раздел курса',
			'new_item_name'     => 'Новый Раздел курса',
			'menu_name'         => 'Разделы курсов',
		),
		'description'           => 'Рубрики для раздела курсов', // описание таксономии
		'public'                => true,
		'show_in_nav_menus'     => true, // равен аргументу public
		'show_ui'               => true, // равен аргументу public
		'show_tagcloud'         => false, // равен аргументу show_ui
		'hierarchical'          => true,
		'rewrite'               => true,
		//'rewrite'               => array('slug'=>'/', 'hierarchical'=>false, 'with_front'=>false, 'feed'=>false ),
		'show_admin_column'     => true, // Позволить или нет авто-создание колонки таксономии в таблице ассоциированного типа записи. (с версии 3.5)
	] );


	register_post_type( 'kursi', [
		'label'               => 'Профессии',
		'labels'              => array(
			'name'          => 'Курсы',
			'singular_name' => 'Профессия',
			'menu_name'     => 'Профессии',
			'all_items'     => 'Все записи',
			'add_new'       => 'Добавить запись',
			'add_new_item'  => 'Добавить новую запись',
			'edit'          => 'Редактировать',
			'edit_item'     => 'Редактировать запись',
			'new_item'      => 'Новая запись',
		),
		'description'         => '',
		'public'              => true,
		'publicly_queryable'  => true,
		'show_ui'             => true,
		'show_in_rest'        => false,
		'rest_base'           => '',
		'show_in_menu'        => true,
		'exclude_from_search' => false,
		'capability_type'     => 'post',
		'map_meta_cap'        => true,
		'menu_icon'           => 'dashicons-businessman',
		'hierarchical'        => false,
		//'rewrite'             => array( 'slug'=>'kursi/%kursicat%', 'with_front'=>false, 'pages'=>false, 'feeds'=>false, 'feed'=>false ),
		'rewrite'             => array( 'slug'=>'kursi', 'with_front'=>false, 'pages'=>false, 'feeds'=>false, 'feed'=>false ),
		'has_archive'         => 'kursicat',
		'query_var'           => true,
		'supports'            => array( 'title', 'editor' ),
		'taxonomies'          => array( 'kursicat' ),
	] );

}
/*
 * Функция создает дубликат поста в виде черновика и редиректит на его страницу редактирования
 */
function true_duplicate_post_as_draft(){
	global $wpdb;
	if (! ( isset( $_GET['post']) || isset( $_POST['post'])  || ( isset($_REQUEST['action']) && 'true_duplicate_post_as_draft' == $_REQUEST['action'] ) ) ) {
		wp_die('Нечего дублировать!');
	}

	/*
	 * получаем ID оригинального поста
	 */
	$post_id = (isset($_GET['post']) ? $_GET['post'] : $_POST['post']);
	/*
	 * а затем и все его данные
	 */
	$post = get_post( $post_id );

	/*
	 * если вы не хотите, чтобы текущий автор был автором нового поста
	 * тогда замените следующие две строчки на: $new_post_author = $post->post_author;
	 * при замене этих строк автор будет копироваться из оригинального поста
	 */
	$current_user = wp_get_current_user();
	$new_post_author = $current_user->ID;

	/*
	 * если пост существует, создаем его дубликат
	 */
	if (isset( $post ) && $post != null) {

		/*
		 * массив данных нового поста
		 */
		$args = array(
			'comment_status' => $post->comment_status,
			'ping_status'    => $post->ping_status,
			'post_author'    => $new_post_author,
			'post_content'   => $post->post_content,
			'post_excerpt'   => $post->post_excerpt,
			'post_name'      => $post->post_name,
			'post_parent'    => $post->post_parent,
			'post_password'  => $post->post_password,
			'post_status'    => 'draft', // черновик, если хотите сразу публиковать - замените на publish
			'post_title'     => $post->post_title,
			'post_type'      => $post->post_type,
			'to_ping'        => $post->to_ping,
			'menu_order'     => $post->menu_order
		);

		/*
		 * создаем пост при помощи функции wp_insert_post()
		 */
		$new_post_id = wp_insert_post( $args );

		/*
		 * присваиваем новому посту все элементы таксономий (рубрики, метки и т.д.) старого
		 */
		$taxonomies = get_object_taxonomies($post->post_type); // возвращает массив названий таксономий, используемых для указанного типа поста, например array("category", "post_tag");
		foreach ($taxonomies as $taxonomy) {
			$post_terms = wp_get_object_terms($post_id, $taxonomy, array('fields' => 'slugs'));
			wp_set_object_terms($new_post_id, $post_terms, $taxonomy, false);
		}

		/*
		 * дублируем все произвольные поля
		 */
		$post_meta_infos = $wpdb->get_results("SELECT meta_key, meta_value FROM $wpdb->postmeta WHERE post_id=$post_id");
		if (count($post_meta_infos)!=0) {
			$sql_query = "INSERT INTO $wpdb->postmeta (post_id, meta_key, meta_value) ";
			foreach ($post_meta_infos as $meta_info) {
				$meta_key = $meta_info->meta_key;
				$meta_value = addslashes($meta_info->meta_value);
				$sql_query_sel[]= "SELECT $new_post_id, '$meta_key', '$meta_value'";
			}
			$sql_query.= implode(" UNION ALL ", $sql_query_sel);
			$wpdb->query($sql_query);
		}


		/*
		 * и наконец, перенаправляем пользователя на страницу редактирования нового поста
		 */
		wp_redirect( admin_url( 'post.php?action=edit&post=' . $new_post_id ) );
		exit;
	} else {
		wp_die('Ошибка создания поста, не могу найти оригинальный пост с ID=: ' . $post_id);
	}
}
add_action( 'admin_action_true_duplicate_post_as_draft', 'true_duplicate_post_as_draft' );

/*
 * Добавляем ссылку дублирования поста для post_row_actions
 */
function true_duplicate_post_link( $actions, $post ) {
	if (current_user_can('edit_posts')) {
		$actions['duplicate'] = '<a href="admin.php?action=true_duplicate_post_as_draft&post=' . $post->ID . '" title="Дублировать этот пост" rel="permalink">Дублировать</a>';
	}
	return $actions;
}

add_filter( 'post_row_actions', 'true_duplicate_post_link', 10, 2 );
add_filter( '{kursi}_row_actions', 'true_duplicate_post_link', 10, 2);
/*тут работает*/
## Отфильтруем ЧПУ произвольного типа
// фильтр: apply_filters( 'post_type_link', $post_link, $post, $leavename, $sample );
add_filter( 'post_type_link', 'kursi_permalink', 1, 2 );
function kursi_permalink( $permalink, $post ){

	// выходим если это не наш тип записи: без холдера %faqcat%
	if( strpos( $permalink, '%kursicat%' ) === false )
		return $permalink;

	// Получаем элементы таксы
	$terms = get_the_terms( $post, 'kursicat' );
	// если есть элемент заменим холдер
	if( ! is_wp_error( $terms ) && !empty( $terms ) && is_object( $terms[0] ) )
		$term_slug = array_pop( $terms )->slug;
	// элемента нет, а должен быть...
	else
		$term_slug = 'no-kursicat';

	return str_replace( '%kursicat%', $term_slug, $permalink );
}

/* *** change route *** */
add_filter('post_link', 'true_post_type_permalink', 20, 3);
add_filter('post_type_link', 'true_post_type_permalink', 20, 3);

function true_post_type_permalink( $permalink, $post_id, $leavename ) {

	$post_type_name = 'kursi'; // название типа записи, вы можете найти его в админке или в функции register_post_type()
	$post_type_slug = 'kursi'; // часть URL товаров, не всегда совпадает с названием типа записи!
	$tax_name = 'kursicat'; // ну это понятно, название таксономии - категории товаров

	$post = get_post( $post_id ); // получаем объект поста по его ID

	if ( strpos( $permalink, $post_type_slug ) === FALSE || $post->post_type != $post_type_name ) // не делаем никаких изменений, если тип записи не соответствует или если URL не содержит ярлык tovar
		return $permalink;

	$termini = wp_get_object_terms( $post->ID, $tax_name ); // получаем все категории, к которым принадлежит данный товар


	if ( !is_wp_error( $termini ) && !empty( $termini ) && is_object( $termini[0] ) ) // и делаем перезапись ссылки, только, если товар находится хотя бы в одной категории, иначе возвращаем ссылку по умолчанию
		$permalink = str_replace( $post_type_slug, $termini[0]->slug, $permalink );

	return $permalink;
}


add_filter('request', 'true_post_type_request', 1, 1 );

function true_post_type_request( $query ){
	global $wpdb; // нам немного придётся поработать с БД

	$post_type_name = 'kursi'; // указываем тут название типа записей товара
	$tax_name = 'kursicat'; // а также название таксономии - категории товаров

	$yarlik = $query['attachment']; // после того, как мы изменили ссылки товаров в предыдущей функции, WordPress начал принимать их за страницы вложений

	// а теперь давайте получим ID товара, ярлык которого соответствует запросу на странице
	$post_id = $wpdb->get_var(
		"
		SELECT ID
		FROM $wpdb->posts
		WHERE post_name = '$yarlik'
		AND post_type = '$post_type_name'
		"
	);

	$termini = wp_get_object_terms( $post_id, $tax_name ); // товар должен находиться в категории (одной или нескольких)


	if( isset( $yarlik ) && $post_id && !is_wp_error( $termini ) && !empty( $termini ) ) : // изменяем запрос, если всё ок

		unset( $query['attachment'] );
		$query[$post_type_name] = $yarlik;
		$query['post_type'] = $post_type_name;
		$query['name'] = $yarlik;

	endif;

	return $query; // возвращаем результат
}
/*************/
// смена запроса кастомной таксономии
add_filter('request', 'true_smenit_request2', 1, 1 );

function true_smenit_request2( $query ){

	$taxonomia_name = 'kursicat'; // укажите название таксономии здесь, это также могут быть рубрики category или метки post_tag

	// запросы для дочерних элементов будут отличаться, поэтому нам потребуется дополнительная проверка
	if( $query['attachment'] ) :
		$dochernia = true; // эту переменную задаём для себя, она нам потребуется дальше
		$urlyarlyk = $query['attachment']; // это ярлык данного термина/рубрики/метки
	else:
		$dochernia = false;
		$urlyarlyk = $query['name']; // как видите, здесь ярлык хранится в другой переменной запроса
	endif;


	$termin = get_term_by('slug', $urlyarlyk, $taxonomia_name); // получаем элемент таксономии по ярлыку

	if ( isset( $urlyarlyk ) && $termin && !is_wp_error( $termin )): // если такого элемента не существует, прекращаем выполнение кода

		// для страниц дочерних элементов код немного отличается
		if( $dochernia ) {
			unset( $query['attachment'] );
			$parent = $termin->parent;
			while( $parent ) {
				$parent_term = get_term( $parent, $taxonomia_name);
				$urlyarlyk = $parent_term->slug . '/' . $urlyarlyk; // нам нужно получить полный путь, состоящий из ярлыка текущего элемента и всех его родителей
				$parent = $parent_term->parent;
			}
		} else {
			unset($query['name']);
		}

		switch( $taxonomia_name ): // параметры запроса для рубрик и меток отличаются от других таксономий
			case 'category':{
				$query['category_name'] = $urlyarlyk;
				break;
			}
			case 'post_tag':{
				$query['tag'] = $urlyarlyk;
				break;
			}
			default:{
				$query[$taxonomia_name] = $urlyarlyk;
				break;
			}
		endswitch;

	endif;

	return $query;

}

// смена самой ссылки
add_filter( 'term_link', 'true_smena_permalink2', 10, 3 );

function true_smena_permalink2( $url, $term, $taxonomy ){

	$taxonomia_name = 'kursicat'; // название таксономии, тут всё понятно
	$taxonomia_slug = 'kursicat'; // ярлык таксономии - зависит от параметра rewrite, указанного при создании и может отличаться от названия,
	// как например таксономия меток это post_tag, а ярлык по умолчанию tag

	// выходим из функции, если указанного ярлыка таксономии нет в URL или если название таксономии не соответствует
	if ( strpos($url, $taxonomia_slug) === FALSE || $taxonomy != $taxonomia_name ) return $url;

	$url = str_replace('/' . $taxonomia_slug, '', $url); // если мы ещё тут, выполняем замену в URL

	return $url;
}

/*custom post types kursi*/

add_action( 'init', 'register_stocks_post_type' );
function register_stocks_post_type() {
    register_post_type( 'stocks', [
		'label'               => 'Акции',
		'labels'              => array(
			'name'          => 'Акции',
			'singular_name' => 'Акция',
			'menu_name'     => 'Акции',
			'all_items'     => 'Все записи',
			'add_new'       => 'Добавить запись',
			'add_new_item'  => 'Добавить новую запись',
			'edit'          => 'Редактировать',
			'edit_item'     => 'Редактировать запись',
			'new_item'      => 'Новая запись',
		),
		'description'         => '',
		'public'              => true,
		'publicly_queryable'  => true,
		'show_ui'             => true,
		'show_in_rest'        => false,
		'rest_base'           => '',
		'show_in_menu'        => true,
		'exclude_from_search' => false,
		'capability_type'     => 'post',
		'map_meta_cap'        => true,
		'menu_icon'           => 'dashicons-buddicons-groups',
		'hierarchical'        => false,
		'rewrite'             => array( 'slug'=>'stocks', 'with_front'=>false, 'pages'=>false, 'feeds'=>false, 'feed'=>false ),
		'has_archive'         => true,
		'query_var'           => true,
		'supports'            => array( 'title', 'editor', 'thumbnail', 'excerpt' ),
	] );

}
add_filter('kama_breadcrumbs_pre_out', 'remove_elem');
function remove_elem($out){
    $r_out = str_replace('Курсы', '', $out);
    return $r_out;

}

/*
 *
 */
add_action( 'init', 'register_clients_post_type' );
function register_clients_post_type() {
    register_post_type( 'clients', [
		'label'               => 'Клиенты',
		'labels'              => array(
			'name'          => 'Клиенты',
			'singular_name' => 'Клиенты',
			'menu_name'     => 'Клиенты (слайды)',
			'all_items'     => 'Все записи',
			'add_new'       => 'Добавить запись',
			'add_new_item'  => 'Добавить новую запись',
			'edit'          => 'Редактировать',
			'edit_item'     => 'Редактировать запись',
			'new_item'      => 'Новая запись',
		),
		'description'         => '',
		'public'              => true,
		'publicly_queryable'  => true,
		'show_ui'             => true,
		'show_in_rest'        => false,
		'rest_base'           => '',
		'show_in_menu'        => true,
		'exclude_from_search' => false,
		'capability_type'     => 'post',
		'map_meta_cap'        => true,
		'menu_icon'           => 'dashicons-format-gallery',
		'hierarchical'        => false,
		'rewrite'             => array( 'slug'=>'clients', 'with_front'=>false, 'pages'=>false, 'feeds'=>false, 'feed'=>false ),
		'has_archive'         => false,
		'query_var'           => true,
		'supports'            => array( 'title', 'editor' ),
		'capabilities' => array(
			'create_posts' => false, // Removes support for the "Add New" function ( use 'do_not_allow' instead of false for multisite set ups )
		),
	] );

}
//поиск только по заголовкам записей start
function wph_search_by_title($search, &$wp_query) {
	global $wpdb;
	if (empty($search)) return $search;

	$q = $wp_query->query_vars;
	$n = !empty($q['exact']) ? '' : '%';
	$search = $searchand = '';

	foreach ((array) $q['search_terms'] as $term) {
		$term = esc_sql(like_escape($term));
		$search.="{$searchand}($wpdb->posts.post_title LIKE '{$n}{$term}{$n}')";
		$searchand = ' AND ';
	}

	if (!empty($search)) {
		$search = " AND ({$search}) ";
		if (!is_user_logged_in())
			$search .= " AND ($wpdb->posts.post_password = '') ";
	}
	return $search;
}
add_filter('posts_search', 'wph_search_by_title', 500, 2);
//поиск только по заголовкам записей end

function get_first_letter( $str ){
	return mb_substr($str, 0, 1, 'utf-8');
}


/*clients loader*/
function my_clients_loader(){
	$our_clients = get_field('our_clients', 1311);
	$out = '';
	foreach ($our_clients as $key => $our_client){
	    if(($key > $_POST['key']) && ($key <= ($_POST['key'] + 8))){
		    $out.= '<div class="col-md-3 col-sm-6 col-xs-12 clients clients-elem">
                    <div class="item-wrap">
                        <div class="img-wrap">
                            <img src="'.$our_client["our_client_img"]["url"].'" alt="'.$our_client["our_client_img"]["alt"].'">
                        </div>
                        <div class="name-wrap">
                            <span class="">'.$our_client["our_client_name"].'</span>
                        </div>
                        <div class="prof-wrap">
                            <span class="prof-text">Специальность: </span><span class="prof-value">'.$our_client["our_client_profession"].'</span>
                        </div>

                    </div>
                </div>';
	    }
	}

	die($out);
}
add_action('wp_ajax_clients_loader', 'my_clients_loader');
add_action('wp_ajax_nopriv_clients_loader', 'my_clients_loader');

add_filter( 'wpseo_canonical', 'my_canonical', 10, 2);
function my_canonical( $canonical_url, $post )
{
    if (!isset($_GET['profile'])){
	    $canonical_url = site_url().$_SERVER['REQUEST_URI'];
    } else {
	    $url = explode('?', $canonical_url = site_url().$_SERVER['REQUEST_URI']);
	    $canonical_url = $url[0];
    }
    return $canonical_url;
}

add_filter( 'wpseo_opengraph_url', 'my_og_url', 10, 1 );
function my_og_url()
{
	$url = site_url().$_SERVER['REQUEST_URI'];
	return $url;
}
