<?php get_header(); ?>
<div class="main-content">
	<!--<p>Ghbdtn platform</p>-->
        <div class="sc1">
            <div class="header-content_hero-slider">
                <div class="header-content_hero-slider1">
                    <div class="header-content_hero">
                        <div class="container">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="header-content_hero-slider-item">
                                        <h1 class="header-content_hero-title">Место для заголовка1</h1>
                                        <p class="header-content_hero-desc">Подзоголовок в две строки место для подзаголовка </p>
                                        <script data-b24-form="click/71/4qg0cm" data-skip-moving="true">
                                    (function(w,d,u){
                                    var s=d.createElement('script');s.async=true;s.src=u+'?'+(Date.now()/180000|0);
                                    var h=d.getElementsByTagName('script')[0];h.parentNode.insertBefore(s,h);
                                    })(window,document,'https://cdn-ru.bitrix24.ru/b6368597/crm/form/loader_71.js');
                                    </script>
                                        <button class="header-content_hero-button">Подробнее</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="header-content_hero-slider2">
                    <div class="header-content_hero">
                        <div class="container">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="header-content_hero-slider-item">
                                        <h1 class="header-content_hero-title">Место для заголовка2</h1>
                                        <p class="header-content_hero-desc">Подзоголовок в две строки место для подзаголовка </p>
                                        <script data-b24-form="click/71/4qg0cm" data-skip-moving="true">
                                    (function(w,d,u){
                                    var s=d.createElement('script');s.async=true;s.src=u+'?'+(Date.now()/180000|0);
                                    var h=d.getElementsByTagName('script')[0];h.parentNode.insertBefore(s,h);
                                    })(window,document,'https://cdn-ru.bitrix24.ru/b6368597/crm/form/loader_71.js');
                                    </script>
                                        <button class="header-content_hero-button">Подробнее</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="header-content_hero-slider3">
                    <div class="header-content_hero">
                        <div class="container">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="header-content_hero-slider-item">
                                        <h1 class="header-content_hero-title">Место для заголовка3</h1>
                                        <p class="header-content_hero-desc">Подзоголовок в две строки место для подзаголовка </p>
                                        <script data-b24-form="click/71/4qg0cm" data-skip-moving="true">
                                    (function(w,d,u){
                                    var s=d.createElement('script');s.async=true;s.src=u+'?'+(Date.now()/180000|0);
                                    var h=d.getElementsByTagName('script')[0];h.parentNode.insertBefore(s,h);
                                    })(window,document,'https://cdn-ru.bitrix24.ru/b6368597/crm/form/loader_71.js');
                                    </script>
                                        <button class="header-content_hero-button">Подробнее</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</div>
<div class="main-page-content">
    <section class="sc11">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <?php 
						if( function_exists('kama_breadcrumbs') ) kama_breadcrumbs();
                    ?>
                </div>
            </div>
        </div>
    </section>
    <section class="sc12">
        <div class="container">
            <h2 class="sc12-title">Учебный центр квалификации работников “ДПО АНТА” - <span>дополнительное профессиональное образование, переподготовка</span></h2>
            <div class="row">
                <div class="col-lg-6 col-md-6">
                    <p>Что подразумевается под тендерным сопровождением?</p>
                    <p>Это ряд услуг, оказываемых для успешного сотрудничества компаний в рамках тендеров, аукционов и торгов. Конечная цель услуги по тендерному сопровождению - это заключение договора с заказчиком по 44-ФЗ и 223-ФЗ или с коммерческими фирмами.</p>
                    <p>Тендерное сопровождение на протяжении длительного срока, оказывается по договору и включает в себя сопровождение в выбранной сфере на протяжении определенного договором времени.</p>
                    <p>Получить консультацию и помощь в конкретном тендере, можно позвонив по бесплатному номеру указанному на сайте. Тендерное сопровождение на протяжении длительного срока, оказывается по договору и включает в себя.</p>
                </div>
                <div class="col-lg-6 col-md-6">
                    <p>Что подразумевается под тендерным сопровождением?</p>
                    <p>Это ряд услуг, оказываемых для успешного сотрудничества компаний в рамках тендеров, аукционов и торгов. Конечная цель услуги по тендерному сопровождению - это заключение договора с заказчиком по 44-ФЗ и 223-ФЗ или с коммерческими фирмами.</p>
                    <p>Тендерное сопровождение на протяжении длительного срока, оказывается по договору и включает в себя сопровождение в выбранной сфере на протяжении определенного договором времени.</p>
                    <p>Получить консультацию и помощь в конкретном тендере, можно позвонив по бесплатному номеру указанному на сайте. Тендерное сопровождение на протяжении длительного срока, оказывается по договору и включает в себя.</p>
                </div>
            </div>
        </div>
    </section>
    <section class="sc13">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6">
                    <a href="">
                        <div class="wrapper wrapper1">
                            <div class="overlay-content">
                                <div>
                                    <p>Свидетельство о профессии рабочего, должности служащего </p>
                                </div>
                                <div> <img src="<? echo get_template_directory_uri()?>/img/zoom-ico-white.svg" alt="" class="img-fluid"></div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-lg-6 col-md-6">
                    <a href="">
                        <div class="wrapper wrapper2">
                            <div class="overlay-content">
                                <div>
                                    <p>Свидетельство о профессии рабочего, должности служащего </p>
                                </div>
                                <div> <img src="<? echo get_template_directory_uri()?>/img/zoom-ico-white.svg" alt="" class="img-fluid"></div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-lg-6 col-md-6">
                    <a href="">
                        <div class="wrapper wrapper3">
                            <div class="overlay-content">
                                <div>
                                    <p>Свидетельство о профессии рабочего, должности служащего </p>
                                </div>
                                <div> <img src="<? echo get_template_directory_uri()?>/img/zoom-ico-white.svg" alt="" class="img-fluid"></div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-lg-6 col-md-6">
                    <a href="">
                        <div class="wrapper wrapper4">
                            <div class="overlay-content">
                                <div>
                                    <p>Свидетельство о профессии рабочего, должности служащего </p>
                                </div>
                                <div> <img src="<? echo get_template_directory_uri()?>/img/zoom-ico-white.svg" alt="" class="img-fluid"></div>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </section>
    <section class="sc14">
        <div class="container">
            <h2 class="sc14-title">Список профессий</h2>
            <div class="row">
                <div class="col-lg-12">
                    <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                        <li class="nav-item"> <a class="nav-link active" id="pills-tab1-tab" data-toggle="pill" href="#pills-tab1" role="tab" aria-controls="pills-tab1" aria-selected="true">Машинисты</a> </li>
                        <li class="nav-item"> <a class="nav-link" id="pills-tab1-tab" data-toggle="pill" href="#pills-tab2" role="tab" aria-controls="pills-tab2" aria-selected="false">Слесари</a> </li>
                        <li class="nav-item"> <a class="nav-link" id="pills-tab3-tab" data-toggle="pill" href="#pills-tab3" role="tab" aria-controls="pills-tab3" aria-selected="false">Монтажники</a> </li>
                        <li class="nav-item"> <a class="nav-link" id="pills-tab4-tab" data-toggle="pill" href="#pills-tab4" role="tab" aria-controls="pills-tab4" aria-selected="true">Слесари</a> </li>
                        <li class="nav-item"> <a class="nav-link" id="pills-tab5-tab" data-toggle="pill" href="#pills-tab5" role="tab" aria-controls="pills-tab5" aria-selected="false">Монтажники</a> </li>
                        <li class="nav-item"> <a class="nav-link" id="pills-tab6-tab" data-toggle="pill" href="#pills-tab6" role="tab" aria-controls="pills-tab6" aria-selected="false">Слесари</a> </li>
                        <li class="nav-item"> <a class="nav-link" id="pills-tab7-tab" data-toggle="pill" href="#pills-tab7" role="tab" aria-controls="pills-tab7" aria-selected="false">Монтажники</a> </li>
                    </ul>
                    <div class="tab-content" id="pills-tabContent">
                        <div class="tab-pane fade show active" id="pills-tab1" role="tabpanel" aria-labelledby="pills-tab1-tab"><img src="<? echo get_template_directory_uri()?>/img/table-img.jpg" alt="" class="img-fluid"></div>
                        <div class="tab-pane fade" id="pills-tab2" role="tabpanel" aria-labelledby="pills-tab2-tab"><img src="<? echo get_template_directory_uri()?>/img/table-img.jpg" alt="" class="img-fluid"></div>
                        <div class="tab-pane fade" id="pills-tab3" role="tabpanel" aria-labelledby="pills-tab3-tab"><img src="<? echo get_template_directory_uri()?>/img/table-img.jpg" alt="" class="img-fluid"></div>
                        <div class="tab-pane fade" id="pills-tab4" role="tabpanel" aria-labelledby="pills-tab4-tab"><img src="<? echo get_template_directory_uri()?>/img/table-img.jpg" alt="" class="img-fluid"></div>
                        <div class="tab-pane fade" id="pills-tab5" role="tabpanel" aria-labelledby="pills-tab5-tab"><img src="<? echo get_template_directory_uri()?>/img/table-img.jpg" alt="" class="img-fluid"></div>
                        <div class="tab-pane fade" id="pills-tab6" role="tabpanel" aria-labelledby="pills-tab6-tab"><img src="<? echo get_template_directory_uri()?>/img/table-img.jpg" alt="" class="img-fluid"></div>
                        <div class="tab-pane fade" id="pills-tab7" role="tabpanel" aria-labelledby="pills-tab7-tab"><img src="<? echo get_template_directory_uri()?>/img/table-img.jpg" alt="" class="img-fluid"></div>
                    </div>
                </div>
            </div>
            <p class="view-all"><a href="">Показать все </a><img src="<? echo get_template_directory_uri()?>/img/view-all.svg" alt=""></p>
        </div>
    </section>
    <section class="sc5">
        <div class="container">
            <h2 class="sc5-title">Наши лицензии</h2>
            <div class="row">
                <div class="col-lg-4 col-6">
                    <a data-fancybox="images2" href="<? echo get_template_directory_uri()?>/img/sertificate-img1.jpg"><img src="<? echo get_template_directory_uri()?>/img/sertificate-img1.jpg" class="img-fluid"></a>
                    <div class="sc5-bottom-content">
                        <div>
                            <p>На право оказывать образовательные услуги</p>
                        </div>
                        <div><img src="<? echo get_template_directory_uri()?>/img/zoom-ico.svg" alt="" class="img-fluid"></div>
                    </div>
                </div>
                <div class="col-lg-4 col-6">
                    <a data-fancybox="images2" href="<? echo get_template_directory_uri()?>/img/sertificate-img2.jpg"><img src="<? echo get_template_directory_uri()?>/img/sertificate-img2.jpg" class="img-fluid"></a>
                    <div class="sc5-bottom-content">
                        <div>
                            <p>На право оказывать образовательные услуги</p>
                        </div>
                        <div><img src="<? echo get_template_directory_uri()?>/img/zoom-ico.svg" alt="" class="img-fluid"></div>
                    </div>
                </div>
                <div class="col-lg-4 col-6">
                    <a data-fancybox="images2" href="<? echo get_template_directory_uri()?>/img/sertificate-img3.jpg"><img src="<? echo get_template_directory_uri()?>/img/sertificate-img3.jpg" class="img-fluid"></a>
                    <div class="sc5-bottom-content">
                        <div>
                            <p>На право оказывать образовательные услуги</p>
                        </div>
                        <div><img src="<? echo get_template_directory_uri()?>/img/zoom-ico.svg" alt="" class="img-fluid"></div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="sc3">
        <div class="container">
            <h2 class="sc3-title">Что мы гарантируем</h2>
            <div class="row">
                <div class="col-lg-4 col-6">
                    <div class="wrapper"> <img src="<? echo get_template_directory_uri()?>/img/sc3-wrapper-ico1.svg" alt="" class="img-fluid">
                        <p class="sc3-wrapper__title">Быстро</p>
                        <p class="sc3-wrapper__desc">От 2 до 4 дней Доставка курьером 0₽</p>
                    </div>
                </div>
                <div class="col-lg-4 col-6">
                    <div class="wrapper"> <img src="<? echo get_template_directory_uri()?>/img/sc3-wrapper-ico2.svg" alt="" class="img-fluid">
                        <p class="sc3-wrapper__title">Все удостоверения в одном месте</p>
                        <p class="sc3-wrapper__desc">Рабочие специальности, охрана труда, ПТМ, ГО и ЧС</p>
                    </div>
                </div>
                <div class="col-lg-4 col-6">
                    <div class="wrapper"> <img src="<? echo get_template_directory_uri()?>/img/sc3-wrapper-ico3.svg" alt="" class="img-fluid">
                        <p class="sc3-wrapper__title">Доступные цены </p>
                        <p class="sc3-wrapper__desc">Лучшие цены в городе</p>
                    </div>
                </div>
                <div class="col-lg-4 col-6">
                    <div class="wrapper"> <img src="<? echo get_template_directory_uri()?>/img/sc3-wrapper-ico4.svg" alt="" class="img-fluid">
                        <p class="sc3-wrapper__title">Дистанционное обучение</p>
                        <p class="sc3-wrapper__desc"> - без отрыва от работы и производства</p>
                    </div>
                </div>
                <div class="col-lg-4 col-6">
                    <div class="wrapper"> <img src="<? echo get_template_directory_uri()?>/img/sc3-wrapper-ico5.svg" alt="" class="img-fluid">
                        <p class="sc3-wrapper__title">Все законно </p>
                        <p class="sc3-wrapper__desc">Учебный центр имеет все лицензии и аккредитации - работаем по договору</p>
                    </div>
                </div>
                <div class="col-lg-4 col-6">
                    <div class="wrapper"> <img src="<? echo get_template_directory_uri()?>/img/sc3-wrapper-ico6.svg" alt="" class="img-fluid">
                        <p class="sc3-wrapper__title">Все сведения заносим в реестр</p>
                        <p class="sc3-wrapper__desc">Выдаем документы гос образца</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="sc4">
        <div class="container">
            <h2 class="sc4-title">Отзывы наших учеников</h2>
            <div class="row">
                <div class="col-lg-12">
                    <div class="sc4-review-slider">
                        <div class="sc4-review-slider__item">
                            <a data-fancybox="images1" href="<? echo get_template_directory_uri()?>/img/review-content-img1.jpg"><img src="<? echo get_template_directory_uri()?>/img/review-content-img1.jpg" class="img-fluid"></a>
                        </div>
                        <div class="sc4-review-slider__item">
                            <a data-fancybox="images1" href="<? echo get_template_directory_uri()?>/img/review-content-img1.jpg"><img src="<? echo get_template_directory_uri()?>/img/review-content-img1.jpg" class="img-fluid"></a>
                        </div>
                        <div class="sc4-review-slider__item">
                            <a data-fancybox="images1" href="<? echo get_template_directory_uri()?>/img/review-content-img1.jpg"><img src="<? echo get_template_directory_uri()?>/img/review-content-img1.jpg" class="img-fluid"></a>
                        </div>
                        <div class="sc4-review-slider__item">
                            <a data-fancybox="images1" href="<? echo get_template_directory_uri()?>/img/review-content-img1.jpg"><img src="<? echo get_template_directory_uri()?>/img/review-content-img1.jpg" class="img-fluid"></a>
                        </div>
                        <div class="sc4-review-slider__item">
                            <a data-fancybox="images1" href="<? echo get_template_directory_uri()?>/img/review-content-img1.jpg"><img src="<? echo get_template_directory_uri()?>/img/review-content-img1.jpg" class="img-fluid"></a>
                        </div>
                        <div class="sc4-review-slider__item">
                            <a data-fancybox="images1" href="<? echo get_template_directory_uri()?>/img/review-content-img1.jpg"><img src="<? echo get_template_directory_uri()?>/img/review-content-img1.jpg" class="img-fluid"></a>
                        </div>
                    </div>
                    <div class="sc4-review-slider-arrow-content">
                        <div class="sc4-review-slider__prev"><img src="<? echo get_template_directory_uri()?>/img/sc4-review-slider__prev.svg" alt="" class="img-fluid"></div>
                        <div class="sc4-review-slider__next"><img src="<? echo get_template_directory_uri()?>/img/sc4-review-slider__next.svg" alt="" class="img-fluid"></div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="sc6">
        <div class="container">
            <h2 class="sc6-title">Наши клиенты</h2>
            <div class="row">
                <div class="col-lg-12">
                    <div class="our-client-slider">
                        <div class="our-client-slider__item"> <img src="<? echo get_template_directory_uri()?>/img/our-client-img1.png" alt="" class="img-fluid"> </div>
                        <div class="our-client-slider__item"> <img src="<? echo get_template_directory_uri()?>/img/our-client-img2.png" alt="" class="img-fluid"> </div>
                        <div class="our-client-slider__item"> <img src="<? echo get_template_directory_uri()?>/img/our-client-img3.png" alt="" class="img-fluid"> </div>
                        <div class="our-client-slider__item"> <img src="<? echo get_template_directory_uri()?>/img/our-client-img4.png" alt="" class="img-fluid"> </div>
                        <div class="our-client-slider__item"> <img src="<? echo get_template_directory_uri()?>/img/our-client-img1.png" alt="" class="img-fluid"> </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="sc8">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 order-lg-1 order-12">
                    <h2 class="sc8-title">Блок с seo текстом и видео</h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor <span>incididunt ut labore</span> et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse <span>cillum dolore</span> eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia <span>deserunt mollit</span> anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit quia dolor sit amet, consectetur, adipisci velit, sed quia non <a href="">Читать далее</a></p>
                </div>
                <div class="col-lg-6 order-lg-12 order-1">
                    <div class="video-container">
                        <div class="youtube" id="XQYpFkU31qE"></div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="sc9">
        <div class="container">
            <div class="wrapper">
                <div class="row">
                    <div class="col-lg-6"> <img src="<? echo get_template_directory_uri()?>/img/sc9-img1.png" alt="" class="img-fluid"> </div>
                    <div class="col-lg-6">
                        <h2>Скидки до 20% <br><span>от 5 человек</span></h2>
                        <form action="" method="post">
                            <input type="text" name="name" placeholder="Ваше имя" class="sc9-wrapper-form-input">
                            <input type="text" name="phone" placeholder="Номер телефона" class="sc9-wrapper-form-input">
                            <input type="email" name="email" placeholder="E-mail" class="sc9-wrapper-form-input">
                            <input type="submit" value="Получить скидку" class="sc9-wrapper-form-submit">
                            <p><img src="img/verified.png" alt="" class="img-fluid">Ваши данные защищены и не будут переданы третьим лицам</p>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="sc10">
        <div class="container">
            <div class="wrapper">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="wrapper-left">
                            <p>Поиск по специальностям</p>
                            <div class="input-content">
                                <div class="input-content-input">
                                    <input type="text" class="sc10-wrapper-input" placeholder="Введитае название курса..."> </div>
                                <div class="input-content-button">
                                    <button type="submit" class="sc10-wrapper-submit">
                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24">
                                            <path fill="none" d="M0 0h24v24H0z" />
                                            <path d="M18.031 16.617l4.283 4.282-1.415 1.415-4.282-4.283A8.96 8.96 0 0 1 11 20c-4.968 0-9-4.032-9-9s4.032-9 9-9 9 4.032 9 9a8.96 8.96 0 0 1-1.969 5.617zm-2.006-.742A6.977 6.977 0 0 0 18 11c0-3.868-3.133-7-7-7-3.868 0-7 3.132-7 7 0 3.867 3.132 7 7 7a6.977 6.977 0 0 0 4.875-1.975l.15-.15z" /> </svg> Найти</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="wrapper-right">
                            <p>Все курсы по алфавиту</p>
                            <div class="alphabite-content">
                                <div>
                                    <p><a href="">A</a></p>
                                </div>
                                <div>
                                    <p><a href="">A</a></p>
                                </div>
                                <div>
                                    <p><a href="">A</a></p>
                                </div>
                                <div>
                                    <p><a href="">A</a></p>
                                </div>
                                <div>
                                    <p><a href="">A</a></p>
                                </div>
                                <div>
                                    <p><a href="">A</a></p>
                                </div>
                                <div>
                                    <p><a href="">A</a></p>
                                </div>
                                <div>
                                    <p><a href="">A</a></p>
                                </div>
                                <div>
                                    <p><a href="">A</a></p>
                                </div>
                                <div>
                                    <p><a href="">A</a></p>
                                </div>
                                <div>
                                    <p><a href="">A</a></p>
                                </div>
                                <div>
                                    <p><a href="">A</a></p>
                                </div>
                                <div>
                                    <p><a href="">A</a></p>
                                </div>
                                <div>
                                    <p><a href="">A</a></p>
                                </div>
                                <div>
                                    <p><a href="">A</a></p>
                                </div>
                                <div>
                                    <p><a href="">A</a></p>
                                </div>
                                <div>
                                    <p><a href="">A</a></p>
                                </div>
                                <div>
                                    <p><a href="">A</a></p>
                                </div>
                                <div>
                                    <p><a href="">A</a></p>
                                </div>
                                <div>
                                    <p><a href="">A</a></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<?php get_footer(); ?>