<?php
/**
 * Template Name: Default
 * Description: The default template.
 */


$continuous_slide = ( isset( $atts['slideshow_settings']['continuous_sliding'] ) && '1' == $atts['slideshow_settings']['continuous_sliding'] ) ? 'true' : 'false';

do_action( 'wpmtst_before_view' );

?>

<div class="strong-view <?php wpmtst_container_class(); ?>"<?php wpmtst_container_data(); ?>>
	<?php do_action( 'wpmtst_view_header' ); ?>

	<div class="strong-content <?php wpmtst_content_class(); ?>">
		<?php do_action( 'wpmtst_before_content', $atts ); ?>

		<?php while ( $query->have_posts() ) : $query->the_post(); ?>
		<div class="<?php wpmtst_post_class($atts); ?>">
			<div class="wpmtst-testimonial-inner testimonial-inner">
			<?php do_action( 'wpmtst_before_testimonial' ); ?>

                <div <?php echo ('slideshow' == $atts['mode']) ? 'data-infinite-loop="'.esc_attr($continuous_slide).'"' : ''; ?>   class="wpmtst-testimonial-content testimonial-content">

					<?php wpmtst_the_thumbnail(); ?>
					<!--<div class="maybe-clear"></div>-->
                    <div class="testimonial-text-wrap">
                        <?php wpmtst_the_title( 'h3', 'wpmtst-testimonial-heading testimonial-heading' ); ?>
                        <?php
                            $post = get_post();
                            $currentDate = date("d.m.Y", strtotime($post->post_date));
                            $_monthsList = array(
                                ".01." => "января",
                                ".02." => "февраля",
                                ".03." => "марта",
                                ".04." => "апреля",
                                ".05." => "мая",
                                ".06." => "июня",
                                ".07." => "июля",
                                ".08." => "августа",
                                ".09." => "сентября",
                                ".10." => "октября",
                                ".11." => "ноября",
                                ".12." => "декабря"
                            );

                            $_mD = date(".m.", strtotime($currentDate));
                            $currentDate = str_replace($_mD, " ".$_monthsList[$_mD]." ", $currentDate);
                            echo '<span class="date">'.$currentDate.'</span>';
                        ?>
                        <div class="testimonial-content-wrap testimonial-hidden">
                        <?php wpmtst_the_content(); ?>
                        </div>
                        <div class="readmore-wrap">
                            <span class="readmore-span">Читать далее..</span>
                            <span class="readless">Скрыть</span>
                        </div>
                        <?php do_action( 'wpmtst_after_testimonial_content' ); ?>
                    </div>
				</div>

				<?php wpmtst_the_client(); ?>

				<div class="clear"></div>

				<?php do_action( 'wpmtst_after_testimonial' ,$atts); ?>
			</div>

		</div>
		<?php endwhile; ?>

		<?php do_action( 'wpmtst_after_content',$atts ) ?>
	</div>

	<?php do_action( 'wpmtst_view_footer' ); ?>
</div>

<?php do_action( 'wpmtst_after_view' ); ?>
